extends Node

#weight in pounds and measurements in inches

var crafting_catagories = []
onready var catagories = get_node("container1/catagories")

func _ready():
	pass
	#_append_tabs()

#create crafting tabs for each item catagory
func _append_tabs():
	for craftable_item in crafting:
		if craftable_item.has("CATAGORY"):
			for catagory in craftable_item["CATAGORY"]:
				if !crafting_catagories.has(catagory):
					crafting_catagories.append(catagory)
	
	for catagory in crafting_catagories:
		catagories.set_tab_title(catagories.get_tab_count() + 1, _get_catagory_by_name(catagory))

#gets item by enum ID and returns the full details
func _get_item_by_ID(ID):
	for item in items:
		if item["ID"] == ID:
			return item

#reversible GET that takes either a string or an int (enum) -used to populate crafting menu catagories
func _get_catagory_by_name(name):
	#typeof(2) is integer
	if typeof(name) == typeof(2):
		match name:
			SURVIVAL:
				return "Survival"
			MECHANICAL:
				return "Mechanical"
			ENGINE:
				return "Engine"
			FIRESTARTER:
				return "Firestarter"
	else:
		match name:
			"Survival":
				return SURVIVAL
			"Mechanical":
				return MECHANICAL
			"Engine":
				return ENGINE
			"Firestarter":
				return FIRESTARTER

var items = [
## POWER
	# battery
	{
		"ID": _9V_BATTERY,
		"NAME": "9V battery",

		"WEIGHT": 0.683433,
		"VOLUME": 0.2,
		"VALUE": 50,

		"PROPERTIES": [SOLID, TINY, RIGID, PARTS, HOLDS_CHARGE, EXPENDABLE],
		"CATAGORY": [ELECTRONIC, POWER, DEVICE]
	},
## DEVICES
	# small flashlight
	{
		"ID": SMALL_FLASHLIGHT,
		"NAME": "small flashlight",

		"WEIGHT": 1,
		"VOLUME": 1,
		"VALUE": 25,

		"PROPERTIES": [SOLID, SMALL_MEDIUM, RIGID, WATERTIGHT],
		"CATAGORY": [ELECTRONIC, DEVICE],
		"AMMUNITION": [
			[BATTERY, 3]
		],
		"COMPOSITION": [
			[HARD_PLASTIC_PIECE, 5],
			[METAL_PIECE, 1],
			[ELECTRONIC_PARTS, 1],
			[FLASHLIGHT_BULB, 1]
		]
	},
	# Bic lighter
	{
		"ID": BIC_LIGHTER,
		"NAME": "Bic lighter",

		"WEIGHT": 0.483433,
		"VOLUME": 0.3,
		"VALUE": 15,

		"PROPERTIES": [SOLID, SMALL, RIGID, PARTS, FLAME_SOURCE, WATERTIGHT, EXPENDABLE],
		"CATAGORY": [MECHANICAL, FIRESTARTER, DEVICE],
		"COMPOSITION": [
			[HARD_PLASTIC_PIECE, 1],
			[METAL_PIECE, 1]
		],
		"AMMUNITION": [
			[LIGHTER_FLUID, 30]
		]
	},
## NATURE
	# bark
	{
		"ID": BARK,
		"NAME": "bark",

		"WEIGHT": 6,
		"VOLUME": 4,
		"VALUE": 1,

		"PROPERTIES": [SOLID, TINY, RIGID, FLEXIBLE, FLAMMABLE, FIRE_FUEL, TANNIN_SOURCE],
		"CATAGORY": [NATURE]
	},
	# small stick
	{
		"ID": SMALL_STICK,
		"NAME": "small stick",
		"ALT": "medium stick",

		"WEIGHT": 6,
		"VOLUME": 4,
		"VALUE": 1,

		"PROPERTIES": [SOLID, MEDIUM, RIGID, FLEXIBLE, SHAFT, FLAMMABLE, FIRE_FUEL, TANNIN_SOURCE],
		"CATAGORY": [NATURE],
		"COMPOSITION": [
			[SMALL_STICK, 2],
			[BARK, 3]
		]
	},
	# medium stick
	{
		"ID": MEDIUM_STICK,
		"NAME": "medium stick",

		"WEIGHT": 6,
		"VOLUME": 4,
		"VALUE": 1,

		"PROPERTIES": [SOLID, MEDIUM, RIGID, FLEXIBLE, SHAFT, FLAMMABLE, FIRE_FUEL, TANNIN_SOURCE],
		"CATAGORY": [NATURE],
		"COMPOSITION": [
			[SMALL_STICK, 2],
			[BARK, 6]
		]
	},
	# big stick
	{
		"ID": BIG_STICK,
		"NAME": "big stick",

		"WEIGHT": 14,
		"VOLUME": 7,
		"VALUE": 1,

		"PROPERTIES": [SOLID, LARGE, RIGID, FLEXIBLE, SHAFT, FLAMMABLE, FIRE_FUEL, TANNIN_SOURCE],
		"CATAGORY": [NATURE],
		"COMPOSITION": [
			[MEDIUM_STICK, 2],
			[BARK, 9]
		]
	},
## CLOTHES
	# jeans
	{
		"ID": JEANS,
		"NAME": "jeans",
		"ALT": "cargo pants",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, DENIM, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, ABSORBANT, NON_RIGID],
		"CATAGORY": [CLOTHING],
		"COVERS": [HEAD],
		"COMPOSITION": [
			[CLOTH_PIECE, 5]
		]
	},
	# newsboy cap
	{
		"ID": NEWSBOY_CAP,
		"NAME": "newsboy cap",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, FELT, SMALL_MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, ABSORBANT, NON_RIGID],
		"CATAGORY": [CLOTHING],
		"COVERS": [HEAD],
		"COMPOSITION": [
			[CLOTH_PIECE, 3]
		]
	},
	# plaid shirt
	{
		"ID": PLAID_SHIRT,
		"NAME": "plaid shirt",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, UPPER_ARMS, ELBOWS, LOWER_ARMS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# armguards
	{
		"ID": ARMGUARDS,
		"NAME": "armguards",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [UPPER_ARMS, LOWER_ARMS],
		
		"COMPOSITION": [
			[HARD_PLASTIC_PIECE, 4]
		]
	},
	# backpack
	{
		"ID": BACKPACK,
		"NAME": "backpack",

		"WARMTH": null,
		"ENCUMBRANCE": null,
		"CONTAINER": 30,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO],
		"COMPOSITION": [
			[CLOTH_PIECE, 8]
		]
	},
	# balaclava
	{
		"ID": BALACLAVA,
		"NAME": "balaclava",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [FACE, HEAD],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# canteen
	{
		"ID": CANTEEN,
		"NAME": "canteen",

		"WARMTH": null,
		"ENCUMBRANCE": null,
		"CONTAINER": 8,
		
		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, SMALL, MELTS, KINDLING, NON_RIGID, SHEET, SEALS],
		"CATAGORY": [CLOTHING],
		"COVERS": [WAIST],
		"COMPOSITION": [
			[HARD_PLASTIC_PIECE, 4]
		]
	},
	# cargo pants
	{
		"ID": CARGO_PANTS,
		"NAME": "cargo pants",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [WAIST, UPPER_LEGS, LOWER_LEGS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# catcher's vest
	{
		"ID": CATCHERS_VEST,
		"NAME": "catcher's vest",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, WAIST],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# combat boots
	{
		"ID": COMBAT_BOOTS,
		"NAME": "combat boots",
		"ALT": "cowboy boots",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [FEET],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# cowboy boots
	{
		"ID": COWBOY_BOOTS,
		"NAME": "cowboy boots",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [FEET],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# cutoff plaid shirt
	{
		"ID": CUTOFF_PLAID_SHIRT,
		"NAME": "cutoff plaid shirt",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# Dick Show shirt
	{
		"ID": DICK_SHOW_SHIRT,
		"NAME": "Dick Show shirt",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, UPPER_ARMS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# duster
	{
		"ID": DUSTER,
		"NAME": "duster",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, UPPER_ARMS, LOWER_ARMS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# fingerless gloves
	{
		"ID": FINGERLESS_GLOVES,
		"NAME": "fingerless gloves",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [HANDS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# glasses
	{
		"ID": GLASSES,
		"NAME": "glasses",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [FACE],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# Goretex coat
	{
		"ID": GORETEX_COAT,
		"NAME": "Goretex coat",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, UPPER_ARMS, LOWER_ARMS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# grey pocket t
	{
		"ID": GREY_POCKET_T,
		"NAME": "grey pocket t",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, UPPER_ARMS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# hat shirt
	{
		"ID": HAT_SHIRT,
		"NAME": "hat shirt",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [HEAD],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# heavy tactical vest
	{
		"ID": HEAVY_TACTICAL_VEST,
		"NAME": "heavy tactical vest",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, WAIST],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# hiking bag
	{
		"ID": HIKING_BAG,
		"NAME": "hiking bag",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# hoodie
	{
		"ID": HOODIE,
		"NAME": "hoodie",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, UPPER_ARMS, LOWER_ARMS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# Kevlar vest
	{
		"ID": KEVLAR_VEST,
		"NAME": "Kevlar vest",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# Kill Bill bodysuit
	{
		"ID": KILL_BILL_BODYSUIT,
		"NAME": "Kill Bill bodysuit",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, UPPER_ARMS, LOWER_ARMS, WAIST, UPPER_LEGS, LOWER_LEGS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# metal shirt
	{
		"ID": METAL_SHIRT,
		"NAME": "metal shirt",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, UPPER_ARMS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# motorcycle gloves
	{
		"ID": MOTORCYCLE_GLOVES,
		"NAME": "motorcycle gloves",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [HANDS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# motorcycle boots
	{
		"ID": MOTORCYCLE_BOOTS,
		"NAME": "motorcycle boots",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [FEET],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# nightvision goggles
	{
		"ID": NIGHTVISION_GOGGLES,
		"NAME": "nightvision goggles",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [FACE],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# painter's overalls
	{
		"ID": PAINTERS_OVERALLS,
		"NAME": "painter's overalls",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, UPPER_ARMS, LOWER_ARMS, WAIST, UPPER_LEGS, LOWER_LEGS, FEET],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# poncho
	{
		"ID": PONCHO,
		"NAME": "poncho",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, HEAD],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# power armor helmet
	{
		"ID": POWER_ARMOR_HELMET,
		"NAME": "power armor helmet",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [HEAD, FACE],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# Predator helmet
	{
		"ID": PREDATOR_HELMET,
		"NAME": "Predator helmet",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, METAL, SMALL_MEDIUM, RIGID],
		"CATAGORY": [CLOTHING],
		"COVERS": [HEAD, FACE],
		"COMPOSITION": [
			[METAL_PIECE, 4]
		]
	},
	# Predator armor
	{
		"ID": PREDATOR_ARMOR,
		"NAME": "Predator armor",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, METAL, MEDIUM, RIGID],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, UPPER_ARMS, LOWER_ARMS, WAIST, UPPER_LEGS, LOWER_LEGS, FEET],
		"COMPOSITION": [
			[METAL_PIECE, 4]
		]
	},
	# messenger bag
	{
		"ID": MESSENGER_BAG,
		"NAME": "messenger bag",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# radsuit
	{
		"ID": RADSUIT,
		"NAME": "radsuit",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, UPPER_ARMS, LOWER_ARMS, HANDS, WAIST, UPPER_LEGS, LOWER_LEGS, FEET],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# radsuit helmet
	{
		"ID": RADSUIT_HELMET,
		"NAME": "radsuit helmet",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [HEAD, FACE],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# safety gloves
	{
		"ID": SAFETY_GLOVES,
		"NAME": "safety gloves",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [HANDS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# sneakers
	{
		"ID": SNEAKERS,
		"NAME": "sneakers",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [FEET],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# sneaking shoes
	{
		"ID": SNEAKING_SHOES,
		"NAME": "sneaking shoes",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [FEET],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# socks
	{
		"ID": SOCKS,
		"NAME": "socks",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [FEET],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# straitjacket
	{
		"ID": STRAITJACKET,
		"NAME": "straitjacket",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, UPPER_ARMS, LOWER_ARMS, WAIST, HANDS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# sunglasses
	{
		"ID": SUNGLASSES,
		"NAME": "sunglasses",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [FACE],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# thermal suit
	{
		"ID": THERMAL_SUIT,
		"NAME": "thermal suit",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, UPPER_ARMS, LOWER_ARMS, WAIST, UPPER_LEGS, LOWER_LEGS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# thongs
	{
		"ID": THONGS,
		"NAME": "thongs",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [FEET],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# utility belt
	{
		"ID": UTILITY_BELT,
		"NAME": "utility belt",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [WAIST],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# welding mask
	{
		"ID": WELDING_MASK,
		"NAME": "welding mask",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [FACE],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# welding goggles
	{
		"ID": WELDING_GOGGLES,
		"NAME": "welding goggles",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [FACE],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# white t shirt
	{
		"ID": WHITE_T_SHIRT,
		"NAME": "white t shirt",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, UPPER_ARMS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# wristwatch
	{
		"ID": WRISTWATCH,
		"NAME": "wristwatch",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [HANDS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
	# black t shirt
	{
		"ID": BLACK_T_SHIRT,
		"NAME": "black t shirt",

		"WARMTH": null,
		"ENCUMBRANCE": null,

		"WEIGHT": null,
		"VOLUME": null,
		"VALUE": null,

		"PROPERTIES": [SOLID, WOOL, MEDIUM, FLEXIBLE, FLAMMABLE, KINDLING, NON_RIGID, SHEET],
		"CATAGORY": [CLOTHING],
		"COVERS": [TORSO, SHOULDERS, UPPER_ARMS],
		"COMPOSITION": [
			[CLOTH_PIECE, 4]
		]
	},
## TOOLS
	# hatchet
	{
		"ID": HATCHET,
		"NAME": "hatchet",

		"WEIGHT": 4,
		"VOLUME": 2,
		"VALUE": 150,

		"PROPERTIES": [SOLID, METAL, SMALL_MEDIUM, RIGID, SHARP_EDGE],
		"CATAGORY": 
		[WEAPON, TOOL, AXE]
	},
	# crowbar
	{
		"ID": CROWBAR,
		"NAME": "crowbar",

		"WEIGHT": 7,
		"VOLUME": 2,
		"VALUE": 150,

		"PROPERTIES": [SOLID, METAL, SMALL_MEDIUM, RIGID],
		"CATAGORY": 
		[WEAPON, TOOL, PRYING_DEVICE]
	},
	# shovel
	{
		"ID": SHOVEL,
		"NAME": "shovel",

		"WEIGHT": 7,
		"VOLUME": 2,
		"VALUE": 150,

		"PROPERTIES": [SOLID, METAL, SMALL_MEDIUM, RIGID],
		"CATAGORY": 
		[WEAPON, TOOL, DIGGING_DEVICE]
	},
	# hoe
	{
		"ID": HOE,
		"NAME": "hoe",

		"WEIGHT": 7,
		"VOLUME": 2,
		"VALUE": 150,

		"PROPERTIES": [SOLID, METAL, MEDIUM, RIGID],
		"CATAGORY": 
		[WEAPON, TOOL, DIGGING_DEVICE]
	},
	# can opener
	{
		"ID": CAN_OPENER,
		"NAME": "can opener",
		"ALT": "steak knife",

		"WEIGHT": 1,
		"VOLUME": 0.2,
		"VALUE": 10,

		"PROPERTIES": [SOLID, PLASTIC, SMALL, RIGID],
		"CATAGORY": 
		[WEAPON, TOOL]
	},
	# corkscrew
	{
		"ID": CORKSCREW,
		"NAME": "corkscrew",
		"ALT": "steak knife",

		"WEIGHT": 1,
		"VOLUME": 0.2,
		"VALUE": 10,

		"PROPERTIES": [SOLID, PLASTIC, SMALL, RIGID],
		"CATAGORY": 
		[WEAPON, TOOL]
	},
## MELEE WEAPONS
	# basic spear
	{
		"ID": BASIC_SPEAR,
		"NAME": "basic spear",

		"WEIGHT": 4,
		"VOLUME": 4,
		"VALUE": 30,

		"PROPERTIES": [SOLID, RIGID, WOOD, LONG, MEDIUM, FLAMMABLE, SHARP_POINT],
		"CATAGORY": 
		[WEAPON, SPEAR]
	},
	# quarterstaff
	{
		"ID": QUARTERSTAFF,
		"NAME": "quarterstaff",

		"WEIGHT": 6,
		"VOLUME": 4,
		"VALUE": 30,

		"PROPERTIES": [SOLID, RIGID, WOOD, LONG, MEDIUM, FLAMMABLE],
		"CATAGORY": 
		[WEAPON, STAFF]
	},
	# fireaxe
	{
		"ID": FIREAXE,
		"NAME": "fireaxe",

		"WEIGHT": 12,
		"VOLUME": 4,
		"VALUE": 150,

		"PROPERTIES": [SOLID, RIGID, METAL, LONG, MEDIUM, FLAMMABLE, SHARP_EDGE],
		"CATAGORY": 
		[WEAPON, AXE]
	},
	# katana
	{
		"ID": KATANA,
		"NAME": "katana",
		"ALT": "combat knife",

		"WEIGHT": 9,
		"VOLUME": 4,
		"VALUE": 150,

		"PROPERTIES": [SOLID, RIGID, METAL, LONG, MEDIUM, FLAMMABLE, SHARP_EDGE, SHARP_POINT],
		"CATAGORY": 
		[WEAPON, BLADE]
	},
	# guitar
	{
		"ID": GUITAR,
		"NAME": "guitar",

		"WEIGHT": 12,
		"VOLUME": 4,
		"VALUE": 150,

		"PROPERTIES": [SOLID, RIGID, WOOD, LONG, SHAFT, MEDIUM, FLAMMABLE],
		"CATAGORY": 
		[WEAPON]
	},
## GUNS
	# Luger pistol
	{
		"ID": LUGER_PISTOL,
		"NAME": "Luger pistol",
		"ALT": "pistol",

		"WEIGHT": 2,
		"VOLUME": 2,
		"VALUE": 400,

		"PROPERTIES": [SOLID, METAL, SMALL_MEDIUM, RIGID, FLAMMABLE],
		"CATAGORY": 
		[WEAPON, FIREARM, _9X19MM_PARABELLUM, LUGER, PISTOL],
		"CAN_HOLD": [
			[[_9X19MM_PARABELLUM, LUGER, MAGAZINE], 1]
		]
	},
	# 9mm pistol
	{
		"ID": _9MM_PISTOL,
		"NAME": "9mm pistol",
		"ALT": "pistol",

		"WEIGHT": 2,
		"VOLUME": 2,
		"VALUE": 400,

		"PROPERTIES": [SOLID, METAL, SMALL_MEDIUM, RIGID, FLAMMABLE],
		"CATAGORY": 
		[WEAPON, FIREARM, _9MM, PISTOL],
		"CAN_HOLD": [
			[[_9MM, MAGAZINE], 1]
		]
	},
	# Desert Eagle
	{
		"ID": DEAGLE,
		"NAME": "Desert Eagle",
		"ALT": "revolver",

		"WEIGHT": 7,
		"VOLUME": 2,
		"VALUE": 400,

		"PROPERTIES": [SOLID, METAL, SMALL_MEDIUM, RIGID, FLAMMABLE],
		"CATAGORY": 
		[WEAPON, FIREARM, _357_MAG, PISTOL],
		"CAN_HOLD": [
			[[_357_MAG, MAGAZINE], 1]
		]
	},
	#.357 revolver
	{
		"ID": _357_REVOLVER,
		"NAME": ".357 revolver",
		"ALT": "revolver",

		"WEIGHT": 3.08647,
		"VOLUME": 2,
		"VALUE": 100,

		"PROPERTIES": [SOLID, METAL, SMALL_MEDIUM, RIGID, FLAMMABLE],
		"CATAGORY": [WEAPON, FIREARM, _357_MAG, REVOLVER],
		"CAN_HOLD": [
			[[_357_MAG, BULLET], 6],
			[[_38_SPECIAL, BULLET], 6]
		]
	},
	# SPAS-12 shotgun
	{
		"ID": SPAS_12_SHOTGUN,
		"NAME": "SPAS-12 shotgun",

		"WEIGHT": 12,
		"VOLUME": 3.5,
		"VALUE": 400,

		"PROPERTIES": [SOLID, METAL, MEDIUM, RIGID, FLAMMABLE],
		"CATAGORY": 
		[WEAPON, FIREARM, _12GA, SHOTGUN],
		"CAN_HOLD": [
			[[_12GA, BULLET], 6]
		]
	},
	# AK47
	{
		"ID": AK47,
		"NAME": "AK47",

		"WEIGHT": 7.7,
		"VOLUME": 4,
		"VALUE": 1000,

		"PROPERTIES": [SOLID, METAL, MEDIUM, RIGID, FLAMMABLE],
		"CATAGORY": [WEAPON, FIREARM, AK, RIFLE, _7_62X39MM],
		"CAN_HOLD": [
			[[_7_62X39MM, AK, MAGAZINE], 1]
		]
	},
	# light machine gun
	{
		"ID": M249_LMG,
		"NAME": "M249 LMG",

		"WEIGHT": 20,
		"VOLUME": 4,
		"VALUE": 1000,

		"PROPERTIES": [SOLID, METAL, MEDIUM, RIGID, FLAMMABLE],
		"CATAGORY": [WEAPON, FIREARM, LIGHT_MACHINE_GUN, STANAG, _5_56X45MM_NATO],
		"CAN_HOLD": [
			[[_5_56X45MM_NATO, MAGAZINE], 1]
		]
	},
	# bow
	{
		"ID": BOW,
		"NAME": "bow",

		"WEIGHT": 1,
		"VOLUME": 4,
		"VALUE": 20,

		"PROPERTIES": [SOLID, WOOD, LARGE, RIGID, FLEXIBLE, FLAMMABLE, SHAFT],
		"CATAGORY": [WEAPON, FIREARM, BOW]
	},
	# compound bow
	{
		"ID": COMPOUND_BOW,
		"NAME": "compound bow",

		"WEIGHT": 1,
		"VOLUME": 4,
		"VALUE": 20,

		"PROPERTIES": [SOLID, WOOD, LARGE, RIGID, FLEXIBLE, FLAMMABLE, SHAFT],
		"CATAGORY": [WEAPON, FIREARM, BOW]
	},
## BULLETS n stuff
	# 9mm magazine
	{
		"ID": _9MM_MAGAZINE,
		"NAME": "9mm magazine",

		"WEIGHT": 0.9,
		"VOLUME": 0.3,
		"VALUE": 400,

		"PROPERTIES": [SOLID, SMALL, RIGID, FLAMMABLE],
		"CATAGORY": [_9MM, MAGAZINE],
		"CAN_HOLD": [
			[[_9MM, BULLET], 12]
		]
	},
	# 9mm bullet
	{
		"ID": _9MM_BULLET,
		"NAME": "9mm bullet",

		"WEIGHT": 0.9,
		"VOLUME": 0.3,
		"VALUE": 400,

		"PROPERTIES": [SOLID, TINY, RIGID, FLAMMABLE],
		"CATAGORY": [_9MM, BULLET]
	},
	# Luger magazine
	{
		"ID": LUGER_MAGAZINE,
		"NAME": "Luger magazine",
		"ALT": "magazine",

		"WEIGHT": 2,
		"VOLUME": 2,
		"VALUE": 400,

		"PROPERTIES": [SOLID, METAL, SMALL_MEDIUM, RIGID, FLAMMABLE],
		"CATAGORY": 
		[_9X19MM_PARABELLUM, MAGAZINE],
		"CAN_HOLD": [
			[[_9X19MM_PARABELLUM, BULLET], 8]
		]
	},
	# Luger drum magazine
	{
		"ID": LUGER_DRUM_MAGAZINE,
		"NAME": "Luger drum magazine",
		"ALT": "magazine",

		"WEIGHT": 2,
		"VOLUME": 2,
		"VALUE": 400,

		"PROPERTIES": [SOLID, METAL, SMALL_MEDIUM, RIGID, FLAMMABLE],
		"CATAGORY": 
		[_9X19MM_PARABELLUM, MAGAZINE],
		"CAN_HOLD": [
			[[_9X19MM_PARABELLUM, BULLET], 32]
		]
	},
	# 9x19mm Parabellum bullet
	{
		"ID": _9X19MM_PARABELLUM_BULLET,
		"NAME": "9x19mm Parabellum bullet",

		"WEIGHT": 0.9,
		"VOLUME": 0.3,
		"VALUE": 400,

		"PROPERTIES": [SOLID, TINY, RIGID, FLAMMABLE],
		"CATAGORY": [_9X19MM_PARABELLUM, BULLET]
	},
	# .357 bullet
	{
		"ID": _357_BULLET,
		"NAME": ".357 bullet",

		"WEIGHT": 0.9,
		"VOLUME": 0.3,
		"VALUE": 400,

		"PROPERTIES": [SOLID, TINY, RIGID, FLAMMABLE],
		"CATAGORY": [_357_MAG, BULLET]
	},
	# .357 magazine
	{
		"ID": _357_MAGAZINE,
		"NAME": ".357 Magnum magazine",

		"WEIGHT": 0.9,
		"VOLUME": 0.3,
		"VALUE": 400,

		"PROPERTIES": [SOLID, SMALL, RIGID, FLAMMABLE],
		"CATAGORY": [_357_MAG, MAGAZINE],
		"CAN_HOLD": [
			[[_357_MAG, BULLET], 9]
		]
	},
	# .357 speedloader
	{
		"ID": _357_SPEEDLOADER,
		"NAME": ".357 speedloader",

		"WEIGHT": 0.1,
		"VOLUME": 0.7,
		"VALUE": 20,

		"PROPERTIES": [SOLID, PLASTIC, SMALL, RIGID, FLAMMABLE],
		"CATAGORY": [_357_MAG, SPEEDLOADER],
		"CAN_HOLD": [
			[[_357_MAG, BULLET], 6]
		]
	},
	# 5.56x45mm NATO bullet
	{
		"ID": _5_56X45MM_NATO_BULLET,
		"NAME": "5.56x45mm NATO bullet",

		"WEIGHT": 0.9,
		"VOLUME": 0.3,
		"VALUE": 400,

		"PROPERTIES": [SOLID, TINY, RIGID, FLAMMABLE],
		"CATAGORY": [_5_56X45MM_NATO, BULLET]
	},
	# 5.56x45mm NATO STANAG magazine
	{
		"ID": _5_56X45MM_NATO_STANAG_MAGAZINE,
		"NAME": "5.56x45mm NATO STANAG magazine",

		"WEIGHT": 0.9,
		"VOLUME": 0.3,
		"VALUE": 400,

		"PROPERTIES": [SOLID, SMALL, RIGID, FLAMMABLE],
		"CATAGORY": [_5_56X45MM_NATO, STANAG, MAGAZINE],
		"CAN_HOLD": [
			[[_5_56X45MM_NATO, BULLET], 50]#-100
		]
	},
	# 5.56x45mm NATO M27 linked disintegrating belt
	{
		"ID": M27_LINKED_DISINTEGRATING_BELT,
		"NAME": "M27 linked disintegrating belt",

		"WEIGHT": 0.9,
		"VOLUME": 0.3,
		"VALUE": 400,

		"PROPERTIES": [SOLID, SMALL, RIGID, FLAMMABLE],
		"CATAGORY": [_5_56X45MM_NATO, STANAG, AMMO_BELT],
		"CAN_HOLD": [
			[[_5_56X45MM_NATO, BULLET], 100]#-1000
		]
	},
	# AK-47 magazine (30)7.62x39mm
	{
		"ID": _7_62X39MM_MAGAZINE_AK,
		"NAME": "AK-47 magazine (30)7.62x39mm",
		"ALT": "magazine",

		"WEIGHT": 0.9,
		"VOLUME": 0.4,
		"VALUE": 40,

		"PROPERTIES": [SOLID, SMALL_MEDIUM, RIGID, FIREPROOF],
		"CATAGORY": [_7_62X39MM, AK, MAGAZINE],
		"CAN_HOLD": [
			[[_7_62X39MM, BULLET], 30]
		]
	},
	# 7.62x39mm bullet
	{
		"ID": _7_62X39MM_BULLET,
		"NAME": "7.62x39mm bullet",
		"ALT": "bullet",

		"WEIGHT": 0.1,
		"VOLUME": 0.05,
		"VALUE": 50,

		"PROPERTIES": [SOLID, TINY, RIGID, FLAMMABLE],
		"CATAGORY": [_7_62X39MM, BULLET]
	},
	# 12 gauge shotgun shell
	{
		"ID": _12GA_SHOTGUN_SHELL,
		"NAME": "12GA shotgun shell",

		"WEIGHT": 0.1,
		"VOLUME": 0.05,
		"VALUE": 25,

		"PROPERTIES": [SOLID, TINY, RIGID, FLAMMABLE],
		"CATAGORY": [_12GA, BULLET]
	},
	# .38 Special speedloader
	{
		"ID": _38_SPECIAL_SPEEDLOADER,
		"NAME": ".38 Special speedloader",

		"WEIGHT": 0.1,
		"VOLUME": 0.7,
		"VALUE": 20,

		"PROPERTIES": [SOLID, PLASTIC, SMALL, RIGID, FLAMMABLE],
		"CATAGORY": [_38_SPECIAL, SPEEDLOADER],
		"CAN_HOLD": [
			[[_38_SPECIAL, BULLET], 6]
		]
	},
	# .38 Special bullet
	{
		"ID": _38_SPECIAL_BULLET,
		"NAME": ".38 Special bullet",

		"WEIGHT": 0.1,
		"VOLUME": 0.05,
		"VALUE": 25,

		"PROPERTIES": [SOLID, TINY, RIGID, FLAMMABLE],
		"CATAGORY": [_38_SPECIAL, BULLET]
	},
	# arrow
	{
		"ID": ARROW,
		"NAME": "arrow",

		"WEIGHT": 0.4,
		"VOLUME": 0.8,
		"VALUE": 5,

		"PROPERTIES": [SOLID, WOOD, SHARP_POINT, MEDIUM, RIGID, FLEXIBLE, FLAMMABLE, SHAFT],
		"CATAGORY": [AMMO, ARROW]
	},
## USEFUL JUNK
	# cloth piece
	{
		"ID": CLOTH_PIECE,
		"NAME": "cloth piece",

		"PROPERTIES": [SOLID, WOOL, SMALL, FLEXIBLE, FLAMMABLE, NON_RIGID, SHEET],
		"CATAGORY": [USEFUL_JUNK],
		"WEIGHT": 0.5,
		"VOLUME": 0.3,
		"VALUE": 0
	},
	# leather piece
	{
		"ID": LEATHER_PIECE,
		"NAME": "leather piece",

		"PROPERTIES": [SOLID, LEATHER, SMALL, FLEXIBLE, FLAMMABLE, NON_RIGID, SHEET],
		"CATAGORY": [USEFUL_JUNK],
		"WEIGHT": 1,
		"VOLUME": 0.5,
		"VALUE": 0
	},
	# faux fur piece
	{
		"ID": FAUX_FUR_PIECE,
		"NAME": "faux fur piece",

		"PROPERTIES": [SOLID, FAUX_FUR, TINY, FLEXIBLE, FLAMMABLE, NON_RIGID, INSULATOR],
		"CATAGORY": [USEFUL_JUNK],
		"WEIGHT": 0.01,
		"VOLUME": 0.01,
		"VALUE": 0
	},
	# down feather
	{
		"ID": DOWN_FEATHER,
		"NAME": "down feather",

		"PROPERTIES": [SOLID, DOWN, TINY, FLEXIBLE, FLAMMABLE, NON_RIGID, INSULATOR],
		"CATAGORY": [USEFUL_JUNK],
		"WEIGHT": 0.01,
		"VOLUME": 0.01,
		"VALUE": 0
	},
	# soft plastic piece
	{
		"ID": SOFT_PLASTIC_PIECE,
		"NAME": "soft plastic piece",

		"PROPERTIES": [SOLID, PLASTIC, SMALL, FLEXIBLE, FLAMMABLE, NON_RIGID, SHEET],
		"CATAGORY": [USEFUL_JUNK],
		"WEIGHT": 1,
		"VOLUME": 1,
		"VALUE": 0
	},
	# hard plastic piece
	{
		"ID": HARD_PLASTIC_PIECE,
		"NAME": "hard plastic piece",

		"PROPERTIES": [SOLID, PLASTIC, SMALL, FLAMMABLE, RIGID, SHEET],
		"CATAGORY": [USEFUL_JUNK],
		"WEIGHT": 1,
		"VOLUME": 1,
		"VALUE": 0
	},
	# metal piece
	{
		"ID": METAL_PIECE,
		"NAME": "metal piece",

		"PROPERTIES": [SOLID, METAL, SMALL, FIREPROOF, RIGID, SHEET],
		"CATAGORY": [USEFUL_JUNK],
		"WEIGHT": 1,
		"VOLUME": 1,
		"VALUE": 0
	},
## PARTS
	# electronic parts
	{
		"ID": ELECTRONIC_PARTS,
		"NAME": "electronic parts",

		"PROPERTIES": [SOLID, PLASTIC, SMALL, FLAMMABLE, RIGID, PARTS],
		"CATAGORY": [ELECTRONIC],
		"WEIGHT": 1,
		"VOLUME": 1,
		"VALUE": 0
	},
	# mechanical parts
	{
		"ID": MECHANICAL_PARTS,
		"NAME": "mechanical parts",

		"PROPERTIES": [SOLID, METAL, SMALL, FIREPROOF, RIGID, PARTS],
		"CATAGORY": [MECHANICAL],
		"WEIGHT": 1,
		"VOLUME": 1,
		"VALUE": 0
	}
]

var crafting = [
	{
		"RESULT": FUEL_TANK,
		
		"CATAGORY": [MECHANICAL, ENGINE],
		
		"COMPONENTS": [
			{
				"TIME": 0,
				"DIFFICULTY": 0,
				#any watertight container
				"GENERAL":
				[
					[
						RIGID, WATERTIGHT, CONTAINER, [1, REVERSIBLE]
					]
				]
			},
		#alternate recipes below ie: six pieces of metal welded together
			{
				"TIME": 300,
				"DIFFICULTY": 5,
				"SKILL":
				[
					e.skill.WELDING
				],
				"SPECIFIC":
				[
					[WELDER, [1, TOOL]],
					[METAL_PIECE, [6]]
				]
			}
		]
	},
	{
		"RESULT": KINDLING_BUNDLE,

		"CATAGORY": [FIRESTARTER],
		"DIFFICULTY": 0,

		"COMPONENTS": [
			{
				"GENERAL":
				[
					[KINDLING, [3, REVERSIBLE]]
				],
				"TIME": 5
			}
		]
	},
	{
		"RESULT": FLINT_AND_STEEL,

		"CATAGORY": [FIRESTARTER],
		"DIFFICULTY": 0,

		"COMPONENTS": [
			{
				"GENERAL":
				[
					[FLINT, [1, REVERSIBLE]],
					[HIGH_CARBON, STEEL, [1, REVERSIBLE]]
				],
				"TIME": 0
			}
		]
	},
	{
		"RESULT": HAND_DRILL,

		"CATAGORY": [FIRESTARTER, SURVIVAL],
		"DIFFICULTY": 2,

		"COMPONENTS": [
			{
				"SKILL":
				[
					e.skill.FIRE_HAND_DRILL
				],
				"GENERAL":
				[
					[SMALL_MEDIUM, RIGID, FLAMMABLE, WOOD, SHAFT, [1]],
					[SMALL_MEDIUM, RIGID, FLAMMABLE, WOOD, BOARD, [1]]
				],
				"TIME": 120
			}
		]
	},
	{
		"RESULT": SMALL_CAMPFIRE,

		"CATAGORY": [SURVIVAL],
		"COMPONENTS": [
			#Flame source ie: lighter, matches
			{
				"TIME": 5,
				"DIFFICULTY": 0,

				"SPECIFIC":
				[
					[KINDLING_BUNDLE, [1]]
				],
				"GENERAL":
				[
					[FLAME_SOURCE, [1, TOOL]]
				]
			},
			#Flint and steel
			{
				"TIME": 1000,
				"DIFFICULTY": 20,
				"SPECIFIC":
				[
					[KINDLING_BUNDLE, [1]],
					[FLINT_AND_STEEL, [1, TOOL]]
				]
			},
			#Hand drill method
			{
				"TIME": 500,
				"DIFFICULTY": 10,
				"SKILL":
				[
					e.skill.FIRE_HAND_DRILL
				],
				"SPECIFIC":
				[
					[KINDLING_BUNDLE, [1]],
					[HAND_DRILL, [1]]
				]
			}
		]
	},
	#Large campfire
	{
		"RESULT": LARGE_CAMPFIRE,

		"CATAGORY": [SURVIVAL],
		"COMPONENTS": [
			#Add two medium flammable objects for large campfire
			{
				"TIME": 5,
				"DIFFICULTY": 0,

				"SPECIFIC":
				[
					[SMALL_CAMPFIRE, [1]]
				],
				"GENERAL":
				[
					[[MEDIUM, LARGE], RIGID, FLAMMABLE, [2]]
				]
			}
		]
	}
]

#ITEMS
enum {
	#BATTERIES
	_9V_BATTERY,
	
	#FIRESTARTERS
	BIC_LIGHTER,
	LIGHTER_FLUID_BOTTLE,
	MATCHES,
	FIRESTARTER_BRICK,
	#crafted
	FLINT_AND_STEEL,
	HAND_DRILL,
	
	#FIRE FUEL
	KINDLING_BUNDLE,
	
	#FIRE SOURCES
	SMALL_CAMPFIRE,
	LARGE_CAMPFIRE,
	BARREL_FIRE,
	
	#NATURE
	BARK,
	SMALL_STICK,
	MEDIUM_STICK,
	BIG_STICK,
	
	#FOOD
	HUMAN_MEAT,
	DOG_MEAT,
	APPLE,
	COFFEE_BEAN,
	
	#LIQUIDS
	SOFT_DRINK,
	ENERGY_DRINK,
	WATER,
	COFFEE,
	LIGHTER_FLUID,
	
	#EMPTY DRINK CONTAINERS
	#containers
	DRINK_CAN,
	TIN_CAN,
	PLASTIC_BOTTLE,
	GLASS_BOTTLE,
	#caps
	PLASTIC_BOTTLE_CAP,
	GLASS_BOTTLE_CAP,
	
	#GUNS N AMMO
	ARROW,
	BOLT,

	_9MM_BULLET,
	_9MM_MAGAZINE,
	
	_9X19MM_PARABELLUM_BULLET,
	LUGER_MAGAZINE,
	LUGER_DRUM_MAGAZINE,
	
	_38_SPECIAL_BULLET
	_38_SPECIAL_SPEEDLOADER,
	
	_357_BULLET,
	_357_MAGAZINE,
	_357_SPEEDLOADER,
	
	_7_62X39MM_BULLET,
	_7_62X39MM_MAGAZINE_AK,
	
	_5_56X45MM_NATO_BULLET,
	_5_56X45MM_NATO_STANAG_MAGAZINE,
	M27_LINKED_DISINTEGRATING_BELT,
	
	_12GA_SHOTGUN_SHELL,

	#WEAPONS
	#ranged
	AK47,
	BOW,
	COMPOUND_BOW,
	DEAGLE,
	_9MM_PISTOL,
	_357_REVOLVER,
	SPAS_12_SHOTGUN,
	M249_LMG,
	LUGER_PISTOL,
	SOCOM,
	
	SHOTGUN, 
	ROCKET_LAUNCHER,
	SILENCED_PISTOL,
	SNIPER_RIFLE,
	
	#melee
	BASIC_SPEAR,
	COMBAT_KNIFE,
	CROWBAR,
	FIREAXE,
	SHOVEL,
	HATCHET,
	SLEDGEHAMMER,
	QUARTERSTAFF,
	SPEAR,
	SHIELD,
	KATANA,
	GUITAR,
	STEAK_KNIFE,

	#CLOTHING
	NEWSBOY_CAP,
	PLAID_SHIRT,
	BALACLAVA,
	CARGO_PANTS,
	CATCHERS_VEST,
	COMBAT_BOOTS,
	COWBOY_BOOTS,
	CUTOFF_PLAID_SHIRT,
	GLASSES,
	GORETEX_COAT,
	GREY_POCKET_T,
	HAT_SHIRT,
	WHITE_T_SHIRT,
	METAL_SHIRT,
	HIKING_BAG,
	HEAVY_TACTICAL_VEST,
	ARMGUARDS,
	FINGERLESS_GLOVES,
	HOODIE,
	KEVLAR_VEST,
	KILL_BILL_BODYSUIT,
	MOTORCYCLE_GLOVES,
	NIGHTVISION_GOGGLES,
	PAINTERS_OVERALLS,
	PONCHO,
	POWER_ARMOR_HELMET,
	RADSUIT_HELMET,
	RADSUIT,
	SAFETY_GLOVES,
	SNEAKERS,
	SOCKS,
	STRAITJACKET,
	SUNGLASSES,
	THERMAL_SUIT,
	THONGS,
	WELDING_GOGGLES,
	UTILITY_BELT,
	WRISTWATCH,
	WELDING_MASK,
	DICK_SHOW_SHIRT,
	BLACK_T_SHIRT,
	DUSTER,
	MOTORCYCLE_BOOTS,
	SNEAKING_SHOES,
	
	JEANS,
	
	#CONTAINERS
	BACKPACK,
	MESSENGER_BAG,
	THERMOS,
	CANTEEN,
	WASHING_BASKET,
	
	#TOOLS
	CAN_OPENER,
	CORKSCREW,
	HOE,
	
	#DEVICES
	SMALL_FLASHLIGHT,
	FLASHLIGHT_LARGE,
	WELDER,
	
	#PARTS
	ELECTRONIC_PARTS,
	MECHANICAL_PARTS,
	FLASHLIGHT_BULB,
	
	#CAR
	_30L_FUEL_TANK,
	
	#USEFUL_SCRAP
	#cloth pieces
	CLOTH_PIECE,
	LEATHER_PIECE,
	#material pieces
	METAL_PIECE,
	SOFT_PLASTIC_PIECE,
	HARD_PLASTIC_PIECE,
	#insulation
	FAUX_FUR_PIECE,
	DOWN_FEATHER,
	
	#USELESS SCRAP
	METAL_BUTTON,
	PLASTIC_BUTTON,
	ZIPPER,
	
	###DEBUG###
	DOOM_ARMOR,
	PREDATOR_ARMOR,
	PREDATOR_HELMET
}

#BONUS OPTIONS -used in crafting only
enum {
	CRAFTING_TOOL,
	REVERSIBLE
}

#battery sizes -unused
enum battery {
	FOUR_POINT_FIVE_VOLT,
	D,
	C,
	AA,
	AAA,
	AAAA,
	A23,
	NINE_VOLT,
	CELL_CR2032,
	CELL_LR44
}

#PROPERTY
enum {
	#CHEMICAL COMPOSITION
	SOLID,
	LIQUID,
	GAS,

	#MATERIAL
	WOOL,
	FELT,
	FAUX_FUR,
	DOWN,
	LEATHER,
	DENIM,
	TERRYCLOTH,
	PLASTIC,
	WOOD,
	METAL,
	FLINT,
	HIGH_CARBON,
	STEEL,
	PAPER,
	ASH,
	FOOD,
	NON_FOOD,

	#APPLICATION
	CONTAINER,
	INSULATOR,
	PARTS,
	DISINFECTANT,
	TANNIN_SOURCE,
	HEAT_SOURCE,
	FLAME_SOURCE,
	KINDLING,
	FIRE_FUEL,
	#holds water
	SEALS,

	#GENERAL SHAPE
	SHAFT,
	SHEET,
	THREAD,
	BOARD,

	#DESCRIPTOR
	LONG,
	SHORT,
	THIN,
	THICK,
	RIGID,
	NON_RIGID,
	BRITTLE,
	FLEXIBLE,
	ABSORBANT,
	EXPENDABLE,
	SHARP_EDGE,
	SHARP_POINT,
	WATERTIGHT,
	FIREPROOF,
	#batteries, energy weapons etc
	HOLDS_CHARGE,
	
	#REACTIONS
	MELTS,
	FLAMMABLE,
	EXPLODES_IN_FIRE,
	

	#SIZE
	#granular, ie: sand
	VERY_TINY,
	#can fit multiple in hand, ie: small batteries
	TINY,
	#fits comfortably in palm, ie: tennis ball
	SMALL,
	#can be held with one hand, ie: pistol
	SMALL_MEDIUM,
	#can be held with one hand, perferably in two, ie: baseball bat
	MEDIUM,
	#should be held in two hands, perhaps not comfortably though, ie: car battery
	LARGE,
	#probably don't pick this up, ie: brick wall
	ENORMOUS,

	#OTHER
	CANNOT_PICK_UP
}

#CATAGORY
enum {
	POWER,
	WEAPON,
	HARDWARE,
	WHITEGOODS,
	ELECTRONIC,
	MECHANICAL,
	BATTERY,
	FIRESTARTER,
	DEVICE,
	NATURE,
	CONSTRUCTION,
	SURVIVAL,

	#TOOLS
	TOOL,
	BLADE,
	AXE,
	STAFF,
	CLUB,
	PLYING_DEVICE,
	PRYING_DEVICE,
	SCREWING_DEVICE,
	WRENCHING_DEVICE,
	HAMMERING_DEVICE,
	DIGGING_DEVICE,
	NAIL,
	SCREW,

	#ENGINE COMPONENTS
	ENGINE,
	FUEL_SYSTEM,
	FUEL_TANK,
	FUEL_PUMP,
	FUEL_FILTER,
	FUEL_INJECTOR,
	CARBURETOR,
	ALTERNATOR,
	VOLTAGE_REGULATOR,
	COOLING_SYSTEM,
	
	#GENERAL CATAGORIES
	USEFUL_JUNK,
	USELESS_JUNK,
	
	#BULLET TYPES
	_380,
	_22LR,
	_40_SMITHANDWESSON,
	_9MM,
	_10MM,
	_45_APC,
	_38_SPECIAL,
	_5_7X28MM,
	_357_MAG,
	_30_CARBINE,
	_300_BLACKOUT,
	_7_62X39MM,
	_5_56X45MM_NATO,
	_7_62X51MM,
	_7_62X54MMR,
	_9X19MM_PARABELLUM,
	_50_BMG,
	_12GA,
	BOW_AMMO,
	CROSSBOW_AMMO,

	#BULLET MAKES
	SOFT_POINT,
	ARMOUR_PIERCING,
	LEAD_ROUND_NOSE,
	WAD_CUTTER,
	SEMI_WAD_CUTTER,
	SEMI_JACKETED,
	FULL_METAL_JACKET,
	SEMI_JACKETED_HOLLOW_POINT,
	JACKETED_HOLLOW_POINT,
	BOAT_TAIL,
	BOAT_TAIL_HOLLOW_POINT,
	SPECIAL_BULLET,

	#GUNS
	FIREARM,
	PROJECTILE_WEAPON,
	AMMO,
	
	BOW_WEAPON,
	SINGLE_FIRE_CROSSBOW,
	PISTOL,
	REVOLVER,
	RIFLE,
	LIGHT_MACHINE_GUN,

	AUTOMATIC,
	SEMI_AUTOMATIC,

	BULLET,
	MAGAZINE,
	AMMO_BELT,
	SPEEDLOADER,

	#SPECIFIC GUN/BULLET/MAGAZINE TYPES
	AK,
	STANAG,
	LUGER,

	#CLOTHING AND SLOTS
	CLOTHING,
	HAT,
	HEAD,
	FACE,
	NECK,
	SHOULDERS,
	SHOULDER,
	UPPER_ARM,
	UPPER_ARMS,
	ELBOW,
	LOWER_ARM,
	LOWER_ARMS,
	HAND,
	TORSO,
	WAIST,
	UPPER_LEG,
	UPPER_LEGS,
	KNEE,
	LOWER_LEG,
	LOWER_LEGS,
	FOOT,

	#dunno if need
	FEET,
	HANDS,
	ELBOWS,
	KNEES,
}



"""
###########
# EXAMPLE #
###########

AA Battery
	(all items must have the following keys)
{
	"ID": BATTERY_AA,
	"NAME": "AA battery",

	"WEIGHT": 0.683433,
	"VOLUME": 0.2,
	"VALUE": 50,

	"PROPERTIES": [SOLID, TINY, RIGID, PARTS, EXPENDABLE],
	"CATAGORY": [ELECTRONIC, POWER, DEVICE]
}

###################
# ADDITIONAL KEYS #
###################

PUTTIN SHIT INTO OTHER SHIT

#item that accepts other specific items in order to contribute to its use (batteries, bullets etc)
"CAN_HOLD": [
	[BATTERY_AA, 3]
]

#gun magazines and less specific items
"CAN_HOLD": [
	[[AK, MAGAZINE], 1]
]
therefore a 30 round ak magazine will have
"CAN_HOLD": [
	[[AK, BULLET], 30]
]

#if an item can conceivably break down into other, smaller objects list them here
"COMPOSITION": [
	[FLASHLIGHT_BULB, 1],
	[HARD_PLASTIC_PIECE, 5],
	[METAL_PIECE, 1],
	[ELECTRONIC_PARTS, 1],
	[CIRCUIT_BOARD, 1]
]

#if the item is capable of holding other items
"CAPACITY": 30

############
# CLOTHING #
############

used to denote the bulkiness of clothing
trying to keep numbers low ie:
boxer shorts
"ENCUMBRANCE": 1
leather jacket
"ENCUMBRANCE": 3

used to negate the outside temperature
"WARMTH": 20 (degrees)


note for CLOTHING
-----------------
item MUST contain the clothing catagory to be able to wear item ie:
"CATAGORY":
[
	CLOTHING
]
the first value of COVERS will determine the main body part associated with 
wearing the item, additional values will also benefit from the warmth of that item
t-shirt
"COVERS":
[
	TORSO, UPPER_ARMS, LOWER_ARMS
]
scarf
"COVERS":
[
	[HEAD], [NECK]
]


############
# CRAFTING #
############

#Small campfire
{
	"RESULT": SMALL_CAMPFIRE,

	"COMPONENTS": [
		#Flame source ie: lighter, matches
		{
			"TIME": 5,
			"DIFFICULTY": 0,

			"SPECIFIC":
			[
				[KINDLING_BUNDLE, [1]]
			],
			"GENERAL":
			[
				[FLAME_SOURCE, [1, TOOL]]
			]
		},
		#Flint and steel
		{
			"TIME": 1000,
			"DIFFICULTY": 20,
			"SPECIFIC":
			[
				[KINDLING_BUNDLE, [1]],
				[FLINT_AND_STEEL, [1, TOOL]]
			]
		},
		#Hand drill method
		{
			"TIME": 500,
			"DIFFICULTY": 10,
			"SKILL":
			[
				e.skill.FIRE_HAND_DRILL
			],
			"SPECIFIC":
			[
				[KINDLING_BUNDLE, [1]],
				[HAND_DRILL, [1]]
			]
		}
	]
}
"""
