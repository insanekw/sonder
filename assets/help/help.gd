extends Node

func _ready():
	pass

func _get_help(tooltip_request):
	match tooltip_request:
		#character creator tooltips		
		"char_gen_male_button":
			return "Males of the realm can excel in whatever profession they choose, whether wizardry, thievery, or the arts of war."
		"char_gen_female_button":
			return "Females of the realm can excel in whatever profession they choose, whether wizardry, thievery, or the arts of war."
		"char_gen_easy_button":
			return """EASY MODE FOR LITTLE BABIES
			
			For people new to the game, especially ones without prior experience with turn-based strategy/survival simulation/roguelike games.
			
			You have more points than everyone! A veritable God among men! Just don't go thinking you're immortal.
				"""
		"char_gen_normal_button":
			return """MEDIUM (aka normal)
			
			The way the game is meant to be played. Other people you meet in the world will have the same amount of skill points as you've been given, making this the fairest possible experience.
			
			You're still going to die, though.
			"""
		"char_gen_hard_button":
			return """HARD MODE ENABLED!!
			
			Please play on normal at least once before giving this one a go. I promise it'll be a quick game.
			
			You get less points than in normal and everyone else you meet will have more. Not by a great margin, but enough to consider possibly becoming a nicer person.
			"""
		"char_gen_extreme_button":
			return """AUSTRALIAN EXTREME
			
			Game ends when being seen by the enemy.
			
			...not really, but it may as well be. You just get fuck all points in this mode and everyone else is walking around like they're on easy mode.
			
			I put this here for people that are sick of this game after playing 500 hours of it. So, pretty much just for me.			
			"""
		"char_gen_str_label":
			return """STRENGTH
			
			They say only the strong survive in this world, but that's only technically true from a semantic standpoint. Raw power will still get you far however, just make sure you know how to wield it.
			
			Governs:
			lift/carry weight
			melee damage + block
			efficacy w/heavy ranged weapons
			climbing speed
			throwing distance
			throwing accuracy (heavy)
			
			Boosts:
			maximum health (20%)
			bluff (10%)
			"""
		"char_gen_con_label":
			return """CONSTITUTION
			
			Healthy body, healthy mind. Or so they say. It's a cold, harsh, dirty, diseased world though, and a strong immunity and fast metabolism might just save your life one day.
			
			Governs:
			maximum health (80%)
			healing rate
			resistance to disease, illness + infection
			hunger/thirst degradation levels
			
			Boosts:
			combat initiative (10%)
			maximum stamina
			bluff (10%)
			"""
		"char_gen_dex_label":
			return """DEXTERITY
			
			There's something to be said for people that are quick to point fingers, but if that finger is attached to a trigger and is also faster than the other guy's, who really cares?
			
			Governs:
			ranged weapon rate of fire
			throwing accuracy (light)
			
			Boosts:
			combat initiative (40%)
			treatment + surgery
			lockpicking
			explosives
			"""
		"char_gen_agi_label":
			return """AGILITY
			
			To be steady on one's feet in times of crisis is a true skill. A deft foot can mean the difference between life and death. Be quick or be dead.
			
			Governs:
			maximum stamina
			melee aim + dodge
			movement speed
			stealth
			
			Boosts:
			combat initiative (40%)
			"""
		"char_gen_cha_label":
			return """CHARISMA
			
			Someone once said that lack of charisma can be fatal. She might not have meant it literally, but such is the case today. Sometimes a silver tongue can accomplish what a metal crowbar cannot.
			
			Governs:
			lying
			bluff (80%)
			bartering
			influencing NPCs
			"""
		"char_gen_wis_label":
			return """WISDOM
			
			Without the intelligence to apply them, the rest of these skills would arguably be much less useful. Knowing yourself is the beginning of all wisdom.
			
			Governs:
			melee attack rate
			learning rate
			cooking
			
			Boosts:
			stealth
			lockpicking
			explosives
			
			also supplements secondary skills (MED, ELC, MEC, ENG)
			"""
		"char_gen_ldr_label":
			return """LEADERSHIP
			
			A leader is one who knows the way, goes the way, and shows the way. When there's safety in numbers and family is everything, being able to keep one from falling apart can be crucial.
			
			Governs:
			influence over groups
			ability to lead
			squad morale
			teaching rate
			"""
		"char_gen_mrk_label":
			return """MARKSMANSHIP
			
			Working guns are rare, but you still want to know how to hold one straight and which end it fires from all the same. One man with a gun can control 100 without one.
			
			Governs:
			ranged weapon accuracy
			efficacy w/conventional weapons
			more damage w/bows, crossbows + slings
			"""
		"char_gen_med_label":
			return """MEDICINE
			
			Being able to patch yourself and others up is arguably one of the most important skills to have. When a doctor cannot do good, they must be kept from doing harm.
			
			Governs:
			treatment + surgery
			diagnostic skill
			ability to see NPC health bars (70+ only)
			
			Boosts:
			healing rate
			"""
		"char_gen_elc_label":
			return """ELECTRONICS
			
			Working electronics are rare, but that just makes them all the more useful. Being able to hack through an electronic lock could be handy.
			
			Governs:
			repair + use of electronic equipment
			efficacy w/energy weapons
			
			Boosts:
			light + bladed melee weapon accuracy
			explosives
			"""
		"char_gen_mec_label":
			return """MECHANICAL
			
			Things break down a lot, and when they do, you'll be grateful to be able to glue them back together using as little of your precious duct tape as possible.
			
			Governs:
			car and gun repair
			item upgrading
			
			Boosts:
			heavy + blunt melee weapon accuracy
			lockpicking
			"""
		"char_gen_eng_label":
			return """ENGINEERING
			
			Engineering is the art of directing the great sources of power in nature for the use and convenience of man. And the fewer moving parts, the better.
			
			Governs:
			building speed + durability		
			crafting
			
			Boosts:
			melee + ranged damage
			explosives
			"""
		"char_gen_health_label":
			return """HEALTH
			86% of (80% CON + 20% STR) + 14
			
			How many kickin's you can take before you stop tickin'. When this number reaches zero, you're dead. No ifs or buts.
			
			Replenished with medical items and treatment. 
			"""
		"char_gen_stamina_label":
			return """STAMINA
			75% CON + 25% AGI
			
			Your reserve of energy. When this number hits zero, you're asleep. No ifs or buts.
			
			Replenished with food and sleep. 
			"""
		"char_gen_carry_label":
			return """CARRY WEIGHT
			STR + 150 ÷ 4
			
			How much you can comfortably carry. 
			"""
		"char_gen_healing_label":
			return """HEALING RATE
			85% CON + 15% MED
			
			Rate at which patched wounds, disease, infection etc heal at.
			"""
		"char_gen_movement_label":
			return """MOVEMENT
			6% AGI + 4
			
			Amount of tiles you can comfortably move in one combat turn.
			"""
		"char_gen_sneak_label":
			return """SNEAK
			70% AGI + 30% WIS
			
			How unlikely you are to be detected when you're actively trying to be sneaky.
			"""
		"char_gen_talk_label":
			return """TALK
			100% CHA
			
			First impressions, bartering and influencing individual NPCs.
			"""
		"char_gen_bluff_label":
			return """BLUFF
			50% CHA + 35% STR + 15% CON
			
			How likely people are to take you seriously when you start throwing your weight around verbally.
			"""
		"char_gen_lockpick_label":
			return """LOCKPICK
			80% MEC + 10% DEX + 10% WIS
			
			How likely it is you will hear a satisfying *click* after sticking a piece of metal into a keyhole.
			"""
		"char_gen_explosives_label":
			return """EXPLOSIVES
			40% ELC + 40% ENG + 10% DEX + 10% WIS
			
			How likely bombs you make, set or detonate are to not blow you up with it.
			"""
		"char_gen_initiative_label":
			return """INITIATIVE
			45% DEX + 45% AGI + 10% CON
			
			How quickly you can react to any given situation. Your reaction time, essentially. A higher number is better.
			"""
		"char_gen_melee_label":
			return """MELEE
			
			Melee encompasses all combat that is had face-to-face, whether unarmed or with melee weapons.
			"""
		"char_gen_melee_aim_label":
			return """MELEE AIM
			81% AGI + 9
			
			How likely you are to hit with any given melee attack.
			"""
		"char_gen_melee_dmg_label":
			return """MELEE DAMAGE
			15% STR + 10% ENG
			
			Bonus damage you apply on top of the weapon's damage.
			"""
		"char_gen_melee_ap_label":
			return """MELEE ACTION POINTS
			10% WIS + 6
			
			How many swings of that knife or sledgehammer you get per combat round.
			"""
		"char_gen_ranged_label":
			return """RANGED
			
			Ranged encompasses all combat with ranged weapons, whether face-to-face or at 1000 yards.
			"""
		"char_gen_ranged_aim_label":
			return """RANGED AIM
			80% MARKSMANSHIP + 10
			
			How likely you are to hit with any given ranged attack.
			"""
		"char_gen_ranged_dmg_label":
			return """RANGED DAMAGE
			10% ENG
			
			Bonus damage you apply on top of the weapon's damage. Don't worry if this number isn't that high - guns are pretty powerful by themselves.
			"""
		"char_gen_ranged_ap_label":
			return """RANGED ACTION POINTS
			10% DEX + 6
			
			How many times you can pull the trigger per combat round.
			"""
		"char_gen_medicine_label":
			return """MEDICINE
			80% MED + 10% DEX + 10% WIS
			
			Represents your knowledge and application of medicine. Applies to all doctor checks, from treatment to surgery.
			"""
		"char_gen_learn_label":
			return """LEARN
			100% WIS
			
			text
			"""
		"char_gen_electronics_label":
			return """ELECTRONICS
			TODO:
			
			text
			"""
		"char_gen_mechanical_label":
			return """MECHANICAL
			TODO:
			
			text
			"""
		"char_gen_engineering_label":
			return """ENGINEERING
			TODO:
			
			text
			"""
		"char_gen_teach_label":
			return """TEACH
			100% LDR
			
			text
			"""
		"char_gen_dodge_label":
			return """DODGE
			(80% AGI + 80% DEX) ÷ 2
			
			text
			"""
		"char_gen_block_label":
			return """BLOCK
			(80% STR + 80% CON) ÷ 2
			
			text
			"""
		"char_gen_new_char_button":
			return "Add a new character to your party."
		"char_gen_load_char_button":
			return "Type the name of the character you want to load and click here."
		"char_gen_save_char_button":
			return """Saves your character for later use. 
			
			[WARNING]: Characters of the same name will overwrite each other.."""
		"char_gen_manager":
			return "Never really one to take orders, you got a job as a manager and acquired a knack for telling people what to do, as well as picking up a few people skills along the way."
		"char_gen_politician":
			return "The government might have disbanded, but the need for chain of command is still deeply ingrained in people. Your skills at schmoozing and acting like you're in charge (even if you're not) may still serve you well."
		"char_gen_stockbroker":
			return ""
		"char_gen_personal loaner":
			return ""
		"char_gen_lawyer":
			return ""
		"char_gen_judge":
			return ""
		"char_gen_army":
			return ""
		"char_gen_secret service":
			return ""
		"char_gen_army":
			return ""
		"char_gen_police person":
			return ""
		"char_gen_firefighter":
			return ""
		"char_gen_ambulance driver":
			return ""
		"char_gen_biologist":
			return ""
		"char_gen_physicist":
			return ""
		"char_gen_chemist":
			return ""
		"char_gen_mathmatician":
			return ""
		"char_gen_doctor":
			return ""
		"char_gen_nurse":
			return ""
		"char_gen_registered nurse":
			return ""
		"char_gen_surgeon":
			return ""
		"char_gen_pilot":
			return ""
		"char_gen_truck driver":
			return ""
		"char_gen_train driver":
			return ""
		"char_gen_forklift driver":
			return ""
		"char_gen_pick packer":
			return ""
		"char_gen_bus driver":
			return ""
		"char_gen_musician":
			return ""
		"char_gen_comedian":
			return ""
		"char_gen_waiter":
			return ""
		"char_gen_checkout operator":
			return ""
		"char_gen_electrician":
			return ""
		"char_gen_mechanic":
			return ""
		"char_gen_engineer":
			return ""
		"char_gen_garbage person":
			return ""
		"char_gen_receptionist":
			return ""
		"char_gen_athlete":
			return ""
		"char_gen_sport shooter":
			return ""
		"char_gen_hunter":
			return ""
		"char_gen_farmer":
			return ""
		"char_gen_personal trainer":
			return ""
		"char_gen_lecturer":
			return ""
		"char_gen_teacher":
			return ""
		"char_gen_tutor":
			return ""
		"char_gen_drug dealer":
			return ""
		"char_gen_assassin":
			return ""
		"char_gen_shoplifter":
			return ""
		"char_gen_help_label":
			return "Not this text, idiot."
		_:
			return "[NO HELP TEXT ASSIGNED OR MISSING POINTER]"
