extends Node

# Getter for perks/drawbacks -takes int ID, shouldn't need typeof checker anymore
func _get_trait(identifier):
	for trait in traits:
		if trait["ID"] == identifier:
			return trait

# Gets random perks and drawbacks for random character generation 
func _get_random_traits(remaining_points, everyone_sucks : = false):
	var traits_array = []
	var new_trait
	var total_allowed_traits = g.max_perks_allowed

	var random

	# initial drawback, everyone gets at least one
	if everyone_sucks:
		new_trait = _get_random_drawback()
		traits_array.append(new_trait["ID"])
		remaining_points -= new_trait["COST"]
		total_allowed_traits -= 1
		
	# Make sure we don't exceed the maximum number of allowed total perks/drawbacks
	while total_allowed_traits > 0 and remaining_points > 0:
		random = g._get_random_1_100()
		# either get a perk or a drawback
		if random < 50:
			new_trait = _get_random_perk_under_amount(remaining_points)
			# check there isn't an opposing drawback or that perk isn't already in there
			while traits_array.has(new_trait["ID"]) or _is_opposed(traits_array, new_trait):
				new_trait = _get_random_perk_under_amount(remaining_points)
		
			traits_array.append(new_trait["ID"])
			remaining_points -= new_trait["COST"]
			total_allowed_traits -= 1
		else:
			new_trait = _get_random_drawback()
			# check there isn't an opposing perk or that drawback isn't already in there
			while traits_array.has(new_trait["ID"]) or _is_opposed(traits_array, new_trait):
				new_trait = _get_random_drawback()
		
			traits_array.append(new_trait["ID"])
			remaining_points -= new_trait["COST"]
			total_allowed_traits -= 1
	
	# invoke _random_drawback_delete until we're either even or can buy another perk
	while remaining_points < 0:
		new_trait =_get_random_drawback_for_deletion(traits_array)
		traits_array.erase(new_trait)
		remaining_points += new_trait["COST"]
		total_allowed_traits += 1
	
	# buy a perk if there's any remaining points and aren't at max limit
	while remaining_points > 0 and total_allowed_traits > 0:
		new_trait = _get_random_perk_under_amount(remaining_points)
		while traits_array.has(new_trait["ID"]) or _is_opposed(traits_array, new_trait):
			new_trait = _get_random_perk_exact_amount(remaining_points)
		
		traits_array.append(new_trait["ID"])
		remaining_points -= new_trait["COST"]
		total_allowed_traits -= 1

	return traits_array


func _get_random_perk():
	var perk = traits[g._get_random(1, traits.size()) - 1]
	
	if g.boosted_traits_only:
		while perk["COST"] < 0 or !perk.has("EFFECT"):
			perk = traits[g._get_random(1, traits.size()) - 1]
	else:
		while perk["COST"] < 0:
			perk = traits[g._get_random(1, traits.size()) - 1]
	
	return perk

# Gets random drawback
func _get_random_drawback():
	var drawback = traits[g._get_random(1, traits.size()) - 1]
	
	if g.boosted_traits_only:
		while drawback["COST"] > 0 or !drawback.has("EFFECT"):
			drawback = traits[g._get_random(1, traits.size()) - 1]
	else:
		while drawback["COST"] > 0:
			drawback = traits[g._get_random(1, traits.size()) - 1]
	
	return drawback

func _get_random_perk_exact_amount(remaining_points):
	var perk = _get_random_perk()
	
	while perk == null or perk["COST"] != remaining_points or !perk.has("EFFECT"):
		perk = _get_random_perk()
	
	return perk

func _get_random_perk_under_amount(remaining_points):
	var perk = _get_random_perk()
	
	while perk == null or perk["COST"] > remaining_points or !perk.has("EFFECT"):
		perk = _get_random_perk()
	
	return perk

func _get_random_drawback_for_deletion(traits_array):
	var drawbacks = []
	for drawback in traits_array:
		if _get_trait(drawback)["COST"] < 0:
			drawbacks.append(drawback)
	var random = g._get_random(1, drawbacks.size()) - 1
	return _get_trait(drawbacks[random])["ID"] if drawbacks != [] else null

func _get_highest_perk_cost():
	var cost = 0
	
	for trait in traits:
		if trait["COST"] > 0 and trait["COST"] > cost:
			cost = trait["COST"]
	
	return cost


func _sort():
	traits.sort_custom(self, "_sort_alphabetically")

func _sort_alphabetically(a, b):
	if (str(a["NAME"])) < (str(b["NAME"])):
		return true
	else:
		return false


func _get_cost_description(new_trait):
	if new_trait.has("COST"):
		return str(new_trait["COST"]) + " perk point" + ("s" if new_trait["COST"] > 1 or new_trait["COST"] < -1 else "")
	else:
		return "[No cost assigned]"


func _get_effect_description(new_trait):
	if new_trait.has("EFFECT"):
		var description = ""
		
		var i = 1
		while i < new_trait["EFFECT"].size():
			if new_trait["EFFECT"][i] > 0:
				description += "+"
			description += str(new_trait["EFFECT"][i]) 
			description += " "
			description += s._get_skill_name(new_trait["EFFECT"][i - 1])
			
			if (i) >= new_trait["EFFECT"].size() - 1:
				break
			else:
				description += ", "
			
			i += 2
		
		return description
	else:
		return ""

func _is_opposed(current_traits, trait):
	if trait.has("OPPOSITE"):
		for opposite in trait["OPPOSITE"]:
			if current_traits.has(opposite):
				return true
	return false

# Informs player opposing trait will be barred if current one is picked in char gen screen
func _get_opposite_description(trait):
	var description = ""
	if trait.has("OPPOSITE"):
		description += "disallows "
		description += _get_trait(trait["OPPOSITE"])["NAME"]
		description += " drawback" if trait["COST"] > 0 else " perk"
	return description


var traits = [

## PERKS

{
	"ID": e.traits.ACCOMPLISHED_SLEEPER,
	"NAME": "accomplished sleeper",
	"OPPOSITE": [e.traits.LIGHT_SLEEPER],
	"COST": 1
},
{
	"ID": e.traits.ADDICTION_RESISTANT,
	"NAME": "addiction resistant",
	"OPPOSITE": [e.traits.ADDICTIVE_PERSONALITY],
	"COST": 1
},
{
	"ID": e.traits.ANIMAL_EMPATHY,
	"NAME": "animal empathy",
	"OPPOSITE": [e.traits.ANIMAL_DISCORD],
	"COST": 1
},
{
	"ID": e.traits.BOOKWORM,
	"NAME": "bookworm",
	"OPPOSITE": [e.traits.HATES_BOOKS],
	"COST": 1
},
{
	"ID": e.traits.CANNIBAL,
	"NAME": "cannibal",
	"COST": 1
},
{
	"ID": e.traits.DISEASE_RESISTANT,
	"NAME": "disease resistant",
	"COST": 1
},
{
	"ID": e.traits.DRUNKEN_MASTER,
	"NAME": "drunken master",
	"COST": 1
},
{
	"ID": e.traits.MEDICAL_MYSTERY,
	"NAME": "medical mystery",
	"OPPOSITE": [e.traits.SICKLY],
	"EFFECT": [e.HEALING_STAT, 10],
	"COST": 2
},
{
	"ID": e.traits.FAST_LEARNER,
	"NAME": "fast learner",
	"OPPOSITE": [e.traits.SLOW_LEARNER],
	"EFFECT": [e.LEARN_STAT, 15],
	"COST": 1
},
{
	"ID": e.traits.FAST_READER,
	"NAME": "fast reader",
	"OPPOSITE": [e.traits.SLOW_READER],
	"COST": 1
},
{
	"ID": e.traits.FLEET_FOOTED,
	"NAME": "fleet-footed",
	"OPPOSITE": [e.traits.BAD_KNEES],
	"EFFECT": [e.MOVEMENT_STAT, 1],
	"COST": 2
},
{
	"ID": e.traits.GOOD_HEARING,
	"NAME": "good hearing",
	"OPPOSITE": [e.traits.POOR_HEARING],
	"COST": 1
},
{
	"ID": e.traits.GOOD_MEMORY,
	"NAME": "good memory",
	"OPPOSITE": [e.traits.FORGETFUL],
	"COST": 1
},
{
	"ID": e.traits.GOURMAND,
	"NAME": "gourmand",
	"COST": 1
},
{
	"ID": e.traits.GUN_TRAINING,
	"NAME": "gun training",
	"EFFECT": [e.RANGED_AP_STAT, 2, e.RANGED_AIM_STAT, 10, e.RANGED_DMG_STAT, 2],
	"COST": 3
},
{
	"ID": e.traits.HIGH_ADRENALINE,
	"NAME": "high adrenaline",
	"COST": 1
},
{
	"ID": e.traits.INDEFATIGABLE,
	"NAME": "indefatigable",
	"OPPOSITE": [e.traits.SHORT_OF_BREATH],
	"EFFECT": [e.STAMINA_STAT, 15],
	"COST": 3
},
{
	"ID": e.traits.INFECTION_RESISTANT,
	"NAME": "infection resistant",
	"OPPOSITE": [e.traits.INFECTION_PRONE],
	"COST": 1
},
{
	"ID": e.traits.LESS_SLEEP,
	"NAME": "less sleep",
	"OPPOSITE": [e.traits.SLEEPY],
	"COST": 1
},
{
	"ID": e.traits.LIGHT_EATER,
	"NAME": "light eater",
	"OPPOSITE": [e.traits.FAST_METABOLISM],
	"COST": 1
},
{
	"ID": e.traits.PUSSYFOOT,
	"NAME": "pussyfoot",
	"OPPOSITE": [e.traits.CLUMSY],
	"EFFECT": [e.SNEAK_STAT, 10],
	"COST": 2
},
{
	"ID": e.traits.MARTIAL_ARTS_TRAINING,
	"NAME": "martial arts training",
	"COST": 3
},
{
	"ID": e.traits.MASOCHIST,
	"NAME": "masochist",
	"COST": 1
},
{
	"ID": e.traits.MELEE_WEAPON_TRAINING,
	"NAME": "melee weapon training",
	"EFFECT": [e.MELEE_AP_STAT, 2, e.MELEE_AIM_STAT, 10, e.MELEE_DMG_STAT, 2],
	"COST": 3
},
{
	"ID": e.traits.NIGHT_VISION,
	"NAME": "night vision",
	"COST": 1
},
{
	"ID": e.traits.OPTISMIST,
	"NAME": "optimist",
	"COST": 1
},
{
	"ID": e.traits.PACKMULE,
	"NAME": "packmule",
	"COST": 1
},
{
	"ID": e.traits.PAIN_RESISTANT,
	"NAME": "pain resistant",
	"COST": 2
},
{
	"ID": e.traits.PRETTY,
	"NAME": "pretty",
	"COST": 1
},
{
	"ID": e.traits.PSYCHOPATH,
	"NAME": "psychopath",
	"COST": 1
},
{
	"ID": e.traits.QUICK,
	"NAME": "quick",
	"OPPOSITE": [e.traits.SLOW],
	"EFFECT": [e.MELEE_AP_STAT, 5, e.RANGED_AP_STAT, 5],
	"COST": 3
},
{
	"ID": e.traits.SCOUT,
	"NAME": "scout",
	"COST": 1
},
{
	"ID": e.traits.SELF_AWARE,
	"NAME": "self-aware",
	"COST": 1
},
{
	"ID": e.traits.SELF_DEFENSE_CLASSES,
	"NAME": "self-defense classes",
	"COST": 1
},
{
	"ID": e.traits.SHAOLIN_ADEPT,
	"NAME": "shaolin adept",
	"COST": 5
},
{
	"ID": e.traits.SKILLED_LIAR,
	"NAME": "skilled liar",
	"COST": 1
},
{
	"ID": e.traits.SPIRITUAL,
	"NAME": "spiritual",
	"COST": 1
},
{
	"ID": e.traits.STRONG_BACK,
	"NAME": "strong back",
	"OPPOSITE": [e.traits.WEAK_BACK],
	"EFFECT": [e.CARRY_STAT, 20],
	"COST": 1
},
{
	"ID": e.traits.STYLISH,
	"NAME": "stylish",
	"COST": 1
},
{
	"ID": e.traits.SUBSTANCE_TOLERANCE,
	"NAME": "substance tolerance",
	"COST": 1
},
{
	"ID": e.traits.TERRIFYING,
	"NAME": "terrifying",
	"COST": 1
},
{
	"ID": e.traits.THICK_SKINNED,
	"NAME": "thick-skinned",
	"COST": 1
},
{
	"ID": e.traits.HARD_TO_KILL,
	"NAME": "hard to kill",
	"OPPOSITE": [e.traits.FLIMSY],
	"EFFECT": [e.HEALTH_STAT, 10],
	"COST": 1
},
{
	"ID": e.traits.TOUGH_FEET,
	"NAME": "tough feet",
	"COST": 1
},
{
	"ID": e.traits.WEAK_SCENT,
	"NAME": "weak scent",
	"COST": 1
},
{
	"ID": e.traits.SMOOTH_OPERATOR,
	"NAME": "smooth operator",
	"OPPOSITE": [e.traits.ABRASIVE],
	"EFFECT": [e.TALK_STAT, 15],
	"COST": 1
},
{
	"ID": e.traits.BULLY,
	"NAME": "bully",
	"OPPOSITE": [e.traits.MEAGRE],
	"EFFECT": [e.BLUFF_STAT, 10],
	"COST": 1
},
{
	"ID": e.traits.MASTER_OF_UNLOCKING,
	"NAME": "master of unlocking",
	#"OPPOSITE": [[],#TODO:
	"EFFECT": [e.LOCKPICK_STAT, 15],
	"COST": 2
},
{
	"ID": e.traits.HIPPOCRATIC,
	"NAME": "hippocratic",
	"OPPOSITE": [e.traits.BUTCHER],
	"EFFECT": [e.MEDICINE_STAT, 15],
	"COST": 3
},
{
	"ID": e.traits.GEARHEAD,
	"NAME": "gearhead",
	#"OPPOSITE": [[],#TODO:
	"EFFECT": [e.MECHANICAL_STAT, 10],
	"COST": 2
},
{
	"ID": e.traits.SPARK_OF_GENIUS,
	"NAME": "spark of genius",
	#"OPPOSITE": [[],#TODO:
	"EFFECT": [e.ELECTRONICS_STAT, 10],
	"COST": 2
},
{
	"ID": e.traits.SAVANT,
	"NAME": "savant",
	"OPPOSITE": [e.traits.BIT_SLOW],
	"EFFECT": [e.LEARN_STAT, 15],
	"COST": 3
},
{
	"ID": e.traits.PROFESSOR,
	"NAME": "professor",
	"OPPOSITE": [e.traits.PEDAGOGUE],
	"EFFECT": [e.TEACH_STAT, 15],
	"COST": 3
},
{
	"ID": e.traits.SPRY,
	"NAME": "spry",
	"OPPOSITE": [e.traits.DELIBERATE],
	"EFFECT": [e.INITIATIVE_STAT, 10],
	"COST": 2
},
{
	"ID": e.traits.STURDY,
	"NAME": "sturdy",
	"OPPOSITE": [e.traits.FEEBLE],
	"EFFECT": [e.BLOCK_STAT, 5],
	"COST": 1
},
{
	"ID": e.traits.FLOATING_BUTTERFLY,
	"NAME": "floating butterfly",
	#"OPPOSITE": [[],#TODO:
	"EFFECT": [e.DODGE_STAT, 10],
	"COST": 2
},
{
	"ID": e.traits.PINCH_HITTER,
	"NAME": "pinch hitter",
	"OPPOSITE": [e.traits.DUFFER],
	"EFFECT": [e.MELEE_AIM_STAT, 10],
	"COST": 1
},
{
	"ID": e.traits.BAM_BAM,
	"NAME": "bam bam",
	"OPPOSITE": [e.traits.NO_FOLLOW_THROUGH],
	"EFFECT": [e.MELEE_DMG_STAT, 10],
	"COST": 3
},
{
	"ID": e.traits.ONSLAUGHT,
	"NAME": "onslaught",
	#"OPPOSITE": [[],#TODO:
	"EFFECT": [e.MELEE_AP_STAT, 3],
	"COST": 2
},
{
	"ID": e.traits.DEAD_EYE,
	"NAME": "dead eye",
	"OPPOSITE": [e.traits.BUNG_EYE],
	"EFFECT": [e.RANGED_AIM_STAT, 10],
	"COST": 1
},
{
	"ID": e.traits.BULLET_SURGEON,
	"NAME": "bullet surgeon",
	"OPPOSITE": [e.traits.SHOOT_TO_WOUND],
	"EFFECT": [e.RANGED_DMG_STAT, 5],
	"COST": 2
},
{
	"ID": e.traits.FASTEST_DRAW,
	"NAME": "fastest draw",
	"OPPOSITE": [e.traits.SLOWEST_DRAW],
	"EFFECT": [e.RANGED_AP_STAT, 3],
	"COST": 3
},

## DRAWBACKS

{
	"ID": e.traits.ADDICTIVE_PERSONALITY,
	"NAME": "addictive personality",
	"OPPOSITE": [e.traits.ADDICTION_RESISTANT],
	"COST": -1
},
{
	"ID": e.traits.FLIMSY,
	"NAME": "flimsy",
	"OPPOSITE": [e.traits.HARD_TO_KILL],
	"EFFECT": [e.HEALTH_STAT, -10],
	"COST": -1
},
{
	"ID": e.traits.ALBINO,
	"NAME": "albino",
	"COST": -4
},
{
	"ID": e.traits.ANIMAL_DISCORD,
	"NAME": "animal discord",
	"OPPOSITE": [e.traits.ANIMAL_EMPATHY],
	"COST": -1
},
{
	"ID": e.traits.ASTHMATIC,
	"NAME": "asthmatic",
	"COST": -2
},
{
	"ID": e.traits.BAD_KNEES,
	"NAME": "bad knees",
	"OPPOSITE": [e.traits.FLEET_FOOTED],
	"EFFECT": [e.MOVEMENT_STAT, -1],
	"COST": -2
},
{
	"ID": e.traits.BAD_TEMPER,
	"NAME": "bad temper",
	"COST": -1
},
{
	"ID": e.traits.CHEMICAL_IMBALANCE,
	"NAME": "chemical imbalance",
	"COST": -3
},
{
	"ID": e.traits.CLUMSY,
	"NAME": "clumsy",
	"OPPOSITE": [e.traits.PUSSYFOOT],
	"EFFECT": [e.SNEAK_STAT, -10],
	"COST": -1
},
{
	"ID": e.traits.COMPULSIVE_LIAR,
	"NAME": "compulsive liar",
	"COST": -1
},
{
	"ID": e.traits.DISORGANISED,
	"NAME": "disorganised",
	"COST": -2
},
{
	"ID": e.traits.FAR_SIGHTED,
	"NAME": "far sighted",
	"COST": -2
},
{
	"ID": e.traits.FAST_METABOLISM,
	"NAME": "fast metabolism",
	"COST": -2
},
{
	"ID": e.traits.FORGETFUL,
	"NAME": "forgetful",
	"COST": -2
},
{
	"ID": e.traits.GLASS_JAW,
	"NAME": "glass jaw",
	"COST": -3
},
{
	"ID": e.traits.GRAIN_INTOLERANCE,
	"NAME": "grain intolerance",
	"COST": -1
},
{
	"ID": e.traits.HATES_BOOKS,
	"NAME": "hates books",
	"OPPOSITE": [e.traits.BOOKWORM],
	"COST": -1
},
{
	"ID": e.traits.HATES_FRUIT,
	"NAME": "hates fruit",
	"COST": -1
},
{
	"ID": e.traits.HATES_VEGETABLES,
	"NAME": "hates vegetables",
	"COST": -1
},
{
	"ID": e.traits.HEAVY_SLEEPER,
	"NAME": "heavy sleeper",
	"COST": -2
},
{
	"ID": e.traits.HIGH_THIRST,
	"NAME": "high thirst",
	"COST": -2
},
{
	"ID": e.traits.HOARDER,
	"NAME": "hoarder",
	"COST": -1
},
{
	"ID": e.traits.ILLITERATE,
	"NAME": "illiterate",
	"COST": -3
},
{
	"ID": e.traits.INFECTION_PRONE,
	"NAME": "infection prone",
	"COST": -2
},
{
	"ID": e.traits.INSOMNIAC,
	"NAME": "insomniac",
	"COST": -2
},
{
	"ID": e.traits.JITTERY,
	"NAME": "jittery",
	"COST": -2
},
{
	"ID": e.traits.JUNKFOOD_INTOLERANCE,
	"NAME": "junkfood intolerance",
	"COST": -1
},
{
	"ID": e.traits.LACTOSE_INTOLERANCE,
	"NAME": "lactose intolerance",
	"COST": -1
},
{
	"ID": e.traits.LIGHTWEIGHT,
	"NAME": "lightweight",
	"COST": -1
},
{
	"ID": e.traits.LIGHT_SLEEPER,
	"NAME": "light sleeper",
	"OPPOSITE": [e.traits.ACCOMPLISHED_SLEEPER],
	"COST": -1
},
{
	"ID": e.traits.LOUD_FOOTSTEPS,
	"NAME": "loud footsteps",
	"COST": -2
},
{
	"ID": e.traits.MEAT_INTOLERANCE,
	"NAME": "meat intolerance",
	"COST": -1
},
{
	"ID": e.traits.MOOD_SWINGS,
	"NAME": "mood swings",
	"COST": -1
},
{
	"ID": e.traits.NARCOLEPTIC,
	"NAME": "narcoleptic",
	"COST": -5
},
{
	"ID": e.traits.NEAR_SIGHTED,
	"NAME": "near-sighted",
	"COST": -2
},
{
	"ID": e.traits.PACIFIST,
	"NAME": "pacifist",
	"COST": -1
},
{
	"ID": e.traits.PAIN_SENSITIVE,
	"NAME": "pain sensitive",
	"COST": -3
},
{
	"ID": e.traits.POOR_HEARING,
	"NAME": "poor hearing",
	"COST": -2
},
{
	"ID": e.traits.PYROMANIAC,
	"NAME": "pyromaniac",
	"COST": -1
},
{
	"ID": e.traits.SCHIZOPHRENIC,
	"NAME": "schizophrenic",
	"COST": -4
},
{
	"ID": e.traits.SLEEPY,
	"NAME": "sleepy",
	"COST": -2
},
{
	"ID": e.traits.SICKLY,
	"NAME": "sickly",
	"OPPOSITE": [e.traits.MEDICAL_MYSTERY],
	"EFFECT": [e.HEALING_STAT, -10],
	"COST": -2
},
{
	"ID": e.traits.SLOW_LEARNER,
	"NAME": "slow learner",
	"COST": -1
},
{
	"ID": e.traits.SLOW_READER,
	"NAME": "slow reader",
	"COST": -1
},
{
	"ID": e.traits.SQUEAMISH,
	"NAME": "squeamish",
	"COST": -1
},
{
	"ID": e.traits.STRONG_SCENT,
	"NAME": "strong scent",
	"COST": -1
},
{
	"ID": e.traits.THIN_SKINNED,
	"NAME": "thin-skinned",
	"COST": -1
},
{
	"ID": e.traits.TRIGGER_HAPPY,
	"NAME": "trigger-happy",
	"COST": -1
},
{
	"ID": e.traits.UGLY,
	"NAME": "ugly",
	"COST": -1
},
{
	"ID": e.traits.WEAK_STOMACH,
	"NAME": "weak stomach",
	"COST": -1
},
{
	"ID": e.traits.WEAK_BACK,
	"NAME": "weak back",
	"OPPOSITE": [e.traits.STRONG_BACK],
	"EFFECT": [e.CARRY_STAT, -20],
	"COST": -1
},
{
	"ID": e.traits.WOOL_ALLERGY,
	"NAME": "wool allergy",
	"COST": -1
},
{
	"ID": e.traits.ABRASIVE,
	"NAME": "abrasive",
	"OPPOSITE": [e.traits.SMOOTH_OPERATOR],
	"EFFECT": [e.TALK_STAT, -15],
	"COST": -2
},
{
	"ID": e.traits.MEAGRE,
	"NAME": "meagre",
	"OPPOSITE": [e.traits.BULLY],
	"EFFECT": [e.BLUFF_STAT, -10],
	"COST": -1
},
{
	"ID": e.traits.BUTCHER,
	"NAME": "butcher",
	"OPPOSITE": [e.traits.HIPPOCRATIC],
	"EFFECT": [e.MEDICINE_STAT, -15],
	"COST": -3
},
{
	"ID": e.traits.BIT_SLOW,
	"NAME": "bit slow",
	"OPPOSITE": [e.traits.SAVANT],
	"EFFECT": [e.LEARN_STAT, -15],
	"COST": -2
},
{
	"ID": e.traits.PEDAGOGUE,
	"NAME": "pedagogue",
	"OPPOSITE": [e.traits.PROFESSOR],
	"EFFECT": [e.TEACH_STAT, -10],
	"COST": -1
},
{
	"ID": e.traits.DELIBERATE,
	"NAME": "deliberate",
	"OPPOSITE": [e.traits.SPRY],
	"EFFECT": [e.INITIATIVE_STAT, -10],
	"COST": -2
},
{
	"ID": e.traits.FEEBLE,
	"NAME": "feeble",
	"OPPOSITE": [e.traits.STURDY],
	"EFFECT": [e.BLOCK_STAT, -10],
	"COST": -2
},
{
	"ID": e.traits.SHORT_OF_BREATH,
	"NAME": "short of breath",
	"OPPOSITE": [e.traits.INDEFATIGABLE],
	"EFFECT": [e.STAMINA_STAT, -10],
	"COST": -2
},
{
	"ID": e.traits.NO_FOLLOW_THROUGH,
	"NAME": "no follow-through",
	"OPPOSITE": [e.traits.BAM_BAM],
	"EFFECT": [e.MELEE_DMG_STAT, -10],
	"COST": -1
},
{
	"ID": e.traits.BUNG_EYE,
	"NAME": "bung eye",
	"OPPOSITE": [e.traits.DEAD_EYE],
	"EFFECT": [e.RANGED_AIM_STAT, -10],
	"COST": -1
},
{
	"ID": e.traits.SHOOT_TO_WOUND,
	"NAME": "shoot to wound",
	"OPPOSITE": [e.traits.BULLET_SURGEON],
	"EFFECT": [e.RANGED_DMG_STAT, -3],
	"COST": -2
},
{
	"ID": e.traits.SLOWEST_DRAW,
	"NAME": "slowest draw",
	"OPPOSITE": [e.traits.FASTEST_DRAW],
	"EFFECT": [e.RANGED_AP_STAT, -5],
	"COST": -3
},
{
	"ID": e.traits.DUFFER,
	"NAME": "duffer",
	"OPPOSITE": [e.traits.PINCH_HITTER],
	"EFFECT": [e.MELEE_AIM_STAT, -10],
	"COST": -1
},
{
	"ID": e.traits.SLOW,
	"NAME": "slow",
	"OPPOSITE": [e.traits.QUICK],
	"EFFECT": [e.MELEE_AP_STAT, -5, e.RANGED_AP_STAT, -5],
	"COST": -3
}
]
