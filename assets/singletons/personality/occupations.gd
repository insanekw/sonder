extends Node

var one_point_count = 0
var two_point_count = 0
var three_point_count = 0

func _ready():	
	for occupation in occupations:
		# UNKNOWN: why is this here? was i just testing shit and forgot to delete?
		_get_occupation_name(occupation)
		_get_occupation_score(occupation)
		_get_bonus_description(occupation)
		
		# debug: goes through each occupation to count how many 1, 2 and 3 point jobs there are respectively for balance reasons
		match _get_occupation_score(occupation):
			1:
				one_point_count += 1
			2:
				two_point_count += 1
			3:
				three_point_count += 1

# Takes occupation NAME or ID and 
func _get_occupation(identifier):
	for occupation in occupations:
		# take int ["ID"] and return full occupation details
		if occupation["ID"] == identifier:
			return occupation

# Gets correct job title based on unit's gender if applicable
func _get_name_by_id(occupation_id, gender):
	for occupation in occupations:
		if occupation["ID"] == occupation_id:
			if occupation["NAME"].size() == 1:
				# generic non-gendered job ie: pilot
				return occupation["NAME"][0]
			else:
				# male job name ie: police man
				return (occupation["NAME"][0 if gender == e.genders.MALE 
				# female job name ie: police woman
				else 1 if gender == e.genders.FEMALE else 
				# other ie: police person
				2])

# takes an array of occupations and returns an array of strings containing the correct occupation names 
#-used for character select screen and character sheets
func _get_occupation_strings(occupation_array : Array, gender : int) -> Array:
	var string_array = []
	for occupation in occupation_array:
		string_array.append(_get_name_by_id(occupation["ID"], gender))
	return string_array

# not sure if i need the next two since we can now call the full occupation and just get ID and NAME from that directly
func _get_occupation_id(occupation):
	return occupation["ID"]
func _get_occupation_name(occupation):
	return occupation["NAME"]

# gets any stat bonuses from the occupation and display them as a string
#-used for character select screen and character sheets
func _get_bonus_description(occupation):
	if occupation.has("BONUS"):
		var description = ""
		
		var i = 1
		while i < occupation["BONUS"].size():
			description += "+" 
			description += str(occupation["BONUS"][i]) 
			description += " "
			description += s._get_stat_name(occupation["BONUS"][i - 1])
			
			
			if (i) >= occupation["BONUS"].size() - 1:
				break
			else:
				description += ", "
			
			# +2 because "BONUS" Array reads: [STAT1, 5, STAT2, 5]
			i += 2
		
		return description

# used to determine the 'buy' value of an occupation based on the skill boost that it provides
func _get_occupation_score(occupation):
	if occupation.has("BONUS"):
		var total_value = 0
		
		var i = 1
		while i < occupation["BONUS"].size():
			total_value += occupation["BONUS"][i]
			i += 2
		
		#ie: a job that adds +5 STR will cost 1 point, +10 STR 2 points, etc
		return int(total_value / 5)


func _get_random_occupation(cost):
	var occupation
	occupation = occupations[(g._get_random(1,occupations.size()) - 1)]
	
	# dunno if this is the ideal way to do it but for loops don't have a where clause
	while _get_occupation_score(occupation) > cost:
		occupation = occupations[(g._get_random(1,occupations.size()) - 1)]
		
	return occupation


func _get_random_occupations(total_points):
	var occupations = []
	while total_points > 0:
		var random = g._get_random(1,total_points)
		var occupation = _get_random_occupation(random)
		if !occupations.has(occupation):
			occupations.append(occupation["ID"])
		total_points -= _get_occupation_score(occupation)
	
	return occupations


func _sort():
	occupations.sort_custom(self, "_sort_alphabetically")


func _sort_alphabetically(a, b):
	if (str(a["NAME"])) < (str(b["NAME"])):
		return true
	else:
		return false


# debug
func _point_counter(occupation):
	var i = _get_occupation_score(occupation)
	match i:
		1:
			one_point_count += 1
		2:
			two_point_count += 1
		3:
			three_point_count += 1


var occupations = [
	{
		"ID": e.occupations.MANAGER,
		"PREFIX": "a",
		"NAME": ["manager"],
		"SECTOR": [e.occupation_sectors.MANAGEMENT],
		"BONUS": [e.LDR, 10, e.CHA, 5],
		"TYPE": ["Regional", "District", "Supervising"]
	},
	{
		"ID": e.occupations.POLITICIAN,
		"PREFIX": "a",
		"NAME": ["politician"],
		"SECTOR": [e.occupation_sectors.POLITICS],
		"BONUS": [e.CHA, 10, e.LDR, 5]
	},
	{
		"ID": e.occupations.STOCKBROKER,
		"PREFIX": "a",
		"NAME": ["stockbroker"],
		"SECTOR": [e.occupation_sectors.FINANCE],
		"BONUS": [e.CHA, 10]
	},
	{
		"ID": e.occupations.PERSONAL_LOANER,
		"PREFIX": "a",
		"NAME": ["personal loaner"],
		"SECTOR": [e.occupation_sectors.FINANCE, e.occupation_sectors.SELF_EMPLOYED],
		"BONUS": [e.CHA, 10]
	},
	{
		"ID": e.occupations.LAWYER,
		"PREFIX": "a",
		"NAME": ["lawyer"],
		"SECTOR": [e.occupation_sectors.LAW],
		"BONUS": [e.CHA, 5, e.WIS, 5]
	},
	{
		"ID": e.occupations.JUDGE,
		"PREFIX": "a",
		"NAME": ["judge"],
		"SECTOR": [e.occupation_sectors.LAW],
		"BONUS": [e.CHA, 10, e.LDR, 5]
	},
	{
		"ID": e.occupations.SECRET_SERVICE,
		"PREFIX": "in the",
		"NAME": ["secret service"],
		"CAPITALISE": true,
		"SECTOR": [e.occupation_sectors.DEFENSE],
		"BONUS": [e.LDR, 5, e.MRK, 5, e.MED, 5]
	},
	{
		"ID": e.occupations.ARMY,
		"PREFIX": "in the",
		"NAME": ["army"],
		"CAPITALISE": true,
		"SECTOR": [e.occupation_sectors.DEFENSE],
		"BONUS": [e.STR, 5, e.MRK, 5, e.MED, 5]
	},
	{
		"ID": e.occupations.POLICE_PERSON,
		"PREFIX": "a",
		"NAME": ["police man", "police woman", "police person"],
		"CAPITALISE": true,
		"SECTOR": [e.occupation_sectors.POLICE],
		"BONUS": [e.LDR, 5, e.MRK, 5, e.MED, 5]
	},
	{
		"ID": e.occupations.FIREFIGHTER,
		"PREFIX": "a",
		"NAME": ["firefighter"],
		"SECTOR": [e.occupation_sectors.FIRE],
		"BONUS": [e.STR, 10, e.LDR, 5]
	},
	{
		"ID": e.occupations.AMBULANCE_DRIVER,
		"PREFIX": "an",
		"NAME": ["ambulance driver"],
		"SECTOR": [e.occupation_sectors.AMBULANCE, e.occupation_sectors.DRIVING],
		"BONUS": [e.DEX, 5, e.MED, 5]
	},
	{
		"ID": e.occupations.BIOLOGIST,
		"PREFIX": "a",
		"NAME": ["biologist"],
		"SECTOR": [e.occupation_sectors.SCIENCE, e.occupation_sectors.MEDICINE],
		"BONUS": [e.WIS, 5, e.CON, 5, e.MED, 5]
	},
	{
		"ID": e.occupations.PHYSICIST,
		"PREFIX": "a",
		"NAME": ["physicist"],
		"SECTOR": [e.occupation_sectors.SCIENCE],
		"BONUS": [e.WIS, 5, e.MRK, 5, e.ENG, 5]
	},
	{
		"ID": e.occupations.CHEMIST,
		"PREFIX": "a",
		"NAME": ["chemist"],
		"SECTOR": [e.occupation_sectors.SCIENCE],
		"BONUS": [e.WIS, 10, e.MED, 5]
	},
	{
		"ID": e.occupations.MATHMATICIAN,
		"PREFIX": "a",
		"NAME": ["mathmatician"],
		"SECTOR": [e.occupation_sectors.SCIENCE],
		"BONUS": [e.WIS, 10]
	},
	{
		"ID": e.occupations.DOCTOR,
		"PREFIX": "a",
		"NAME": ["doctor"],
		"SECTOR": [e.occupation_sectors.MEDICINE],
		"BONUS": [e.MED, 15]
	},
	{
		"ID": e.occupations.NURSE,
		"PREFIX": "a",
		"NAME": ["nurse"],
		"SECTOR": [e.occupation_sectors.MEDICINE],
		"BONUS": [e.MED, 5, e.CHA, 5]
	},
	{
		"ID": e.occupations.REGISTERED_NURSE,
		"PREFIX": "a",
		"NAME": ["registered nurse"],
		"CAPITALISE": true,
		"SECTOR": [e.occupation_sectors.MEDICINE],
		"BONUS": [e.MED, 10, e.CHA, 5]
	},
	{
		"ID": e.occupations.SURGEON,
		"PREFIX": "a",
		"NAME": ["surgeon"],
		"SECTOR": [e.occupation_sectors.MEDICINE],
		"BONUS": [e.MED, 10, e.DEX, 5],
		"TYPE": ["Heart"]
	},
	{
		"ID": e.occupations.PILOT,
		"PREFIX": "a",
		"NAME": ["pilot"],
		"SECTOR": [e.occupation_sectors.FLIGHT, e.occupation_sectors.LOGISTICS, e.occupation_sectors.TRANSPORT],
		"BONUS": [e.DEX, 10, e.MEC, 5]
	},
	{
		"ID": e.occupations.TRUCK_DRIVER,
		"PREFIX": "a",
		"NAME": ["truck driver"],
		"SECTOR": [e.occupation_sectors.LOGISTICS, e.occupation_sectors.DRIVING],
		"BONUS": [e.DEX, 5, e.MEC, 5]
	},
	{
		"ID": e.occupations.TRAIN_DRIVER,
		"PREFIX": "a",
		"NAME": ["train driver"],
		"SECTOR": [e.occupation_sectors.LOGISTICS, e.occupation_sectors.DRIVING, e.occupation_sectors.TRANSPORT],
		"BONUS": [e.DEX, 5, e.ELC, 5]
	},
	{
		"ID": e.occupations.FORKLIFT_DRIVER,
		"PREFIX": "a",
		"NAME": ["forklift driver"],
		"SECTOR": [e.occupation_sectors.LOGISTICS, e.occupation_sectors.FACTORY],
		"BONUS": [e.DEX, 3, e.MEC, 2]
	},
	{
		"ID": e.occupations.PICK_PACKER,
		"PREFIX": "a",
		"NAME": ["pick packer"],
		"SECTOR": [e.occupation_sectors.LOGISTICS, e.occupation_sectors.FACTORY],
		"BONUS": [e.DEX, 3, e.CON, 2]
	},
	{
		"ID": e.occupations.BUS_DRIVER,
		"PREFIX": "a",
		"NAME": ["bus driver"],
		"SECTOR": [e.occupation_sectors.DRIVING, e.occupation_sectors.TRANSPORT],
		"BONUS": [e.DEX, 5]
	},
	{
		"ID": e.occupations.MUSICIAN,
		"PREFIX": "a",
		"NAME": ["musician"],
		"SECTOR": [e.occupation_sectors.ENTERTAINMENT],
		"BONUS": [e.DEX, 5]
	},
	{
		"ID": e.occupations.COMEDIAN,
		"PREFIX": "a",
		"NAME": ["comedian"],
		"SECTOR": [e.occupation_sectors.ENTERTAINMENT],
		"BONUS": [e.CHA, 5]
	},
	{
		"ID": e.occupations.WAITER,
		"PREFIX": "a",
		"NAME": ["waiter"],
		"SECTOR": [e.occupation_sectors.HOSPITALITY, e.occupation_sectors.CUSTOMER_SERVICE],
		"BONUS": [e.CHA, 3, e.DEX, 2]
	},
	{
		"ID": e.occupations.CHECKOUT_OPERATOR,
		"PREFIX": "a",
		"NAME": ["checkout operator"],
		"SECTOR": [e.occupation_sectors.CUSTOMER_SERVICE],
		"BONUS": [e.CHA, 5]
	},
	{
		"ID": e.occupations.ELECTRICIAN,
		"PREFIX": "an",
		"NAME": ["electrician"],
		"SECTOR": [e.occupation_sectors.ELECTRIC],
		"BONUS": [e.ELC, 15]
	},
	{
		"ID": e.occupations.MECHANIC,
		"PREFIX": "a",
		"NAME": ["mechanic"],
		"SECTOR": [e.occupation_sectors.MECHANIC],
		"BONUS": [e.MEC, 15]
	},
	{
		"ID": e.occupations.ENGINEER,
		"PREFIX": "an",
		"NAME": ["engineer"],
		"SECTOR": [e.occupation_sectors.ENGINEER],
		"BONUS": [e.ENG, 15]
	},
	{
		"ID": e.occupations.GARBAGE_PERSON,
		"PREFIX": "a",
		"NAME": ["garbage man", "garbage woman", "garbage person"],
		"SECTOR": [e.occupation_sectors.SERVICES, e.occupation_sectors.DRIVING],
		"BONUS": [e.MEC, 5]
	},
	{
		"ID": e.occupations.RECEPTIONIST,
		"PREFIX": "a",
		"NAME": ["receptionist"],
		"SECTOR": [e.occupation_sectors.CUSTOMER_SERVICE],
		"BONUS": [e.CHA, 5]
	},
	{
		"ID": e.occupations.ATHLETE,
		"PREFIX": "an",
		"NAME": ["athlete"],
		"SECTOR": [e.occupation_sectors.SPORT],
		"BONUS": [e.STR, 5, e.DEX, 5, e.AGI, 5]
	},
	{
		"ID": e.occupations.SPORT_SHOOTER,
		"PREFIX": "a",
		"NAME": ["sport shooter"],
		"SECTOR": [e.occupation_sectors.SPORT],
		"BONUS": [e.MRK, 15]
	},
	{
		"ID": e.occupations.HUNTER,
		"PREFIX": "a",
		"NAME": ["hunter"],
		"SECTOR": [e.occupation_sectors.SPORT, e.occupation_sectors.SELF_EMPLOYED],
		"BONUS": [e.MRK, 10]
	},
	{
		"ID": e.occupations.FARMER,
		"PREFIX": "a",
		"NAME": ["farmer"],
		"SECTOR": [e.occupation_sectors.FARMING, e.occupation_sectors.SELF_EMPLOYED],
		"BONUS": [e.CON, 5, e.DEX, 5],
		"TYPE": ["Livestock", "Crop"]
	},
	{
		"ID": e.occupations.PERSONAL_TRAINER,
		"PREFIX": "a",
		"NAME": ["personal trainer"],
		"SECTOR": [e.occupation_sectors.SERVICES],
		"BONUS": [e.STR, 5, e.AGI, 5]
	},
	{
		"ID": e.occupations.LECTURER,
		"PREFIX": "a",
		"NAME": ["lecturer"],
		"SECTOR": [e.occupation_sectors.EDUCATION],
		"BONUS": [e.WIS, 10, e.CHA, 5]
	},
	{
		"ID": e.occupations.TEACHER,
		"PREFIX": "a",
		"NAME": ["teacher"],
		"SECTOR": [e.occupation_sectors.EDUCATION],
		"BONUS": [e.WIS, 5, e.CHA, 5],
		"TYPE": ["Primary school", "Secondary school"]
	},
	{
		"ID": e.occupations.TUTOR,
		"PREFIX": "a",
		"NAME": ["tutor"],
		"SECTOR": [e.occupation_sectors.EDUCATION, e.occupation_sectors.SELF_EMPLOYED],
		"BONUS": [e.WIS, 5]
	},
	{
		"ID": e.occupations.DRUG_DEALER,
		"PREFIX": "a",
		"NAME": ["drug dealer"],
		"SECTOR": [e.occupation_sectors.CRIME],
		"BONUS": [e.DEX, 5, e.WIS, 5]
	},
	{
		"ID": e.occupations.ASSASSIN,
		"PREFIX": "an",
		"NAME": ["assassin"],
		"SECTOR": [e.occupation_sectors.CRIME],
		"BONUS": [e.DEX, 5, e.MRK, 5, e.AGI, 5]
	},
	{
		"ID": e.occupations.SHOPLIFTER,
		"PREFIX": "a",
		"NAME": ["shoplifter"],
		"SECTOR": [e.occupation_sectors.CRIME],
		"BONUS": [e.DEX, 5]
	}
]
