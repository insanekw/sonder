extends Node

## FILE POINTERS
const FACE_SPRITE_DIR : = "res://assets/sprites/faces/"
const ITEM_SPRITE_DIR : = "res://assets/sprites/items/"
const UNIT_SPRITE_DIR : = "res://assets/sprites/units/"
const STATUS_SPRITE_DIR : = "res://assets/sprites/status/"

## FORMATTERS
func _seperate_with_commas(string_array : Array, add_and = true) -> String:
	var list = ""

	for i in range(string_array.size()):
		if i == string_array.size() - 1 and add_and:
			list += string_array[i] + " and "
		else:
			if i != 0 and i != string_array.size():
				list += string_array[i] + ", "

	return list

func _seperate_with_slashes(string_array : Array) -> String:
	var list = ""
	
	for i in range(string_array.size()):
		if i < string_array.size() - 1:
			list += string_array[i] + "/"
		else:
			list += string_array[i]

	return list

## RANDOM
# Goes through array of strings and picks one
func _get_random_string(strings : Array) -> String:
	return str(strings[randi()%strings.size()])

## ENUM COMPARE
func _get_stat_name(stat):
	match stat:
		e.STR:
			return "STR"
		e.CON:
			return "CON"
		e.DEX:
			return "DEX"
		e.AGI:
			return "AGI"
		e.CHA:
			return "CHA"
		e.WIS:
			return "WIS"
		e.LDR:
			return "LDR"
		e.MRK:
			return "MRK"
		e.MED:
			return "MED"
		e.MEC:
			return "MEC"
		e.ELC:
			return "ELC"
		e.ENG:
			return "ENG"


#string controls
func _get_skill_name(stat):
	match stat:
		e.HEALTH_STAT:
			return "health"
		e.STAMINA_STAT:
			return "stamina"
		e.CARRY_STAT:
			return "carry"
		e.HEALING_STAT:
			return "healing"
		e.MOVEMENT_STAT:
			return "movement"
		e.SNEAK_STAT:
			return "stealth"
		e.TALK_STAT:
			return "talking"
		e.BLUFF_STAT:
			return "bluff"
		e.LOCKPICK_STAT:
			return "lockpick"
		e.EXPLOSIVES_STAT:
			return "explosives"
		e.MEDICINE_STAT:
			return "medicine"
		e.MECHANICAL_STAT:
			return "mechanical"
		e.ELECTRONICS_STAT:
			return "electronics"
		e.ENGINEERING_STAT:
			return "engineering"
		e.LEARN_STAT:
			return "learn"
		e.TEACH_STAT:
			return "teach"
		e.INITIATIVE_STAT:
			return "initiative"
		e.BLOCK_STAT:
			return "block"
		e.DODGE_STAT:
			return "dodge"
		e.MELEE_AIM_STAT:
			return "melee aim"
		e.MELEE_DMG_STAT:
			return "melee damage"
		e.MELEE_AP_STAT:
			return "melee AP"
		e.RANGED_AIM_STAT:
			return "ranged aim"
		e.RANGED_DMG_STAT:
			return "ranged damage"
		e.RANGED_AP_STAT:
			return "ranged AP"
