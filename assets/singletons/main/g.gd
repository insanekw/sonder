extends Node

var fullscreen = false

var current_scene
## STRING FILE POINTERS
const FACE_SPRITE_DIR : = "res://assets/sprites/faces/"
const ITEM_SPRITE_DIR : = "res://assets/sprites/items/"
const UNIT_SPRITE_DIR : = "res://assets/sprites/units/"
const STATUS_SPRITE_DIR : = "res://assets/sprites/status/"

## CONFIG
var boosted_traits_only = true
var max_perks_allowed = 15

## TILEMAP
var TILESET_SIZE : = 32
var TILE_OFFSET : = 16

## CONFIG
# Game variables
var game_difficulty = e.difficulty.NORMAL

# Metric system for measurements
var metric : = true

# Difficulty stat min/max
const NPC_HUMAN_STAT_MIN_easy : = 250
const NPC_HUMAN_STAT_MAX_easy : = 400
const NPC_HUMAN_STAT_MIN_normal : = 300
const NPC_HUMAN_STAT_MAX_normal : = 500
const NPC_HUMAN_STAT_MIN_hard : = 350
const NPC_HUMAN_STAT_MAX_hard : = 550
const NPC_HUMAN_STAT_MIN_extreme : = 400
const NPC_HUMAN_STAT_MAX_extreme : = 600

const NPC_ANIMAL_STAT_MIN_easy : = 125
const NPC_ANIMAL_STAT_MAX_easy : = 200
const NPC_ANIMAL_STAT_MIN_normal : = 150
const NPC_ANIMAL_STAT_MAX_normal : = 250
const NPC_ANIMAL_STAT_MIN_hard : = 175
const NPC_ANIMAL_STAT_MAX_hard : = 425
const NPC_ANIMAL_STAT_MIN_extreme : = 200
const NPC_ANIMAL_STAT_MAX_extreme : = 300

# NPC variables
enum {NUMBER_OF_HUMANS = 200, NUMBER_OF_ANIMALS = 200}

# Unit variables
const MAX_STAT_VALUE : = 100
const STAT_MEDIAN : = MAX_STAT_VALUE / 2;
# Offset used to make more/less unique personalities
const STAT_OFFSET : = 20
var LOWER_THRESHOLD : = STAT_MEDIAN - STAT_OFFSET
var UPPER_THRESHOLD : = STAT_MEDIAN + STAT_OFFSET

## MOVEMENT
# Directions
var NORTH : = Vector2(0,-1) * TILESET_SIZE
var SOUTH : = Vector2(0, 1) * TILESET_SIZE
var EAST : = Vector2(1, 0) * TILESET_SIZE
var WEST : = Vector2(-1, 0) * TILESET_SIZE
var NORTHEAST : = Vector2(1, -1) * TILESET_SIZE
var NORTHWEST : = Vector2(-1, -1) * TILESET_SIZE
var SOUTHEAST : = Vector2(1, 1) * TILESET_SIZE
var SOUTHWEST : = Vector2(-1, 1) * TILESET_SIZE
# Unused
var UP_Z_LEVEL : = Vector3(0, 0, 1)
var DOWN_Z_LEVEL : = Vector3(0, 0, -1)

## UNITS
# Age
const YOUNGEST_AGE : = 19
const LIFE_EXPECTANCY : = 60
const MAX_POSSIBLE_AGE : = 111

## COUNTERS
# Gameworld IDs
var unit_ID_count : = 0
var group_ID_count : = 0
var item_ID_count : = 0
var business_ID_count : = 0
var job_ID_count : = 0
var building_ID_count : = 0

## STRING ARRAYS
# Dialogue and action tracker
var game_log : = []
var dialogue : = []
var actions : = []
var thoughts : = []

## DATE AND TIME
# variables
var _24_hour_time : bool
var date_time
var seconds : int
var minutes : int
var hour : int
var day : int
var month : int
var year : int
var am_pm : String
var day_of_week : int
var LEAP_YEAR : = (year - 776) % 4 == 0

# Randomly picks one of 8 directions OR returns the base node
func _random_direction(include_centre_square : = false):
	randomize()
	if include_centre_square:
		var random = randi()%9
		match random:
			0:
				return NORTH
			1:
				return NORTHEAST
			2:
				return NORTHWEST
			3:
				return EAST
			4:
				return WEST
			5:
				return SOUTHEAST
			6:
				return SOUTH
			7:
				return SOUTHWEST
			8:
				return Vector2(0,0)
	else:
		var random = randi()%8
		match random:
			0:
				return NORTH
			1:
				return NORTHEAST
			2:
				return NORTHWEST
			3:
				return EAST
			4:
				return WEST
			5:
				return SOUTHEAST
			6:
				return SOUTH
			7:
				return SOUTHWEST

# Scene change
func goto_scene(path : String) -> void:
	call_deferred("_deferred_goto_scene",path)

func _deferred_goto_scene(path : String) -> void:
	current_scene.free()
	var s = ResourceLoader.load(path)
	current_scene = s.instance()
	get_tree().get_root().add_child(current_scene)
	get_tree().set_current_scene(current_scene)


func _unhandled_input(event : InputEvent) -> void:
	# fullscreen on alt+enter
	if Input.is_action_just_released("ui_alt_enter"):
		if OS.window_fullscreen:
			OS.window_fullscreen = false
			fullscreen == false
		else:
			OS.window_fullscreen = true
			fullscreen == true

# Random number generators
func _get_random_1_100() -> int:
	var random = 0
	while random == 0:
		randomize()
		random = randi()%101
	return random

# TODO:pretty sure this is rolling extra times unnecessarily, handled for now but optimise later if need be
func _get_random(min_number : int, max_number : int) -> int:
	if min_number > 0:
		var random = min_number - 1
		while random < min_number or random > max_number:
			randomize()
			random = randi()%(max_number + min_number) + 1
			random -= min_number
		return random
	else:
		var random = min_number - 1
		while random < min_number or random > max_number + 1:
			randomize()
			random = randi()%((1 + max_number) + min_number) + 1
			random -= min_number
		return random - 1

func _get_random_50_weight() -> int:
	var random = 0
	var temp
	while random == 0:
		randomize()
		random = randi()%101
	temp = random
	random = 0
	while random == 0:
		randomize()
		random = randi()%101
	random += temp
	random /= 2	
	if random < 1:
		print("Oh no, <1 was rolled!")
	elif random == 1:
		print("Oh good, a 1 was rolled. (50/50)")
	elif random == 100:
		print("Oh good, 100 was rolled. (50/50)")
	elif random > 100:
		print("Oh no, >100 was rolled!")
	return random

func _sort_array_alphabetically(array : Array) -> void:
	array.sort_custom(self, "_sort_alphabetically")

func _sort_alphabetically(a : Dictionary, b : Dictionary) -> bool:
	if (str(a["ID"])) < (str(b["ID"])):
		return true
	else:
		return false

func _load_character_prefab(unit_dictionary : Dictionary) -> Node2D:
	var new_unit = load("res://scenes/unit/Unit.tscn").instance()
	
	new_unit.first_name = unit_dictionary["FIRST_NAME"]
	new_unit.last_name = unit_dictionary["LAST_NAME"]
	new_unit.nick_name = unit_dictionary["NICK_NAME"]
	new_unit.face = unit_dictionary["FACE"]
	new_unit.gender = unit_dictionary["GENDER"]
	new_unit.place_of_birth = unit_dictionary["PLACE_OF_BIRTH"]
	new_unit.experience_points = unit_dictionary["EXPERIENCE_POINTS"]
	new_unit.level = unit_dictionary["LEVEL"]
	new_unit.birth_day = unit_dictionary["BIRTH_DAY"]
	new_unit.birth_month = unit_dictionary["BIRTH_MONTH"]
	new_unit.birth_year = unit_dictionary["BIRTH_YEAR"]
	new_unit.strength = unit_dictionary["STR"]
	new_unit.constitution = unit_dictionary["CON"]
	new_unit.dexterity = unit_dictionary["DEX"]
	new_unit.agility = unit_dictionary["AGI"]
	new_unit.wisdom = unit_dictionary["WIS"]
	new_unit.charisma = unit_dictionary["CHA"]
	new_unit.leadership = unit_dictionary["LDR"]
	new_unit.marksmanship = unit_dictionary["MRK"]
	new_unit.medical = unit_dictionary["MED"]
	new_unit.electronics = unit_dictionary["ELC"]
	new_unit.mechanical = unit_dictionary["MEC"]
	new_unit.engineering = unit_dictionary["ENG"]
	
	return new_unit


# Returns 007 James Bond, unit ID + first and last name
func _get_easy_unit_ID(unit : Unit) -> String:
	return str(unit.ID) + " " + unit.first_name + " " + unit.last_name

func _int_to_Vector2(number : int) -> Vector2:
	return Vector2(number, number)

# Get a vector3 of an item or unit -unused at the moment, might be used for Z-levels
func _get_position_as_Vector3(thing : Node2D):
	return Vector3(thing.global_position.x, thing.global_position.y, thing.z_level)

# Returns full item information by a given itemID
func _get_item(ID : int) -> Dictionary:
	return items.items._get_item_by_ID(ID)

# Goes through array of strings and picks one
func _get_random_string(strings : Array) -> String:
	return str(strings[randi()%strings.size()+0])
