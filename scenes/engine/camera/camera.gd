extends Camera2D

# CAMERA ZOOM CONSTRAINTS
const MAX_ZOOM_IN : = Vector2(0.2, 0.2)
const MAX_ZOOM_OUT : = Vector2(1, 1)

# Moves camera to specified location and up or down the appropriate number of z-levels
func _snap_to_square(location : Vector2, z_level : int) -> void:
	"""MOVE CAMERA TO LOCATION
	(location should always be global_position to account for z-level offset??? untested)"""
	position = location
	# might not need this if the global_position thing works
	_shift_between_levels(get_parent().current_level, z_level, true)

# Moves camera up/down the y axis to account for the z-level offset of the tilemaps 
#(might be redundant, see above)
func _shift_between_levels(start_level : int, end_level : int, camera_follows : = false) -> void:
	# Camera doesn't need to move up or down if the specified levels are the same
	if start_level == end_level:
		return
	
	elif start_level < end_level and end_level <= get_parent().highest_level:
		# up
		for i in range(end_level - start_level):
			get_parent().current_level += 1
			get_parent().z_level_label.text = "Z" + str(get_parent().current_level)
			get_parent()._display_level()
			if camera_follows:
				_move_up_level()
	elif start_level > end_level:
		# down
		for i in range(start_level - end_level):
			get_parent()._hide_level()
			get_parent().current_level -= 1
			get_parent().z_level_label.text = "Z" + str(get_parent().current_level)
			if camera_follows:
				_move_down_level()

# Move camera up (default -32 on the y axis)
func _move_up_level():
	position.y -= g.TILESET_SIZE

# Move camera down (default +32 on the y axis)
func _move_down_level():
	position.y += g.TILESET_SIZE

## CONTROLS
func _input(event : InputEvent) -> void:

	# TODO:used to click and drag map
	if event.is_action_pressed("middle_click"):
		pass

	# Zoom camera in
	if event.is_action_pressed("mousewheel_up") and zoom > MAX_ZOOM_IN:
		if zoom < Vector2(1, 1):
			zoom -= Vector2(0.1, 0.1)
		elif zoom < Vector2(2, 2):
			zoom -= Vector2(0.15, 0.15)
		else:
			zoom -= Vector2(0.2, 0.2)
	elif zoom <= MAX_ZOOM_IN:
		zoom = MAX_ZOOM_IN

	# Zoom camera out
	if event.is_action_pressed("mousewheel_down") and zoom < MAX_ZOOM_OUT:
		if zoom < Vector2(1, 1):
			zoom += Vector2(0.1, 0.1)
		elif zoom < Vector2(2, 2):
			zoom += Vector2(0.15, 0.15)
		else:
			zoom += Vector2(0.2, 0.2)
	elif zoom >= MAX_ZOOM_OUT:
		zoom = MAX_ZOOM_OUT

	# Display next level
	if event.is_action_pressed("ui_page_up"):
		var current_level = get_parent().current_level
		var highest_level = get_parent().highest_level
		if current_level < highest_level:
			_shift_between_levels(current_level, current_level + 1, true)
			print(str(current_level + 1))

	# Hide current level
	if event.is_action_pressed("ui_page_down"):
		var current_level = get_parent().current_level
		if current_level > 0:
			_shift_between_levels(current_level, current_level - 1, true)
			print(str(current_level - 1))
	"""
	if event is InputEventMouseMotion:
		for i in range(0, get_parent().current_level):
			for 
			if !_mouse_over_empty_tile(i):
				print(str(get_parent().current_level))
				print(_detect_tile(get_global_mouse_position(), i))
	"""
	# Move camera up -minimap currently not implemented
	if event.is_action("w"):
		position += Vector2(0, -1) * g.TILESET_SIZE
		# minimap_center += Vector2(0, -1)
		# _draw_minimap()
	# Move camera left
	if event.is_action("a"):
		position += Vector2(-1, 0) * g.TILESET_SIZE
		# minimap_center += Vector2(-1, 0)
		# _draw_minimap()
	# Move camera down
	if event.is_action("s"):
		position += Vector2(0, 1) * g.TILESET_SIZE
		# minimap_center += Vector2(0, 1)
		# _draw_minimap()
	# Move camera right
	if event.is_action("d"):
		position += Vector2(1, 0) * g.TILESET_SIZE
		# minimap_center += Vector2(1, 0)
		# _draw_minimap()
