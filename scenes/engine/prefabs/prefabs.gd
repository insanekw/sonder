extends Node

"""
var crafting = [
	{
		"RESULT": e.item.CAMPFIRE
	}
]
"""

## CHARACTERS
"""
example:
var character_prefabs = [
	{
		"FIRST_NAME": "",
		"LAST_NAME": "",
		"NICK_NAME": "",
		
		"FACE": "",
		
		"GENDER": null,
		"PLACE_OF_BIRTH": "",
		
		"EXPERIENCE_POINTS": 0,
		"LEVEL": 1,
		
		"BIRTH_DAY": 0,
		"BIRTH_MONTH": 0,
		"BIRTH_YEAR": 0,
		
		"STR": 0,
		"CON": 0,
		"DEX": 0,
		"AGI": 0,
		"WIS": 0,
		"CHA": 0,
		
		"LDR": 0,
		"MRK": 0,
		"MED": 0,
		"ELC": 0,
		"MEC": 0,
		"ENG": 0,
		
		"LOCATION": [1, 0, 0]
	}
]
"""

## INVENTORIES
var inventory = [
	#specific to char
	{
		"ID": e.prefabs.TWD_RICK,
		"ITEMS": [
			items._357_REVOLVER, 
			items.CUTOFF_PLAID_SHIRT, 
			items.JEANS, 
			items.SOCKS, 
			items.COWBOY_BOOTS,
			items._9V_BATTERY
		]
	},
	{
		"ID": e.prefabs.TWD_MICHONNE,
		"ITEMS": [
			items.KATANA, 
			items.BLACK_T_SHIRT, 
			items.CARGO_PANTS, 
			items.SOCKS, 
			items.COMBAT_BOOTS,
			items._9V_BATTERY
		]
	},
	#generic
	{
		"ID": e.prefabs.SHIRT_AND_PANTS,
		"ITEMS": [
			items.CROWBAR, 
			items.DICK_SHOW_SHIRT, 
			items.CARGO_PANTS, 
			items.SOCKS, 
			items.SNEAKERS,
			items._9V_BATTERY
		]
	},
	{
		"ID": e.prefabs.HAZMAT_SUIT,
		"ITEMS": [
			items.DEAGLE, 
			items.RADSUIT_HELMET, 
			items.RADSUIT,
			items._9V_BATTERY
		]
	},
	{
		"ID": e.prefabs.THE_WELDER,
		"ITEMS": [
			items.FIREAXE, 
			items.DICK_SHOW_SHIRT, 
			items.PAINTERS_OVERALLS,
			items._9V_BATTERY
		]
	},
	{
		"ID": e.prefabs.NEWSBOY,
		"ITEMS": [
			items.SHOVEL, 
			items.NEWSBOY_CAP,
			items.PLAID_SHIRT,
			items.JEANS, 
			items.SOCKS, 
			items.SNEAKERS,
			items._9V_BATTERY
		]
	},
	{
		"ID": e.prefabs.SURVIVALIST,
		"ITEMS": [
			items.AK47, 
			items.BALACLAVA,
			items.CATCHERS_VEST,
			items.HIKING_BAG,
			items.JEANS, 
			items.SOCKS, 
			items.SNEAKERS,
			items._9V_BATTERY
		]
	},
]

## CHARACTERS
var chars = [
	{
		"ID": e.prefabs.TWD_RICK,
		"FIRST_NAME": "Rick",
		"LAST_NAME": "Grimes",
		"NICK_NAME": "",
		
		"FACE": "rick_grimes",
		
		"GENDER": e.genders.MALE,
		"PLACE_OF_BIRTH": "United States of America",
		
		"EXPERIENCE_POINTS": 0,
		"LEVEL": 1,
		
		"INVENTORY": _get_inventory_by_ID(e.prefabs.TWD_RICK),
		
		"BIRTH_DAY": 13,
		"BIRTH_MONTH": 3,
		"BIRTH_YEAR": 1968,
		
		"STR": 55,
		"CON": 45,
		"DEX": 68,
		"AGI": 28,
		"WIS": 75,
		"CHA": 62,
		
		"LDR": 87,
		"MRK": 96,
		"MED": 16,
		"ELC": 4,
		"MEC": 12,
		"ENG": 0,
		
		"LOCATION": [0, 0, 0]
	},
	{
		"ID": e.prefabs.TWD_MICHONNE,
		"FIRST_NAME": "Michonne",
		"LAST_NAME": "",
		"NICK_NAME": "",
		
		"FACE": "michonne",
		
		"GENDER": e.genders.FEMALE,
		"PLACE_OF_BIRTH": "United States of America",
		
		"EXPERIENCE_POINTS": 0,
		"LEVEL": 1,
		
		"INVENTORY": _get_inventory_by_ID(e.prefabs.TWD_MICHONNE),
		
		"BIRTH_DAY": 26,
		"BIRTH_MONTH": 7,
		"BIRTH_YEAR": 1978,
		
		"STR": 45,
		"CON": 66,
		"DEX": 71,
		"AGI": 52,
		"WIS": 67,
		"CHA": 13,
		
		"LDR": 0,
		"MRK": 21,
		"MED": 30,
		"ELC": 3,
		"MEC": 5,
		"ENG": 2,
		
		"LOCATION": [0, 0, 0]
	}
]

func _get_inventory_by_ID(ID : int):
	for inventory_list in inventory:
		if ID == inventory_list["ID"]:
			return inventory_list

func _get_char_by_ID(ID : int):
	for character in chars:
		if ID == character["ID"]:
			return character
