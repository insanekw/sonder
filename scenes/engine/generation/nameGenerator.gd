extends Node

# Character generation
func _generate_first_name(gender):
	match gender:
		e.genders.MALE:
			return _get_male_first_name()
		e.genders.FEMALE:
			return _get_female_first_name()
		_:
			if g._get_random_1_100() <= 50:
				return _get_male_first_name()
			else:
				return _get_female_first_name()

func _generate_last_name():
	return _get_last_name()

func _get_male_first_name():
	return g._get_random_string(get_node("first_names").male_first_names)

func _get_female_first_name():
	return g._get_random_string(get_node("first_names").female_first_names)

func _get_last_name():
	var roll = g._get_random_1_100()
	if roll <= 25:
		return g._get_random_string(get_node("last_names1").last_names)
	elif roll <= 50:
		return g._get_random_string(get_node("last_names2").last_names)
	elif roll <= 75:
		return g._get_random_string(get_node("last_names3").last_names)
	else:
		return g._get_random_string(get_node("last_names4").last_names)

# TODO:move this into its own scene(???)
func _get_countries():
	return get_node("countries").countries
