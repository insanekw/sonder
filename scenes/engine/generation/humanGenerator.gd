extends Node

# For LOL RANDOMness
var additional : int

var total_points : int
var points_remaining : int

func _initial_setup(unit, lower, upper):
	randomize()
	total_points = upper - randi()%(upper - lower)
	# probably unneccessary
	additional = 0
	
	_clear_points(unit)
	_update_total(unit)

func _clear_points(unit):
	# set primary points to
	unit.strength = 1
	unit.constitution = 1
	unit.dexterity = 1
	unit.agility = 1
	unit.charisma = 1
	unit.wisdom = 1
	
	unit.leadership = 0
	unit.marksmanship = 0
	unit.medicine = 0
	unit.mechanical = 0
	unit.engineering = 0
	unit.electronics = 0

func _generate_human(unit):
	_clear_points(unit)
	_generate_gender(unit)
	_generate_name(unit)
	_generate_face(unit)
	_generate_occupations(unit)
	_generate_perks_and_drawbacks(unit)
	# _generate_skin(unit)
	# unit.nick_name = global._generate_nick_name()
	
	_generate_stats(unit)
	
	unit.set_name(unit.first_name + " " + unit.last_name)

func _generate_gender(unit):
	var roll = g._get_random_1_100()
	if roll <= 49:
		unit.gender = e.genders.MALE
	elif roll <= 99:
		unit.gender = e.genders.FEMALE
	else:
		unit.gender = e.genders.OTHER_GENDER

func _generate_name(unit):
	var names = load("res://scenes/engine/generation/NameGenerator.tscn").instance()
	unit.first_name = names._generate_first_name(unit.gender)
	unit.last_name = names._generate_last_name()

func _generate_face(unit):
	var faces = load("res://scenes/engine/generation/Faces.tscn").instance()
	unit.face = faces._get_face(unit.gender, null)

func _generate_occupations(unit):
	unit.occupations = occupations_resource._get_random_occupations(3)

func _generate_perks_and_drawbacks(unit):
	unit.traits = traits_resource._get_random_traits(1, true)

# Spends points on stats until points = 0
func _generate_stats(unit):
	while points_remaining > 0:
		_place_stat_points(unit)
		_update_total(unit)
	
	_place_personality_points(unit)
	_set_properties(unit)

func _set_properties(unit):
	unit.health = round(unit._get_health()) as int
	unit.current_health = unit.health
	unit.stamina = round(unit._get_stamina()) as int
	unit.current_stamina = unit.stamina
	unit.metabolism = round(unit._get_healing()) as int
	unit.hunger = unit.metabolism
	unit.thirst = unit.metabolism


func _place_personality_points(unit):
	unit.militarism = g._get_random_1_100()
	unit.spiritualism = g._get_random_1_100()
	unit.authoritarianism = g._get_random_1_100()
	unit.open_mindedness = g._get_random_1_100()
	# Social
	unit.kindness = g._get_random_1_100()
	unit.honesty = g._get_random_1_100()
	unit.trust = g._get_random_1_100()
	unit.social = g._get_random_1_100()
	# Mental
	unit.optimism = g._get_random_1_100()
	unit.drive = g._get_random_1_100()
	unit.courage = g._get_random_1_100()
	unit.knowledge = g._get_random_1_100()
	
	if unit.wisdom < unit.knowledge:
		unit.knowledge = unit.wisdom


# Spend point/s on individual random stat + other randomness
func _place_stat_points(unit):
	# Random
	var roll
	roll = g._get_random_1_100()
	match roll:
		1, 2:
			if unit.strength < 99:
				if (unit.strength + 1 + additional) > 99:
					additional -= (unit.strength + 1 + additional) - 100
					unit.strength = 99
				else:
					unit.strength += 1 + additional
					additional = 0
		3, 4:
			if unit.constitution < 99:
				if (unit.constitution + 1 + additional) > 99:
					additional -= (unit.constitution + 1 + additional) - 100
					unit.constitution = 99
				else:
					unit.constitution += 1 + additional
					additional = 0
		5, 6:
			if unit.dexterity < 99:
				if (unit.dexterity + 1 + additional) > 99:
					additional -= (unit.dexterity + 1 + additional) - 100
					unit.dexterity = 99
				else:
					unit.dexterity += 1 + additional
					additional = 0
		7, 8:
			if unit.agility < 99:
				if (unit.agility + 1 + additional) > 99:
					additional -= (unit.agility + 1 + additional) - 100
					unit.agility = 99
				else:
					unit.agility += 1 + additional
					additional = 0
		9, 10:
			if unit.charisma < 99:
				if (unit.charisma + 1 + additional) > 99:
					additional -= (unit.charisma + 1 + additional) - 100
					unit.charisma = 99
				else:
					unit.charisma += 1 + additional
					additional = 0
		11, 12:
			if unit.wisdom < 99:
				if (unit.wisdom + 1 + additional) > 99:
					additional -= (unit.wisdom + 1 + additional) - 100
					unit.wisdom = 99
				else:
					unit.wisdom += 1 + additional
					additional = 0
		13:
			if unit.leadership < 99:
				if (unit.leadership + 1 + additional) > 99:
					additional -= (unit.leadership + 1 + additional) - 100
					unit.leadership = 99
				else:
					unit.leadership += 1 + additional
					additional = 0
		14:
			if unit.marksmanship < 99:
				if (unit.marksmanship + 1 + additional) > 99:
					additional -= (unit.marksmanship + 1 + additional) - 100
					unit.marksmanship = 99
				else:
					unit.marksmanship += 1 + additional
					additional = 0
		15:
			if unit.medicine < 99:
				if (unit.medicine + 1 + additional) > 99:
					additional -= (unit.medicine + 1 + additional) - 100
					unit.medicine = 99
				else:
					unit.medicine += 1 + additional
					additional = 0
		16:
			if unit.mechanical < 99:
				if (unit.mechanical + 1 + additional) > 99:
					additional -= (unit.mechanical + 1 + additional) - 100
					unit.mechanical = 99
				else:
					unit.mechanical += 1 + additional
					additional = 0
		17:
			if unit.electronics < 99:
				if (unit.electronics + 1 + additional) > 99:
					additional -= (unit.electronics + 1 + additional) - 100
					unit.electronics = 99
				else:
					unit.electronics += 1 + additional
					additional = 0
		18:
			if unit.engineering < 99:
				if (unit.engineering + 1 + additional) > 99:
					additional -= (unit.engineering + 1 + additional) - 100
					unit.engineering = 99
				else:
					unit.engineering += 1 + additional
					additional = 0
		_:
			if points_remaining > 1 + additional:
				additional += 1
	
	_update_total(unit)
	
	if points_remaining > 0:
		# More randomness
		roll = g._get_random_1_100()
		match roll:
			1:
				if unit.strength > 2:
					unit.strength / 2
			2:
				if unit.dexterity > 2:
					unit.dexterity / 2
			3:
				if unit.constitution > 2:
					unit.constitution / 2
			4:
				if unit.agility > 2:
					unit.agility / 2
			5:
				if unit.charisma > 2:
					unit.charisma / 2
			6:
				if unit.wisdom > 2:
					unit.wisdom / 2
			7:
				if unit.leadership > 1:
					unit.leadership / 2
			8:
				if unit.marksmanship > 1:
					unit.marksmanship / 2
			9:
				if unit.medicine > 1:
					unit.medicine / 2
			10:
				if unit.mechanical > 1:
					unit.mechanical / 2
			11:
				if unit.engineering > 1:
					unit.engineering / 2
			12:
				if unit.electronics > 1:
					unit.electronics / 2
			_:
				pass

func _update_total(unit):
	points_remaining = (
	total_points - 
	unit.strength - 
	unit.leadership -
	unit.constitution - 
	unit.marksmanship - 
	unit.dexterity -
	unit.electronics - 
	unit.agility - 
	unit.medicine - 
	unit.wisdom - 
	unit.engineering - 
	unit.charisma - 
	unit.mechanical
	)

"""
func _generate_map_position(unit):
	var x = ((global._get_random_1_100() - 50))
	if x > 0:
		x -= 1
	else:
		 x += 1
	
	var y = 0
	
	var z = ((global._get_random_1_100() - 50))
	if z > 0:
		z -= 1
	else:
		z += 1
	unit.translation = Vector3(x, y + 0, z)

func _paint_skin(skin, texture):
	for surface in skin.mesh.get_surface_count():
		skin.mesh.surface_set_material(surface, texture)


func _create_skin(skin, gender):
	if gender == global.MALE:
		if global._get_random_1_100() < 50:
			_paint_skin(skin, load("res://assets/unit/human/material/male.tres"))
		else:
			_paint_skin(skin, load("res://assets/unit/human/material/male2.tres"))
	elif gender == global.FEMALE:
		if global._get_random_1_100() < 50:
			_paint_skin(skin, load("res://assets/unit/human/material/female.tres"))
		else:
			_paint_skin(skin, load("res://assets/unit/human/material/female2.tres"))
	else:
		_paint_skin(skin, load("res://assets/unit/human/material/zombie.tres"))
		

func _generate_skin(unit):
	# TODO: load correct mesh based on person
	unit.skin.mesh = preload("res://assets/unit/human/mesh/Non-rigged/advancedCharacter.obj").duplicate()
	# TODO: make mesh correct size so this is not necessary
	unit.scale = Vector3(0.1, 0.1, 0.1)
	
	if unit.gender == global.MALE:
		_create_skin(unit.skin, global.MALE)
	elif unit.gender == global.FEMALE:
		_create_skin(unit.skin, global.FEMALE)
	else:
		_create_skin(unit.skin, global.OTHER_GENDER)
"""
