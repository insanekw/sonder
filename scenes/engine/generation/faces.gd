extends Node


var tempDict = []

#triggered when can't find picture based exactly on parameters
var desperate = false

func _ready():
	pass

#FOR PLAYER CHARACTER GENERATION
func _get_faces(gender, appearance):
	_get_face(gender, appearance)
	if tempDict.empty():
		return []
	else:
		return tempDict
	tempDict.clear()

func _get_face(gender, appearance):
	#RETURN RANDOM FACE BASED ON GENDER IF NO PARAMETERS PASSED
	if appearance == null or appearance.empty():
		tempDict.clear()
		
		if gender == e.genders.MALE:
			_scan_faces(maleFaceDict, appearance)
		elif gender == e.genders.FEMALE:
			_scan_faces(femaleFaceDict, appearance)
		else:
			_scan_faces(maleFaceDict, appearance)
			_scan_faces(femaleFaceDict, appearance)
		
		randomize()
		return tempDict[randi()%tempDict.size()]
	else:
		tempDict.clear()
		
		if gender == e.genders.MALE:
			_scan_faces(maleFaceDict, appearance)
		elif gender == e.genders.FEMALE:
			_scan_faces(femaleFaceDict, appearance)
		else:
			_scan_faces(maleFaceDict, appearance)
			_scan_faces(femaleFaceDict, appearance)
	
	if !tempDict.empty():
		randomize()
		return tempDict[randi()%tempDict.size()]
	else:
		#awful endless loop here
		print("can't find exact match, returning next best thing")
		tempDict.clear()
		for item in tempDict:
			print(str(item))
		desperate = true
		
		if gender == e.genders.MALE:
			_scan_faces(maleFaceDict, appearance)
		elif gender == e.genders.FEMALE:
			_scan_faces(femaleFaceDict, appearance)
		else:
			_scan_faces(maleFaceDict, appearance)
			_scan_faces(femaleFaceDict, appearance)
		
		if !tempDict.empty():
			randomize()
			return tempDict[randi()%tempDict.size()]
		else:
			#return random whatever idc
			pass


func _scan_faces(faceDictionary, char_appearance):
	#CHOOSE RANDOM FACE BASED ON GENDER IF NO FACIAL FEATURES INPUT
	if char_appearance == null or char_appearance.empty():
		for face in faceDictionary:
			tempDict.append(face.ID)
	else:
		#GOES THROUGH EACH PHOTO IN FACE DICTIONARY
		for face in faceDictionary:
			#GOES THROUGH EACH FACIAL FEATURE IN UNIT'S DESCRIPTION
			for feature in char_appearance:
				#FIRST RUN, LOOKING FOR PERECT MATCHES - desperate = false
				if !desperate:
					#CHECK IF FEATURE EXISTS IN PICTURE
					if face.has(feature):
						#IF IT MATCHES THE CHARACTER'S FACIAL FEATURE
						if face[feature] == char_appearance[feature]:
							#ADD PICTURE ID TO THE LIST IF NOT ALREADY IN THERE
							if !tempDict.has(face.ID):
								tempDict.append(face.ID)
								print(str(face.ID) + " added to list")
						#IF VALUE IS NOT A MATCH
						else:
							#REMOVE PICTURE ID FROM THE LIST IF IT'S IN THERE
							if tempDict.has(face.ID):
								tempDict.erase(face.ID)
								print(str(face.ID) + " removed from list")
							#BREAK BECAUSE WE ONLY WANT PERFECT MATCHES
							break
					#IF PICTURE CONTAINS NO DATA RE: FEATURE
					else:
						#REMOVE PICTURE ID FROM THE LIST IF IT'S IN THERE
						if tempDict.has(face.ID):
							tempDict.erase(face.ID)
							print(str(face.ID) + " removed from list - no match")
						#BREAK BECAUSE WE ONLY WANT PERFECT MATCHES
						break
							
				#SECOND RUN, LOOKING FOR CLOSE MATCH - desperate = true
				else:
					#ONLY CHECK FOR BASIC ITEMS - gender/hair/skin
					if _required_item(feature, char_appearance):
						#CHECK IF FEATURE EXISTS IN PICTURE
						if face.has(feature):
							#IF IT MATCHES THE CHARACTER'S FACIAL FEATURE
							if face[feature] == char_appearance[feature]:
								#ADD PICTURE ID TO THE LIST IF NOT ALREADY IN THERE
								if !tempDict.has(face.ID):
									tempDict.append(face.ID)
									print(str(face.ID) + " added to list")
							#IF VALUE IS NOT A MATCH
							else:
								#REMOVE PICTURE ID FROM THE LIST IF IT'S IN THERE
								#make sure some kind of facial hair gets put through
								if feature != "FACIAL_HAIR":
									if tempDict.has(face.ID):
										tempDict.erase(face.ID)
										print(str(face.ID) + " removed from list")
									#BREAK BECAUSE WE ONLY WANT PERFECT MATCHES
									break
								else:
									#sympathetic beard add
									if !tempDict.has(face.ID):
										tempDict.append(face.ID)
										print(str(face.ID) + " sympathetic beard add")
						else:
							if feature != "FACIAL_HAIR":
								print("should never happen")
								if tempDict.has(face.ID):
									tempDict.erase(face.ID)
									print(str(face.ID) + " removed from list")
								break
					else:
						#print("not checking for " + str(feature))
						pass

func _required_item(feature, char_appearance):
	if char_appearance.has("FACIAL_HAIR"):
		if (
			(feature == "SKIN") or 
			(feature == "HAIR_COLOUR") or 
			(feature == "FACIAL_HAIR")
		):
			return true
	else:
		if (feature == "SKIN") or (feature == "HAIR_COLOUR"):
			return true

func _get_appearance_values(feature):
	var stringList = []
	for face in maleFaceDict:
		if face.has(feature):
			if !stringList.has(face[feature]):
				stringList.append(face[feature])
	
	for face in femaleFaceDict:
		if face.has(feature):
			if !stringList.has(face[feature]):
				stringList.append(face[feature])
	
	return stringList

"""DICTIONARIES
GENDER = male, female, other
SKIN = light, light, dark
FACIAL_HAIR = null, moustache, beard, goatee, curtain, redneck, lemmy, chops
HAIR_COLOUR = black, blonde, brown, red
BUILD = very_thin, thin, null, large, very_large
GLASSES = sun, eye
OLD = bool
FRECKLES = bool
"""
var maleFaceDict = [
{
	"ID": "m1",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "m2",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "m3",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"BUILD": "thin"
},
{
	"ID": "m4",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "moustache"
},
{
	"ID": "m5",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"GLASSES": "sun"
},
{
	"ID": "m6",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "curtain"
},
{
	"ID": "m7",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde",
	"FACIAL_HAIR": "beard",
	"GLASSES": "eye"
},
{
	"ID": "m8",
	"SKIN": "dark",
	"HAIR_COLOUR": "black",
	"FACIAL_HAIR": "beard"
},
{
	"ID": "m9",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "m10",
	"SKIN": "light",
	"HAIR_COLOUR": "bald",
	"BUILD": "thin",
	"GLASSES": "eye"
},
{
	"ID": "m11",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye"
},
{
	"ID": "m12",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "beard"
},
{
	"ID": "m13",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "curtain"
},
{
	"ID": "m14",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "lemmy",
	"GLASSES": "eye"
},
{
	"ID": "m15",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "moustache",
	"FRECKLES": true
},
{
	"ID": "m16",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde",
	"FACIAL_HAIR": "lemmy"
},
{
	"ID": "m17",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "moustache",
	"OLD": true
},
{
	"ID": "m18",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"BUILD": "thin"
},
{
	"ID": "m19",
	"SKIN": "light",
	"HAIR_COLOUR": "bald",
	"BUILD": "thin"
},
{
	"ID": "m20",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "goatee",
	"BUILD": "thin"
},
{
	"ID": "m21",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "m22",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "curtain"
},
{
	"ID": "m23",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "m24",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde"
},
{
	"ID": "m25",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "lemmy"
},
{
	"ID": "m26",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "balbo"
},
{
	"ID": "m27",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "moustache",
	"BUILD": "very_thin"
},
{
	"ID": "m28",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"BUILD": "large"
},
{
	"ID": "m29",
	"SKIN": "light",
	"HAIR_COLOUR": "black",
	"FACIAL_HAIR": "beard"
},
{
	"ID": "m30",
	"SKIN": "light",
	"HAIR_COLOUR": "grey",
	"FACIAL_HAIR": "curtain",
	"OLD": true
},
{
	"ID": "m31",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "goatee",
	"BUILD": "large",
	"FRECKLES": true
},
{
	"ID": "m32",
	"SKIN": "dark",
	"HAIR_COLOUR": "bald",
	"FACIAL_HAIR": "goatee",
	"FRECKLES": true,
	"OLD": true
},
{
	"ID": "m33",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "m34",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "moustache",
	"BUILD": "large",
	"FRECKLES": true
},
{
	"ID": "m35",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde",
	"FACIAL_HAIR": "moustache"
},
{
	"ID": "m36",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "lemmy"
},
{
	"ID": "m37",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true,
	"GLASSES": "eye"
},
{
	"ID": "m38",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "lemmy"
},
{
	"ID": "m39",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "m40",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye"
},
{
	"ID": "m41",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "m42",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "goatee"
},
{
	"ID": "m43",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "beard",
	"FRECKLES": true
},
{
	"ID": "m44",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "moustache",
	"GLASSES": "eye",
	"OLD": true
},
{
	"ID": "m45",
	"SKIN": "dark",
	"HAIR_COLOUR": "grey",
	"GLASSES": "eye",
	"OLD": true
},
{
	"ID": "m46",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "beard",
	"GLASSES": "eye",
	"OLD": true
},
{
	"ID": "m47",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "lemmy",
	"GLASSES": "eye",
	"OLD": true
},
{
	"ID": "m48",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"OLD": true
},
{
	"ID": "m49",
	"SKIN": "light",
	"HAIR_COLOUR": "bald"
},
{
	"ID": "m50",
	"SKIN": "light",
	"HAIR_COLOUR": "black",
	"OLD": true
},
{
	"ID": "m51",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "m52",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye",
	"OLD": true
},
{
	"ID": "m53",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "m54",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "beard",
	"OLD": true
},
{
	"ID": "m55",
	"SKIN": "light",
	"HAIR_COLOUR": "grey",
	"FRECKLES": true,
	"FACIAL_HAIR": "balbo",
	"GLASSES": "eye",
	"OLD": true
},
{
	"ID": "m56",
	"SKIN": "light",
	"HAIR_COLOUR": "grey",
	"FACIAL_HAIR": "balbo",
	"OLD": true
},
{
	"ID": "m57",
	"SKIN": "light",
	"HAIR_COLOUR": "grey"
},
{
	"ID": "m58",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "beard"
},
{
	"ID": "m59",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "m60",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "moustache"
},
{
	"ID": "m59",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "m61",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde",
	"GLASSES": "eye"
},
{
	"ID": "m62",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde",
	"FACIAL_HAIR": "goatee"
},
{
	"ID": "m63",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "lemmy",
	"OLD": true
},
{
	"ID": "m64",
	"SKIN": "dark",
	"HAIR_COLOUR": "bald",
	"FACIAL_HAIR": "lemmy"
},
{
	"ID": "m64",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "lemmy"
},
{
	"ID": "m65",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "moustache"
},
{
	"ID": "m66",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde"
},
{
	"ID": "m67",
	"SKIN": "light",
	"HAIR_COLOUR": "grey"
},
{
	"ID": "m68",
	"SKIN": "light",
	"HAIR_COLOUR": "grey",
	"FACIAL_HAIR": "moustache",
	"OLD": true
},
{
	"ID": "m69",
	"SKIN": "light",
	"HAIR_COLOUR": "bald",
	"FACIAL_HAIR": "moustache"
},
{
	"ID": "m70",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "moustache",
	"OLD": true
},
{
	"ID": "m71",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "m72",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye",
	"FACIAL_HAIR": "lemmy",
	"OLD": true
},
{
	"ID": "m73",
	"SKIN": "dark",
	"HAIR_COLOUR": "grey",
	"OLD": true
},
{
	"ID": "m74",
	"SKIN": "dark",
	"HAIR_COLOUR": "grey",
	"FACIAL_HAIR": "moustache",
	"OLD": true
},
{
	"ID": "m75",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "balbo",
	"GLASSES": "eye"
},
{
	"ID": "m76",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "goatee"
},
{
	"ID": "m77",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye"
},
{
	"ID": "m78",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "m79",
	"SKIN": "dark",
	"HAIR_COLOUR": "bald",
	"GLASSES": "eye",
	"FACIAL_HAIR": "beard"
},
{
	"ID": "m80",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde"
},
{
	"ID": "m81",
	"SKIN": "light",
	"HAIR_COLOUR": "bald",
	"FRECKLES": true
},
{
	"ID": "m82",
	"SKIN": "dark",
	"HAIR_COLOUR": "bald",
	"BUILD": "thin"
},
{
	"ID": "m83",
	"SKIN": "light",
	"HAIR_COLOUR": "grey",
	"BUILD": "thin",
	"OLD": true
},
{
	"ID": "m84",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "m85",
	"SKIN": "dark",
	"HAIR_COLOUR": "grey",
	"OLD": true
},
{
	"ID": "m86",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "lemmy"
},
{
	"ID": "m87",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "m88",
	"SKIN": "dark",
	"HAIR_COLOUR": "bald",
	"FACIAL_HAIR": "moustache"
},
{
	"ID": "m89",
	"SKIN": "dark",
	"HAIR_COLOUR": "bald",
	"FACIAL_HAIR": "beard",
	"OLD": true
},
{
	"ID": "m90",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "m91",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "m92",
	"SKIN": "light",
	"HAIR_COLOUR": "grey",
	"FACIAL_HAIR": "chops",
	"OLD": true
},
{
	"ID": "m93",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FACIAL_HAIR": "moustache"
}
]

var femaleFaceDict = [
{
	"ID": "f1",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f2",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde",
	"GLASSES": "sun"
},
{
	"ID": "f3",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f4",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "f5",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde"
},
{
	"ID": "f6",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f7",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f8",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"BUILD": "thin"
},
{
	"ID": "f9",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f10",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f11",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde",
	"BUILD": "thin"
},
{
	"ID": "f12",
	"SKIN": "light",
	"HAIR_COLOUR": "grey",
	"BUILD": "thin",
	"OLD": true,
	"FRECKLES": true
},
{
	"ID": "f13",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f14",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f15",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f16",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f17",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f18",
	"SKIN": "light",
	"HAIR_COLOUR": "grey"
},
{
	"ID": "f19",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde",
	"FRECKLES": true
},
{
	"ID": "f20",
	"SKIN": "light",
	"HAIR_COLOUR": "grey",
	"OLD": true,
	"FRECKLES": true
},
{
	"ID": "f21",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f22",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde"
},
{
	"ID": "f23",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f24",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f25",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f26",
	"SKIN": "dark",
	"HAIR_COLOUR": "black"
},
{
	"ID": "f27",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye"
},
{
	"ID": "f28",
	"SKIN": "light",
	"HAIR_COLOUR": "grey",
	"FRECKLES": true,
	"OLD": true
},
{
	"ID": "f29",
	"SKIN": "light",
	"HAIR_COLOUR": "grey",
	"OLD": true
},
{
	"ID": "f30",
	"SKIN": "light",
	"HAIR_COLOUR": "grey"
},
{
	"ID": "f31",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "f32",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f33",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f34",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"BUILD": "thin"
},
{
	"ID": "f35",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"BUILD": "thin"
},
{
	"ID": "f36",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "f37",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f38",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "f39",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye"
},
{
	"ID": "f40",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"BUILD": "very_thin",
	"GLASSES": "eye"
},
{
	"ID": "f41",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"BUILD": "very_thin",
	"GLASSES": "eye"
},
{
	"ID": "f42",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye",
	"OLD": true
},
{
	"ID": "f43",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"BUILD": "large",
	"FRECKLES": true
},
{
	"ID": "f44",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde"
},
{
	"ID": "f45",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f46",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye",
	"FRECKLES": true
},
{
	"ID": "f47",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye"
},
{
	"ID": "f48",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"BUILD": "large"
},
{
	"ID": "f49",
	"SKIN": "light",
	"HAIR_COLOUR": "grey"
},
{
	"ID": "f50",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true,
	"BUILD": "very_thin"
},
{
	"ID": "f51",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "f52",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"BUILD": "thin"
},
{
	"ID": "f53",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f54",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "f55",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "f56",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde",
	"FRECKLES": true
},
{
	"ID": "f57",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"BUILD": "very_thin"
},
{
	"ID": "f58",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye",
	"BUILD": "thin"
},
{
	"ID": "f59",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true,
	"BUILD": "thin"
},
{
	"ID": "f60",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye",
	"FRECKLES": true
},
{
	"ID": "f61",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f62",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f63",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f64",
	"SKIN": "dark",
	"HAIR_COLOUR": "grey"
},
{
	"ID": "f65",
	"SKIN": "dark",
	"HAIR_COLOUR": "grey",
	"FRECKLES": true
},
{
	"ID": "f66",
	"SKIN": "dark",
	"HAIR_COLOUR": "grey",
	"GLASSES": "eye",
	"OLD": true
},
{
	"ID": "f67",
	"SKIN": "dark",
	"HAIR_COLOUR": "grey"
},
{
	"ID": "f68",
	"SKIN": "light",
	"HAIR_COLOUR": "grey",
	"FRECKLES": true
},
{
	"ID": "f69",
	"SKIN": "dark",
	"HAIR_COLOUR": "grey"
},
{
	"ID": "f70",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true,
	"GLASSES": "eye"
},
{
	"ID": "f71",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "f72",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true,
	"GLASSES": "eye",
	"BUILD": "very_thin"
},
{
	"ID": "f73",
	"SKIN": "light",
	"HAIR_COLOUR": "green",
	"FRECKLES": true,
	"GLASSES": "eye",
	"BUILD": "large"
},
{
	"ID": "f74",
	"SKIN": "dark",
	"HAIR_COLOUR": "blonde",
	"FRECKLES": true
},
{
	"ID": "f75",
	"SKIN": "light",
	"HAIR_COLOUR": "blonde"
},
{
	"ID": "f76",
	"SKIN": "dark",
	"HAIR_COLOUR": "blonde",
	"FRECKLES": true
},
{
	"ID": "f77",
	"SKIN": "dark",
	"HAIR_COLOUR": "blonde"
},
{
	"ID": "f78",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye",
	"OLD": true
},
{
	"ID": "f79",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f80",
	"SKIN": "dark",
	"HAIR_COLOUR": "grey"
},
{
	"ID": "f81",
	"SKIN": "dark",
	"HAIR_COLOUR": "blonde"
},
{
	"ID": "f82",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f83",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "f84",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "f85",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "f86",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f87",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"BUILD": "very_large"
},
{
	"ID": "f88",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye",
	"FRECKLES": true
},
{
	"ID": "f89",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"GLASSES": "eye"
},
{
	"ID": "f90",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f91",
	"SKIN": "light",
	"HAIR_COLOUR": "brown",
	"BUILD": "very_thin"
},
{
	"ID": "f92",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"BUILD": "thin"
},
{
	"ID": "f93",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f94",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f95",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f96",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f97",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f98",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f99",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f100",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown",
	"FRECKLES": true
},
{
	"ID": "f101",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f102",
	"SKIN": "dark",
	"HAIR_COLOUR": "brown"
},
{
	"ID": "f103",
	"SKIN": "light",
	"HAIR_COLOUR": "brown"
}
]
