extends Node2D

class_name Unit

"""
var facing_direction
signal left_clicked
signal right_clicked
signal stopped_moving
signal whistle
"""

var selected = false

onready var avatar = get_node("avatar")
onready var sprite = get_node("sprite")
onready var animation_player = get_node("animationplayer")
onready var tween = get_node("tween")

var ID : int

## BASIC STATS
var first_name : String
var last_name : String
var initials : String
var nick_name : String
var face : String
var gender : int
var place_of_birth : String
var experience_points : float
var level : int
var birth_day : int
var birth_month : int
var birth_year : int

# occupation/s preapocalypse
var occupations = []

# Perks/Drawbacks
var traits = []

# Determine age by using in-game date/time
var age : int = (
# By month
(g.year - birth_year - 1) if (g.month < birth_month)
else g.year - birth_year if g.month > birth_month
# By day
else g.year - birth_year - 1 if g.day < birth_day
else g.year - birth_year
) 

# If old, roll randomly for death by old age
onready var old : bool = age >= g.LIFE_EXPECTANCY

var health : int
var current_health : int

var injured : int = (current_health < health) 
var severely_injured : int = (
(true if health < 4 and current_health == 1
else true if current_health <= health / 4.0
else false)) 

var stamina : int
var current_stamina : int
var metabolism : int
var hunger : int
var thirst : int

## PRIMARY STATS
var strength : int
var strong : bool = strength >= g.UPPER_THRESHOLD
var weak : bool = strength <= g.LOWER_THRESHOLD

var constitution : int
var healthy : bool  = constitution >= g.UPPER_THRESHOLD
var sickly : bool  = constitution <= g.LOWER_THRESHOLD

var dexterity : int
var dexterous : bool = dexterity >= g.UPPER_THRESHOLD
var clumsy : bool = dexterity <= g.LOWER_THRESHOLD

var agility : int
var fast : bool = agility >= g.UPPER_THRESHOLD
var slow : bool = agility <= g.LOWER_THRESHOLD

# -made redundant by knowledge and social?
var wisdom : int
var wise : bool = wisdom >= g.UPPER_THRESHOLD
var inexperienced : bool = wisdom <= g.LOWER_THRESHOLD

var charisma : int
var charismatic : bool = charisma >= g.UPPER_THRESHOLD
var boring : bool = charisma <= g.LOWER_THRESHOLD

## SECONDARY STATS
var leadership : int
var marksmanship : int
var electronics : int
var mechanical : int
var engineering : int
var medicine : int

## PERSONALITY STATS
# Political
var militarism : int
var militant : bool = militarism >= g.UPPER_THRESHOLD
var pacifist : bool = militarism <= g.LOWER_THRESHOLD

var spiritualism : int
var spiritual : bool = spiritualism >= g.UPPER_THRESHOLD
var pragmatic : bool = spiritualism <= g.LOWER_THRESHOLD

var authoritarianism : int
var authoritarian : bool = authoritarianism >= g.UPPER_THRESHOLD
var egalitarian : bool = authoritarianism <= g.LOWER_THRESHOLD

var open_mindedness : int
var open_minded : bool = open_mindedness >= g.UPPER_THRESHOLD
var closed_minded : bool = open_mindedness <= g.LOWER_THRESHOLD

# Social
var kindness : int
var kind : bool = kindness >= g.UPPER_THRESHOLD
var cruel : bool = kindness <= g.LOWER_THRESHOLD

var honesty : int
var honest : bool = honesty >= g.UPPER_THRESHOLD
var deceitful : bool = honesty <= g.LOWER_THRESHOLD

var trust : int
var trusting : bool = trust >= g.UPPER_THRESHOLD
var paranoid : bool = trust <= g.LOWER_THRESHOLD

var social : int
var sociable : bool = social >= g.UPPER_THRESHOLD
var unsociable : bool = social <= g.LOWER_THRESHOLD

# Mental
var optimism : int
var optimist : bool = optimism >= g.UPPER_THRESHOLD
var pessimist : bool = optimism <= g.LOWER_THRESHOLD

var drive : int
var driven : bool = drive >= g.UPPER_THRESHOLD
var lazy : bool = drive <= g.LOWER_THRESHOLD

var courage : int
var courageous : bool = courage >= g.UPPER_THRESHOLD
var cowardly : bool = courage <= g.LOWER_THRESHOLD

var knowledge : int
var smart : bool = knowledge >= g.UPPER_THRESHOLD
var dumb : bool = knowledge <= g.LOWER_THRESHOLD

# DEBUG:used to check if unit has non of the above personality bools set to true
var unremarkable : bool = (
!militant and !pacifist and !spiritual and !pragmatic and !authoritarian and 
!egalitarian and !open_minded and !closed_minded and !kind and !cruel and !honest 
and !deceitful and !sociable and !unsociable and !trusting and !paranoid and 
!strong and !weak and !healthy and !sickly and !fast and !slow and !dexterous and 
!clumsy and !optimist and !pessimist and !driven and !lazy and !smart and !dumb 
and !courageous and !cowardly
)

## SPEECH
var wants_to_talk : bool
var has_something_to_say : bool
var whats_on_mind : String

## INVENTORY
var inventory : = []
var wearing : = []
var wielding : = []

## MOVEMENT
var moving_fast : = false
var sneaking : = false
var standing = true if not crouching and not prone else false
var crouching : = false
var prone : = false

# Used for multiple square 'sliding' movement
var speed : = 500.0 if moving_fast else 250.0
var gravity : = 0
var path : = PoolVector2Array() setget _set_path

# Position on gridmap
var z_level : int

## ACTION POINTS and TURN BASED COMBAT

# Base Action Points -100 for humans
var AP_base : int
# Modifier for Action Points -high energy etc will raise, carrying too much etc will lower
var AP_modifier : int
# Current action points
var AP : = 100
# Triggered true if second turn used on extra movement -used for turn based combat
var second_turn_used : bool = false

# Animation state -unused
var state

func _init():
	# Unit counter
	ID = g.unit_ID_count
	g.unit_ID_count += 1

# Called when the node enters the scene tree for the first time.
func _ready():
	# Human generator
	var humanGen = load("res://scenes/engine/generation/HumanGenerator.tscn").instance()
	humanGen._initial_setup(self, g.NPC_HUMAN_STAT_MIN_normal, g.NPC_HUMAN_STAT_MAX_normal)
	humanGen._generate_human(self)
	_set_initials()
	
	# Give char random item set from prefabs
	_give_random_inventory_prefab()
	
	# Equip clothes/items they may be carrying
	_initial_equip()
	
	# Overhead name initials
	$initials_label.text = _get_initials()
	print(first_name + " " + last_name)
	
	# Set male/female sprite
	_set_sprite()

# Performed on each step
func _process(delta : float):
	var move_distance = speed * delta
	_move_along_path(move_distance)

## ACTION POINTS
func _refresh_AP():
	AP += AP_base + AP_modifier

func _has_enough_AP(AP_needed : int) -> bool:
	if AP >= AP_needed:
		return true
	return false

## INVENTORY
func _give_random_inventory_prefab():
	var prefabs = load("res://scenes/engine/prefabs/Prefabs.tscn").instance()
	var random = g._get_random(0, prefabs.inventory.size() - 1)
	for item in prefabs.inventory[random]["ITEMS"]:
		inventory.append(items._get_item_by_ID(item))

# Equips character with all held equipable items (clothes, weapons, etc) -used at the beginning of the game
func _initial_equip():
	_strip()
	
	for item in inventory:
		_equip_item(item)
	"""
	var list = ""
	
	for i in range(0, inventory.size()):
		list += g._seperate_strings_with_commas(i, inventory.size())
		list += _equip_item(inventory[i])
	
	return (first_name + " equipped " + list + "!")
	"""

# Gets characters inventory, optional bool to exclude items in wielding and wearing
func _get_inventory(include_wearing : = true):
	if include_wearing:
		return inventory
	else:
		var amended_inventory = []
		for item in wielding:
			amended_inventory.append(item)
		for item in wearing:
			amended_inventory.append(item)
		for item in inventory:
			if amended_inventory.has(item):
				amended_inventory.erase(item)
			else:
				amended_inventory.append(item)
		return amended_inventory

# Shows item on sprite
func _equip_item(item):
	if item != null:
		if item["CATAGORY"].has(items.WEAPON):
			_display_worn(item)
			wielding.append(item)
			return item["NAME"]
		if item["CATAGORY"].has(items.CLOTHING):
			_display_worn(item)
			wearing.append(item)
			return item["NAME"]
		
		return false

func _display_worn(item):
	var new_gear_sprite = Sprite.new()
	new_gear_sprite.name = item["NAME"]
	new_gear_sprite.centered = true
	new_gear_sprite.texture = load(g.ITEM_SPRITE_DIR + item["NAME"] + "_worn.png")
	if new_gear_sprite.texture == null:
		new_gear_sprite.texture = load(g.ITEM_SPRITE_DIR + item["ALT"] + "_worn.png")
		
	
	if item.has("COVERS"):
		if item["COVERS"][0] == items.HEAD:
			$body/wearing/head.add_child(new_gear_sprite)
		elif item["COVERS"][0] == items.FACE:
			$body/wearing/face.add_child(new_gear_sprite)
		elif item["COVERS"][0] == items.SHOULDERS:
			$body/wearing/shoulders.add_child(new_gear_sprite)
		elif item["COVERS"][0] == items.UPPER_ARMS or item["COVERS"][0] == items.LOWER_ARMS or item["COVERS"][0] == items.ELBOWS:
			$body/wearing/arms.add_child(new_gear_sprite)
		elif item["COVERS"][0] == items.HANDS:
			$body/wearing/hands.add_child(new_gear_sprite)
		elif item["COVERS"][0] == items.TORSO:
			$body/wearing/torso.add_child(new_gear_sprite)
		elif item["COVERS"][0] == items.WAIST:
			$body/wearing/waist.add_child(new_gear_sprite)
		elif item["COVERS"][0] == items.UPPER_LEGS or item["COVERS"][0] == items.LOWER_LEGS or item["COVERS"][0] == items.KNEES:
			$body/wearing/legs.add_child(new_gear_sprite)
		elif item["COVERS"][0] == items.KNEES:
			$body/wearing/knees.add_child(new_gear_sprite)
		elif item["COVERS"][0] == items.FEET:
			$body/wearing/feet.add_child(new_gear_sprite)
	else:
		$body/wearing/hands.add_child(new_gear_sprite)
		

#DEBUG:only used to take editor test clothes off
func _strip():
	for body_part in $body/wearing.get_children():
		for i in range(0, body_part.get_child_count()):
			body_part.get_child(i).queue_free()

# Get first initial from first, nick and last names respectively and sets the initials variable
func _set_initials():
	var final_initials = ""
	if first_name != null and first_name != "":
		final_initials += first_name.left(1)
	if initials == "" and nick_name != null and nick_name != "":
		final_initials += nick_name.left(1)
	if last_name != null and last_name != "":
		final_initials += last_name.left(1)
	
	initials = final_initials if final_initials != "" else "?"

func _get_initials():
	return initials

func _set_sprite():
	if gender == e.genders.MALE:
		$body.texture = load(g.UNIT_SPRITE_DIR + "male.png")
	elif gender == e.genders.FEMALE:
		$body.texture = load(g.UNIT_SPRITE_DIR + "female.png")
	else:
		$body.texture = load(g.UNIT_SPRITE_DIR + "male.png")

func _on_Unit_left_clicked():
	print("LEFT")

## MOVEMENT

# Changing stance
func _prone():
	crouching = false
	prone = true

func _crouch():
	prone = false
	crouching = true

func _stand():
	crouching = false
	prone = false

func _move_fast():
	_stop_sneaking()
	moving_fast = true

func _stop_moving_fast():
	moving_fast = false

func _sneak():
	_stop_moving_fast()
	sneaking = true

func _stop_sneaking():
	sneaking = false

# Animated pathfinding path setter
func _set_path(value : PoolVector2Array):
	path = value
	if value.size() == 0:
		return
	set_process(true)

func _move_along_path(distance : float):
	var start_point = position
	for i in range(path.size()):
		var distance_to_next = start_point.distance_to(path[0])
		if distance <= distance_to_next and distance >= 0.0:
			position = start_point.linear_interpolate(path[0], distance / distance_to_next)
			break
		elif distance < 0.0:
			position = path[0]
			emit_signal("stopped_moving")
			set_process(false)
		distance -= distance_to_next
		start_point = path[0]
		path.remove(0)


## PERSONALITY

func _get_occupation_strings_slashes() -> String:
	var occupations_array = occupations_resource._get_occupation_strings(occupations, gender)
	var occupations_string = s._seperate_with_slashes(occupations_array)
	return occupations_string

func _get_occupations():
	return occupations

func _get_traits_strings_slashes() -> String:
	var traits_array = traits_resource._get_traits(traits)
	var traits_string = s._seperate_with_slashes(traits_array)
	return traits_string
	
func _get_traits():
	return traits


## SKILLS

# Determines HP
func _get_health(bonus_con : = 0, bonus_str : = 0) -> float:
	return ((constitution + bonus_con) * 0.8 + (strength + bonus_str) * 0.2) * 0.86 + 14

func _get_stamina(bonus_con : = 0, bonus_agi : = 0) -> float:
	return ((constitution + bonus_con) * 0.75 + (agility + bonus_agi) * 0.25) * 0.81 + 19

func _get_healing(bonus_con : = 0, bonus_med : = 0) -> float:
	return ((constitution + bonus_con) * 0.85 + (medicine + bonus_med) * 0.15) * 0.86 + 14

func _get_movement(bonus_agi : = 0) -> float:
	return 4 + ((agility + bonus_agi) * 0.06)

func _get_sneak(bonus_agi : = 0, bonus_wis : = 0) -> float:
	return (agility + bonus_agi) * 0.7 + (wisdom + bonus_wis) * 0.3

func _get_carry(bonus_str : = 0) -> int:
	return (strength + bonus_str) + 150 / 4

func _get_talk(bonus_cha : = 0) -> int:
	return (charisma + bonus_cha)

func _get_initiative(bonus_dex : = 0, bonus_agi : = 0, bonus_con : = 0) -> float:
	return (dexterity + bonus_dex) * 0.45 + (agility + bonus_agi) * 0.45 + (constitution + bonus_con) * 0.1

func _get_dodge(bonus_agi : = 0, bonus_dex : = 0) -> float:
	return ((agility + bonus_agi) * 0.8 + (dexterity + bonus_dex) * 0.8) / 2

func _get_learn(bonus_wis : = 0) -> int:
	return (wisdom + bonus_wis)

func _get_teach(bonus_ldr : = 0) -> int:
	return (leadership + bonus_ldr)

func _get_block(bonus_str : = 0, bonus_con : = 0) -> float:
	return ((strength + bonus_str) * 0.8 + (constitution + bonus_con) * 0.8) / 2

func _get_melee_aim(bonus_agi : = 0) -> float:
	return (agility + bonus_agi) * 0.81 + 9

func _get_melee_dmg(bonus_str : = 0, bonus_eng : = 0) -> float:
	return (strength + bonus_str) * 0.14 + (engineering + bonus_eng) * 0.1 + 1

func _get_melee_ap(bonus_wis : = 0) -> float:
	return (wisdom + bonus_wis) * 0.1 + 6

func _get_ranged_aim(bonus_mrk : = 0) -> float:
	return (marksmanship + bonus_mrk) * 0.8 + 10

func _get_ranged_dmg(bonus_eng : = 0) -> float:
	return (engineering + bonus_eng) * 0.1

func _get_ranged_ap(bonus_dex : = 0) -> float:
	return (dexterity + bonus_dex) * 0.1 + 6

func _get_explosives(bonus_wis : = 0, bonus_dex : = 0, bonus_elc : = 0, bonus_eng : = 0) -> float:
	return (wisdom + bonus_wis) * 0.1 + (dexterity + bonus_dex) * 0.1 + (electronics + bonus_elc) * 0.4 + (engineering + bonus_eng) * 0.4

func _get_lockpick(bonus_mec : = 0, bonus_dex : = 0, bonus_wis : = 0) -> float:
	return (mechanical + bonus_mec) * 0.8 + (dexterity + bonus_dex) * 0.1 + (wisdom + bonus_wis) * 0.1

func _get_bluff(bonus_cha : = 0, bonus_str : = 0, bonus_con : = 0) -> float:
	return (charisma + bonus_cha) * 0.5 + (strength + bonus_str) * 0.35 + (constitution + bonus_con) * 0.15

func _get_medicine(bonus_med : = 0, bonus_dex : = 0, bonus_wis : = 0) -> float:
	return (medicine + bonus_med) * 0.8 + (dexterity + bonus_dex) * 0.1 + (wisdom + bonus_wis) * 0.1

func _get_electronics(bonus_elc : = 0, bonus_dex : = 0, bonus_wis : = 0) -> float:
	return (electronics + bonus_elc) * 0.8 + (dexterity + bonus_dex) * 0.1 + (wisdom + bonus_wis) * 0.1

func _get_mechanical(bonus_mec : = 0, bonus_dex : = 0, bonus_wis : = 0) -> float:
	return (mechanical + bonus_mec) * 0.8 + (dexterity + bonus_dex) * 0.1 + (wisdom + bonus_wis) * 0.1

func _get_engineering(bonus_eng : = 0, bonus_dex : = 0, bonus_wis : = 0) -> float:
	return (engineering + bonus_eng) * 0.8 + (dexterity + bonus_dex) * 0.1 + (wisdom + bonus_wis) * 0.1


""" TRASH CODE -might be useful later
func _wait_turn():
	pass

# Currently hard coded into the MainGame script -unused
func _simple_move(direction : Vector2, z_level = 0):
	pass




# Facing directions
enum {DOWN_LEFT, DOWN, DOWN_RIGHT,
		LEFT, RIGHT,
		UP_LEFT, UP, UP_RIGHT}

# Walking/running/facing directions
enum {IDLE_DOWN, IDLE_UP, IDLE_LEFT, IDLE_RIGHT,
		WALK_DOWN, WALK_UP, WALK_LEFT, WALK_RIGHT,
		RUN_DOWN, RUN_UP, RUN_LEFT, RUN_RIGHT}

func _input(event):
	# Movement directions on numpad
	if event.is_action_pressed("numeral1"):
		_move(DOWN_LEFT)
		_change_state(IDLE_LEFT)
	elif event.is_action_pressed("numeral2"):
		_move(DOWN)
		_change_state(IDLE_DOWN)
	elif event.is_action_pressed("numeral3"):
		_move(DOWN_RIGHT)
		_change_state(IDLE_RIGHT)
	elif event.is_action_pressed("numeral4"):
		_move(LEFT)
		_change_state(IDLE_LEFT)
	elif event.is_action_pressed("numeral6"):
		_move(RIGHT)
		_change_state(IDLE_RIGHT)
	elif event.is_action_pressed("numeral7"):
		_move(UP_LEFT)
		_change_state(IDLE_LEFT)
	elif event.is_action_pressed("numeral8"):
		_move(UP)
		_change_state(IDLE_UP)
	elif event.is_action_pressed("numeral9"):
		_move(UP_RIGHT)
		_change_state(IDLE_RIGHT)
	# Wait turn if 5
	elif event.is_action_pressed("numeral5"):
		_wait_turn()

func _move(direction):
	match direction:
		DOWN_LEFT:
			position += Vector2.DOWN * 16
			position += Vector2.LEFT * 16
		DOWN:
			tween.interpolate_property(self, "transform:position", position, Vector2.DOWN * 16, 1 if running else 0.6, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		DOWN_RIGHT:
			position += Vector2.DOWN * 16
			position += Vector2.RIGHT * 16
		LEFT:
			position += Vector2.LEFT * 16
		RIGHT:
			position += Vector2.RIGHT * 16
		UP_LEFT:
			position += Vector2.UP * 16
			position += Vector2.LEFT * 16
		UP:
			position += Vector2.UP * 16
		UP_RIGHT:
			position += Vector2.UP * 16
			position += Vector2.RIGHT * 16

func _change_state(new_state):
	match new_state:
		IDLE_LEFT:
			animation_player.play("idle_left")
		IDLE_RIGHT:
			animation_player.play("idle_right")
		IDLE_UP:
			animation_player.play("idle_up")
		IDLE_DOWN:
			animation_player.play("idle_down")
		
		WALK_LEFT:
			animation_player.play("walk_left")
		WALK_RIGHT:
			animation_player.play("walk_right")
		WALK_UP:
			animation_player.play("walk_up")
		WALK_DOWN:
			animation_player.play("walk_down")
		
		RUN_LEFT:
			animation_player.play("run_left")
		RUN_RIGHT:
			animation_player.play("run_right")
		RUN_UP:
			animation_player.play("run_up")
		RUN_DOWN:
			animation_player.play("run_down")
"""
