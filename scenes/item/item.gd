extends Node2D

class_name Item

onready var sprite = $sprite
var ID : int
var weight : float


func _load_item(item_dictionary : Dictionary):
	sprite = load(g.ITEMDIR + item_dictionary["NAME"])
	name = item_dictionary["NAME"]
	weight = item_dictionary["WEIGHT"]
