extends Node2D

## ENGINE
#dictionary of tiles and their properties -weights, walkable etc
var tilemap_rules = preload("res://scenes/tilemaps/Tilemap_Rules.tscn").instance()

var format_tileinfo_string = "L%s%s %s:[%s]"

## CONSTANTS
# Highest possible z-level
const MAX_HEIGHT = 10

# How many single turns allocated per combat round -used for turn based combat
const TURN_LENGTH = 6

## UNITS
#currently selected player -player 1 when game loads
var current_player : Node2D

## UNITS/TESTING
#DEBUG-testing: friendly unit not in player's party
var friendly : Node2D

#DEBUG-testing: hostile unit
var enemy : Node2D
var units_in_world : = []
var items_in_world : = []

## UI
onready var ui = $Camera/UI
onready var clock = $Camera/UI/Clock
onready var gamelog = $Camera/UI/gamelog
onready var z_level_label = $Camera/UI/level

## MOVEMENT LINES
onready var movement_line : = $movement_line
onready var sprint_line : = $sprint_line
onready var movement_numbers : = $movement_numbers

## CAMERA
onready var camera : = $Camera

## WORLD
var current_level : int
var levels : = []
var highest_level : int

## STATES
enum ui_state {
	IDLE,
	IDLE_TURN_BASED,
	SAFE_MODE,
	PLAYING_TURN,
	EXAMINE,
	EQUIP,
	WEAR,
	INVENTORY,
	GRAB,
	DROP,
	CLOSE
}
var current_ui_state : int = ui_state.IDLE

## CONTROL BOOLS
var has_control : = true
var turn_taken : = false
var safe_mode_enabled : = true
var safe_mode : = false

## _READY()
func _ready() -> void:
	self.add_child(tilemap_rules)

	# Grab number of Z-levels in world
	# TODO:currently crashes game on launch if current_level != number of levels in the editor
	current_level = 5

	# TODO:procedural generation -not even close to starting
	# _add_levels(MAX_HEIGHT)
	# DEBUG
	_load_test_level1()

	# Add player characters and random units into the world
	_add_units()

	# Add random items into the world
	_add_items()

	if current_player.z_level == current_level:
		pass
	elif current_player.z_level > current_level:
		for i in range(current_level, current_player.z_level - current_level):
			_display_level()
	else:
		pass

	# move camera to player 1
	current_level = 0
	_get_level(current_level).visible = true
	camera._snap_to_square(current_player.position, current_player.z_level)

	# does nothing yet
	set_process(false)
	if current_player.occupations != []:
		for occupation in current_player.occupations:
			print(str(occupations_resource._get_occupation(occupation)["NAME"]))
	if current_player.traits != []:
		for trait in current_player.traits:
			print(str(traits_resource._get_trait(trait)["NAME"]))

## _PROCESS() -unused
func _process(delta) -> void:
	pass


## PLAY TURN
# Activates when player has taken their turn, recurs until player gets enough action points to take another turn
func _play_turn() -> void:
	"""
	Proposed turn order ala CDDA
	1.  Events are processed
	2.  Missions are processed
	3.  Time display is processed if you don't have a watch
	4.  Check the player for overdose, hunger, thirst, fatigue, healing mutations, pain and radiation levels
	5.  Autosaves if appropriate
	6.  Checks if the inconspicuous trait kicks in
	7.  Corpses are checked for deletion
	8.  Player performs actions
	9.  The player scent is updated
	10. Vehicle moves
	11. Fields are processed
	12. Active items (such as active flashlights) are processed
	13. Creatures move (unfriendly act first)
	14. Body temperature is updated
	15. Skill rust is checked
	16. Morale is updated
	"""
	# Disables player control to avoid unnecessary and potentially harmful input while the turn is playing out
	has_control = false
	current_ui_state = ui_state.PLAYING_TURN


	g.seconds += TURN_LENGTH
	clock._calculate_time()
	clock._print_date_and_time()

	# AI TAKES TURN
	_follow(friendly, current_player)
	_single_move(enemy, g._random_direction(true))

	for unit in units_in_world:
		if _get_level(unit.z_level).get_cellv(unit.position) == -1:
			_check_for_floor(unit)

	current_player._refresh_AP()

	# DEBUG-testing: eventually this will be a global func that applies to all units
	# that have AP remaining
	friendly._refresh_AP()
	enemy._refresh_AP()

	# Keep playing turn if current_player has negative AP
	if current_player.AP < 0:
		_play_turn()
	else:
		has_control = true
		current_ui_state = ui_state.IDLE
		turn_taken = false

## PLAYER FUNCTIONS
# Opens equip dialogue box
func _equip():
	pass

# Enables the peek action
func _peek(direction):
	pass

## NPC FUNCTIONS
func _follow(follower : Node2D, leader : Node2D) -> void:
	# Get distance between follower and leader
	var destination = _set_destination(follower, leader.position)
	
	# TODO:change destination to a Vector2 while using _single_move()
	if destination.size() > 1 and !_has_unit(destination[1], 0):
		_single_move(follower, destination[1] - follower.position)

func _attack(attacker : Node2D, victim : Node2D) -> void:
	# Get distance between attacker and victim
	var destination = _set_destination(attacker, victim.position)
	# Remove a square to account for personal space
	destination.pop_back()
	# if using ranged weapon and can hit victim:
		# Shoot
	# else if using melee/unarmed:
		# if within range of victim:
			# Attack
		# else:
			# Move towards victim

## OBJECT MOVEMENT
# Base function for getting the distance between two things
func _set_destination(start : Node2D, destination : Vector2) -> Vector2:
	# Get the correct z-level surface to work with -possible bandaid
	var tilemap = _get_level(start.z_level)
	# Calculate the path between the two objects
	var path = tilemap._get_path(start.position, destination)

	return path

"""
#DEBUG:using simplified movement function below ala cataclysm (_single_move())
#(ie: move one square per keypress (if possible))

# Gets distance between an object and its destination
func _move_thing(thing : Node2D, distance : Vector2, camera_follows : = false) -> void:
	var destination = _set_destination(thing, distance)
	# Sets things path
	thing.path = destination
	if camera_follows:
		camera._snap_to_square(destination, thing.z_level)
"""

# Cataclysm style single-square movement
func _single_move(thing : Node2D, direction : Vector2, camera_follows : = false, players_turn : = false) -> void:
	thing.position += direction
	if camera_follows:
		camera._snap_to_square(thing.global_position, thing.z_level)
	if players_turn:
		turn_taken = true

# Moves thing up a level, will probably be called before any necessary
# position alterations due to the TileMap offset per z-level --UNTESTED
func _move_up_level(thing : Node2D, camera_follows : = false) -> void:
	_get_units_on_level(thing.z_level).remove_child(thing)
	thing.z_level += 1
	_get_units_on_level(thing.z_level).add_child(thing)
	if camera_follows:
		camera._snap_to_square(thing.global_position, thing.z_level)

# Moves thing down a level, as above
func _move_down_level(thing : Node2D, camera_follows : = false) -> void:
	_get_units_on_level(thing.z_level).remove_child(thing)
	thing.z_level -= 1
	_get_units_on_level(thing.z_level).add_child(thing)
	if camera_follows:
		camera._snap_to_square(thing.global_position, thing.z_level)

func _check_for_floor(thing : Node2D):
	# If there is no floor beneath thing
	if _get_level(thing.z_level)._get_tile_ID(thing.position) == -1:
		# Thing starts falling, gravity is applied
		thing.gravity += 1# + thing.gravity / 3
		# Find out the maximum distance that thing will fall this turn if not stopped by an obstacle
		var distance_to_ground_after_turn = thing.z_level - thing.gravity
		# Go down level by level to make sure thing doesn't hit an obstacle
		for i in range(thing.z_level, distance_to_ground_after_turn, -1):
			# Bring thing down a z-level
			_move_down_level(thing, true)
			if _get_level(thing.z_level)._get_tile_ID(thing.position) != -1:
				print(thing.name + " landed!")
				thing.gravity = 0
				return
			else:
				print(thing.name + " is falling! (Z" + str(thing.z_level) + "/" + str(_get_level(thing.z_level).world_to_map(thing.position)) + ")" )

## FEEDBACK
# Puts important information into the messagebox on the right side of the UI
func _add_message(message : String) -> void:
	var new_message = Label.new()
	new_message.text = message
	new_message.align = Label.ALIGN_RIGHT
	gamelog.add_child(new_message)
	# TODO:further formatting of the label
	# TODO:add timer to kill unimportant messages

func _play_sound():
	pass

## TILE-BASED DETECTION
# TODO: move to TileMap script
func _get_tile_w2m_position(point, z_level):
	return _get_level(z_level).world_to_map(point)#  * g._int_to_Vector2(g.TILESET_SIZE)

# Check if square has a unit on it -used for pathfinding
func _has_unit(point : Vector2, z_level : int) -> bool:
	var surface = _get_level(z_level)
	for unit in _get_units_on_level(z_level).get_children():
		if surface.world_to_map(unit.position) == surface.world_to_map(point):
			return true
	return false

func _has_wall(point : Vector2, z_level : int) -> bool:
	if _get_tile_name(_get_level(z_level)._get_tile_ID(point)) == "wall":
		return true
	else:
		return false

# Check if square has a wall on it -used for pathfinding
func _has_obstacle(point : Vector2, z_level : int) -> bool:
	if _has_unit(point, z_level):
		print("UNIT HERE")
		return true
	elif _has_wall(point, z_level):
		print("WALL HERE")
		return true
	else:
		return false

# Check if square can be traversed or if it is blocked
func _walkable(point : Vector2, z_level : int) -> bool:
	if _has_unit(point, z_level):
		return false
	elif _has_obstacle(point, z_level):
		return false
	return true

# Returns the basic Node (units) containing the units on a particular TileMap
func _get_units_on_level(z_level):
	return self.get_node(str(z_level) + "/units")

# Returns array of all tiles surrounding a square, optionally include the origin tile
func _get_tiles_around_tile(point : Vector2, include_centre_tile : = false):
	var tiles : = []

	tiles.append(point + g.NORTH)
	tiles.append(point + g.SOUTH)
	tiles.append(point + g.EAST)
	tiles.append(point + g.WEST)
	tiles.append(point + g.NORTHEAST)
	tiles.append(point + g.NORTHWEST)
	tiles.append(point + g.SOUTHEAST)
	tiles.append(point + g.SOUTHWEST)
	if include_centre_tile:
		tiles.append(point)

	return tiles

# Scan seeable area for npcs and items
func _check_surroundings():
	pass

# Detects unknown units
func _see_threat(position : Vector2, z_level : int) -> void:
	# if threat seen
	_enable_safe_mode()

## WORLD STATE
# Will check after a key is pressed and takes the appropriate action
func _check_ui_state() -> void:
	if current_ui_state == ui_state.CLOSE:
		 #_close(current_player.point)
		pass
	if current_ui_state == ui_state.EQUIP:
		_equip()

# Returns gamestate back to neutral/keypad movement
func _back_to_idle() -> void:
	# TODO:close all dialogue windows
	current_ui_state = ui_state.IDLE

## LINE OF SIGHT
func _draw_line_of_sight() -> void:
	pass

## XCOM TURN-BASED MOVEMENT
# Draws movement line -used in turn-based movement
func _draw_movement_line(unit : Node2D, end_pos : Vector2) -> void:
	movement_line.visible = true
	sprint_line.visible = true
	var movement_path = _set_destination(unit, end_pos)
	var sprint_path : PoolVector2Array
	sprint_path = _adjust_move_to_movement_points(movement_path, sprint_path, unit.AP)
	_clear_movement_numbers()

	var square_counter = 1
	# Add movement square number labels
	for i in range(movement_path.size()):
		if i != 0:
			var new_number =_new_movement_number(square_counter, movement_path[i])
			new_number.add_color_override("font_color", Color(0,1,0))
			new_number.rect_position = _get_current_level().world_to_map(movement_path[i]) * g.TILESET_SIZE
			movement_numbers.add_child(new_number)
			square_counter += 1

	for i in range(sprint_path.size()):
		if i != 0:
			var new_number =_new_movement_number(square_counter, sprint_path[i])
			new_number.add_color_override("font_color", Color(1,0.1,0.1))
			new_number.rect_global_position = _get_current_level().world_to_map(sprint_path[i]) * g.TILESET_SIZE
			movement_numbers.add_child(new_number)
			square_counter += 1

	movement_line.points = movement_path
	sprint_line.points = sprint_path

# Clears movement lines -used in turn-based movement
func _clear_movement_line() -> void:
	movement_line.visible = false
	sprint_line.visible = false

# Removes the numbers from movement_numbers -used for turn-based click movement
func _clear_movement_numbers() -> void:
	for number in movement_numbers.get_children():
		number.queue_free()

"""
Limits a unit from being able to move beyond its movement_points as well as calculates
whether or not the unit has sprinted and thus forfeited the attack phase of their turn
-at the moment only used to draw the movement_line and sprint_lines, will probably have
to use a modified version of this for actual unit-based movement
"""
func _adjust_move_to_movement_points(movement_path : Array, sprint_path : PoolVector2Array, movement_points : int) -> PoolVector2Array:
	for i in range(movement_points, movement_path.size() - 1):
		movement_path.pop_back()

	for i in range(movement_points / 2, movement_path.size()):
		sprint_path.append(movement_path[i])

	for i in range(movement_points / 2, movement_path.size() - 1):
		movement_path.pop_back()

	return sprint_path

## CAMERA Z-LEVELS
# Returns a given Z-level
func _get_level(z_level):
	return self.get_node(str(z_level))

# Returns the current Z-level
func _get_current_level():
	return self.get_node(str(current_level))

# Hides the current Z-level of the map -used before going down a level
func _hide_level():
	_get_level(current_level).visible = false

# Reveals the current Z-level of the map -used after going up a level
func _display_level():
	_get_level(current_level).visible = true

func _enable_safe_mode() -> void:
	if safe_mode_enabled:
		current_ui_state = ui_state.SAFE_MODE
		if clock.game_soft_paused == false:
			clock.game_soft_paused = true

func _disable_safe_mode() -> void:
	current_ui_state = ui_state.IDLE

##WOLRD BUILDING
# Build initial levels
func _load_test_level1() -> void:
	var levels = [
	"res://scenes/tilemaps/test1.tscn",
	"res://scenes/tilemaps/test2.tscn",
	"res://scenes/tilemaps/test2.tscn",
	"res://scenes/tilemaps/test2.tscn",
	"res://scenes/tilemaps/test2.tscn",
	"res://scenes/tilemaps/test3.tscn"
	]
	# Add and draw levels to the game
	_add_test_level(levels)

func _add_test_level(levels : Array) -> void:
	# Goes through array of levels
	for i in range(levels.size()):
		# Create new level (Node2D) and add it to the main scene
		var new_level = load(levels[i]).instance()
		add_child(new_level)
		# Set name (ie: 0, 1, 2)
		new_level.name = str(i)
		# Place level correctly on the z(y)-axis for 3d effect
		new_level.position = Vector2(0, -g.TILESET_SIZE * i)
		# DEBUG:make level invisible - should do this elsewhere
		new_level.visible = false
	# Set the highest level to the amount of levels we just added - 1 to account for level 0
	highest_level = levels.size() - 1

# Adds units into the world and gives them a randomised start point -only half implemented
func _add_units() -> void:
	current_player = preload("res://scenes/unit/Unit.tscn").instance()
	friendly = preload("res://scenes/unit/Unit.tscn").instance()
	enemy = preload("res://scenes/unit/Unit.tscn").instance()

	# Add units to global unit array
	units_in_world.append(current_player)
	units_in_world.append(friendly)
	units_in_world.append(enemy)

	current_player.z_level = 5
	friendly.z_level = 0
	enemy.z_level = 0
	
	for unit in units_in_world:
		_get_level(unit.z_level).get_node("units").add_child(unit)
	
	# Place player
	current_player.position = (Vector2(5, -5) * g.TILESET_SIZE) + g._int_to_Vector2(g.TILE_OFFSET)

	# TODO:generate units and randomise their position
	friendly.position = (Vector2(1,1) * g.TILESET_SIZE) + g._int_to_Vector2(g.TILE_OFFSET)
	enemy.position = (Vector2(2, 2) * g.TILESET_SIZE) + g._int_to_Vector2(g.TILE_OFFSET)


func _add_items() -> void:
# TODO:will generate random items across the map/in containers etc
	pass

## TEMPLATES
# Template for number label that shows up on tiles to indicate movement options and distance
func _new_movement_number(square_counter : int, movement_path : Vector2) -> int:
	var new_number = Label.new()
	new_number.name = "movement_number" + str(square_counter)
	new_number.text = str(square_counter)
	new_number.add_color_override("font_color_", Color(0,1,0))
	new_number.align = Label.ALIGN_CENTER
	new_number.valign = Label.VALIGN_CENTER
	new_number.rect_size = Vector2(32,32)
	return new_number

# Returns the tileID (as per the Dictionary entry in tilemap_rules
func _get_tile_name(ID : int) -> String:
	for rule in tilemap_rules.rules:
		if rule["ID"].find(ID) != -1:
			return rule["NAME"]
	return "NOTHING"

# Returns the tileID (as per the Dictionary entry in tilemap_rules
func _get_tile_weight(ID : int) -> int:
	for rule in tilemap_rules.rules:
		if rule.has("WEIGHT"):
			if rule["ID"].find(ID) != -1:
				return rule["WEIGHT"]
	return 0

## CONTROLS
func _input(event):
	if has_control and current_ui_state != ui_state.SAFE_MODE:
		# PLAYER CONTROLS
		# Movement directions on numpad
		if current_ui_state == ui_state.IDLE:
			#TODO:change _has_unit to _walkable to pick up walls and other obstacles
			if event.is_action_pressed("num_1"):
				if !_has_obstacle(current_player.position + g.SOUTHWEST, current_player.z_level):
					_single_move(current_player, g.SOUTHWEST, true, true)
			elif event.is_action_pressed("num_2"):
				if !_has_obstacle(current_player.position + g.SOUTH, current_player.z_level):
					_single_move(current_player, g.SOUTH, true, true)
			elif event.is_action_pressed("num_3"):
				if !_has_obstacle(current_player.position + g.SOUTHEAST, current_player.z_level):
					_single_move(current_player, g.SOUTHEAST, true, true)
			elif event.is_action_pressed("num_4"):
				if !_has_obstacle(current_player.position + g.WEST, current_player.z_level):
					_single_move(current_player, g.WEST, true, true)
			elif event.is_action_pressed("num_6"):
				if !_has_obstacle(current_player.position + g.EAST, current_player.z_level):
					_single_move(current_player, g.EAST, true, true)
			elif event.is_action_pressed("num_7"):
				if !_has_obstacle(current_player.position + g.NORTHWEST, current_player.z_level):
					_single_move(current_player, g.NORTHWEST, true, true)
			elif event.is_action_pressed("num_8"):
				if !_has_obstacle(current_player.position + g.NORTH, current_player.z_level):
					_single_move(current_player, g.NORTH, true, true)
			elif event.is_action_pressed("num_9"):
				if !_has_obstacle(current_player.position + g.NORTHEAST, current_player.z_level):
					_single_move(current_player, g.NORTHEAST, true, true)
			# Wait turn if 5
			elif event.is_action_pressed("num_5"):
				turn_taken = true
				pass

		if event.is_action_pressed("space"):
			clock._start_stop_clock()

		if event.is_action_pressed("ui_cancel"):
			# Close pop up windows and go back to idle
			_back_to_idle()

		if event.is_action_pressed("c"):
			current_ui_state = ui_state.CLOSE

		if event.is_action_pressed("left_click"):
			var mouse_pos = get_global_mouse_position()
			if !_get_current_level()._mouse_over_empty_tile(mouse_pos):
				var path = _get_current_level()._get_path(current_player.position, mouse_pos)
				if path != []:
					current_player.path = path
					turn_taken = true

		if event.is_action_pressed("middle_click"):
			if current_level == 0:
				var mouse_pos = get_global_mouse_position()
				print("----------------")
				print(format_tileinfo_string % 
				[0, _get_tile_w2m_position(mouse_pos, 0), _get_tile_name(_get_level(0)._get_tile_ID(mouse_pos)), _get_tile_weight(_get_level(0)._get_tile_ID(mouse_pos))])
			else:
				for i in range(current_level, -1, -1):
					var mouse_pos = _get_level(i).get_local_mouse_position()
					if _get_level(i)._get_tile_ID(mouse_pos) != -1:
						print("----------------")
						print(format_tileinfo_string % 
						[str(i), _get_tile_w2m_position(mouse_pos, i), _get_tile_name(_get_level(i)._get_tile_ID(mouse_pos)), _get_tile_weight(_get_level(i)._get_tile_ID(mouse_pos))])
						break

		if event.is_action_pressed("right_click"):
			_follow(friendly, current_player)

		if event is InputEventMouseMotion:
			pass

		if event.is_action("alt"):
			if !_get_current_level()._mouse_over_empty_tile(get_global_mouse_position()):
				_draw_movement_line(current_player, get_global_mouse_position())

		if event.is_action_released("alt"):
			_clear_movement_numbers()
			_clear_movement_line()


	_check_ui_state()

	if turn_taken == true:
		_play_turn()
