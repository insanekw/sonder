extends Panel


onready var skills = load("res://scenes/gui/character/Skills.tscn").instance()
onready var inventory_label = Label.new()

var unit_info : Unit
var mini : Sprite
var showing_extra_info = false

# Called when the node enters the scene tree for the first time.
func _ready():
	rect_size = Vector2(460, get_node("main_vbox").rect_size.y + 30)
	pass # Replace with function body.

func _set_unit(unit : Unit):
	unit_info = unit
	_fill_main()

func _dress_character(inventory : Array):
	pass

func _fill_main() -> void:
	$main_vbox/top/name_label.text = str(unit_info.first_name + " " + unit_info.last_name)
	#TODO: occupation	$main_vbox/top/occupation_label.text = 
	$main_vbox/top/portrait_stats/portrait.texture = load(s.FACE_SPRITE_DIR + (unit_info.face if unit_info.face != null else "0") + ".png")
	$main_vbox/top/portrait_stats/portrait/mini_sprite.texture = unit_info.get_node("body").texture
	for bodypart in unit_info.get_node("body/wearing").get_children():
		for piece in bodypart.get_children():
			var new_piece = Sprite.new()
			new_piece.flip_h = true
			new_piece.texture = piece.texture
			$main_vbox/top/portrait_stats/portrait/mini_sprite.add_child(new_piece)
	
	$main_vbox/top/portrait_stats/stats/str_value.text = str(unit_info.strength)
	$main_vbox/top/portrait_stats/stats/con_value.text = str(unit_info.constitution)
	$main_vbox/top/portrait_stats/stats/dex_value.text = str(unit_info.dexterity)
	$main_vbox/top/portrait_stats/stats/agi_value.text = str(unit_info.agility)
	$main_vbox/top/portrait_stats/stats/cha_value.text = str(unit_info.charisma)
	$main_vbox/top/portrait_stats/stats/wis_value.text = str(unit_info.wisdom)
	
	$main_vbox/top/portrait_stats/stats/ldr_value.text = str(unit_info.leadership)
	$main_vbox/top/portrait_stats/stats/mrk_value.text = str(unit_info.marksmanship)
	$main_vbox/top/portrait_stats/stats/med_value.text = str(unit_info.medicine)
	$main_vbox/top/portrait_stats/stats/mec_value.text = str(unit_info.mechanical)
	$main_vbox/top/portrait_stats/stats/elc_value.text = str(unit_info.electronics)
	$main_vbox/top/portrait_stats/stats/eng_value.text = str(unit_info.engineering)
	"""
	$main_vbox/top/perks_label.text = ""
	var perks_and_drawbacks = ""
	for p_or_d in unit_info._get_perks_and_drawbacks_as_string():
		perks_and_drawbacks += str(p_or_d) + ", "
	
	perks_and_drawbacks.erase(perks_and_drawbacks.length() - 2, 2)
	$main_vbox/top/perks_label.text = perks_and_drawbacks
	"""
	$main_vbox/top/occupation_label.text = ""
	$main_vbox/top/occupation_label.text += unit_info._get_occupation_strings_slashes()
	
	var wielding = "wielding: "
	if unit_info.wielding != []:
		for weapon in unit_info.wielding:
			wielding += weapon["NAME"] as String + ", "
		
		wielding.erase(wielding.length() - 2, 2)
	else:
		wielding += "(nothing)"
	$main_vbox/top/equipped_wearing.text = wielding + "\n"
	
	var clothing = "wearing: "
	if unit_info.wearing != []:
		for clothing_piece in unit_info.wearing:
			clothing += clothing_piece["NAME"] as String + ", "
		
		clothing.erase(clothing.length() - 2, 2)
	else:
		clothing += "(nothing)"
	$main_vbox/top/equipped_wearing.text += clothing


func _fill_skill() -> void:
	skills.get_node("health_value").text = round(unit_info._get_health()) as String + "/" + str(unit_info.current_health)
	skills.get_node("stamina_value").text = str(round(unit_info._get_stamina())) + "/" + str(unit_info.current_stamina)
	skills.get_node("carry_value").text = str(round(unit_info._get_carry())) + "kg" if g.metric else "lbs"
	skills.get_node("heal_value").text = str(round(unit_info._get_healing())) + "%"
	skills.get_node("move_value").text = str(round(unit_info._get_movement())) + "sq"
	skills.get_node("sneak_value").text = str(round(unit_info._get_sneak())) + "%"
	skills.get_node("talk_value").text = str(round(unit_info._get_talk())) + "%"
	skills.get_node("bluff_value").text = str(round(unit_info._get_bluff())) + "%"
	skills.get_node("locks_value").text = str(round(unit_info._get_lockpick())) + "%"
	skills.get_node("heal_value").text = str(round(unit_info._get_healing())) + "%"
	skills.get_node("mech_value").text = str(round(unit_info._get_mechanical())) + "%"
	skills.get_node("elec_value").text = str(round(unit_info._get_electronics())) + "%"
	skills.get_node("engi_value").text = str(round(unit_info._get_engineering())) + "%"
	skills.get_node("meds_value").text = str(round(unit_info._get_medicine())) + "%"
	skills.get_node("learn_value").text = str(round(unit_info._get_learn())) + "%"
	skills.get_node("teach_value").text = str(round(unit_info._get_teach())) + "%"
	skills.get_node("init_value").text = str(round(unit_info._get_initiative()))
	skills.get_node("block_value").text = str(round(unit_info._get_block())) + "%"
	skills.get_node("dodge_value").text = str(round(unit_info._get_dodge())) + "%"
	skills.get_node("ranged_ap_value").text = "+" + str(round(unit_info._get_ranged_ap()))
	skills.get_node("ranged_aim_value").text = str(round(unit_info._get_ranged_aim())) + "%"
	skills.get_node("ranged_dmg_value").text = "+" + str(round(unit_info._get_ranged_dmg()))
	skills.get_node("melee_ap_value").text = "+" + str(round(unit_info._get_melee_ap()))
	skills.get_node("melee_aim_value").text = str(round(unit_info._get_melee_aim())) + "%"
	skills.get_node("melee_dmg_value").text = "+" + str(round(unit_info._get_melee_dmg()))

func _fill_inventory():
	var inventory_text = ""
	for item in unit_info._get_inventory(false):
		if item != null:
			inventory_text += item["NAME"] + ", "
	
	inventory_text.erase(inventory_text.length() - 2, 2)
	inventory_label.text = inventory_text

func _input(event):
	if event.is_action_pressed("alt"):
		_fill_inventory()
		$main_vbox.add_child(inventory_label)
		rect_size += Vector2(0, inventory_label.rect_size.y)
	if event.is_action_released("alt"):
		$main_vbox.remove_child(inventory_label)
		rect_size -= Vector2(0,inventory_label.rect_size.y)
	if event.is_action_pressed("ctrl"):
		if skills.get_node("health_value").text == "":
			_fill_skill()
		$main_vbox.add_child(skills)
		rect_size += Vector2(0, 170)
	if event.is_action_released("ctrl"):
		$main_vbox.remove_child(skills)
		rect_size -= Vector2(0, 170)
