extends Node

onready var background_resource = load("res://scenes/Main_Menu_Background.tscn")

# DEBUG: Only use perks and drawbacks that have a stat boost associated with them
var boosted_perks_only = true

# SQL save/load character
const SQLite = preload("res://addons/godot-sqlite/bin/gdsqlite.gdns")
var database
# Make a table containing the variable types
var save_dict : Dictionary = Dictionary()
# Permanent save character directory -used to save characters for future playthroughs
const SAVE_DIR : = "res://assets/characters"
# Temp save character directory -used to store player's starting characters
const PARTY_DIR : = "res://assets/characters/char_gen"

# Used to save and iterate between current party members
var character
# Used to store int IDs of party members currently being made
var characters = []

#used to store all matching faces
var face_generator = preload("res://scenes/engine/generation/Faces.tscn").instance()
var faces = []
var current_face_index = 0
var gender = e.genders.OTHER_GENDER
var occupations = []
var perks_and_drawbacks = []
# Hair/skin colour etc
var appearance = {}

# Used to prompt user to double check user intended to push random button
var change_made = false

#use unrounded numbers to get a better sense of how skill points affect stats
var round_numbers = true

#points left to spend
var total_points

# Used for extra random generation
var additional = 0

var current_difficulty
#total skill points allocated according to difficulty
var easy = 600
var normal = 525
var hard = 450
var extreme = 350

# Used to adjust actual value of skills based on occupation/perk/drawback bonuses/penalties
var bonuses = []
var penalties = []

var occupation_points = 3
var current_occupation_points = 0
var perk_points = 1
var current_perk_points = 0

var total_perks = 15
var current_total_perks = 0

#bonuses from occupations/perks/drawbacks
var str_bonus : int
var con_bonus : int
var dex_bonus : int
var agi_bonus : int
var cha_bonus : int
var wis_bonus : int
var ldr_bonus : int
var mrk_bonus : int
var elc_bonus : int
var med_bonus : int
var eng_bonus : int
var mec_bonus : int

var health_bonus : int
var stamina_bonus : int
var carry_bonus : int
var healing_bonus : int
var movement_bonus : int
var sneak_bonus : int
var talk_bonus : int
var bluff_bonus : int
var lockpick_bonus : int
var explosives_bonus : int
var medicine_bonus : int
var mechanical_bonus : int
var electronics_bonus : int
var engineering_bonus : int
var learn_bonus : int
var teach_bonus : int
var initiative_bonus : int
var block_bonus : int
var dodge_bonus : int
var melee_aim_bonus : int
var melee_dmg_bonus : int
var melee_ap_bonus : int
var ranged_aim_bonus : int
var ranged_dmg_bonus : int
var ranged_ap_bonus : int

#first/last name generator
var names_resource = preload("res://scenes/engine/generation/NameGenerator.tscn").instance()

"""
TODO:? currently using a singleton for the below rather than invoking them penaltiestantly during gameplay, might change later if memory takes too hard of a shot
#used to fill the occupation/perks/drawbacks scrollbox with occupations and get info on them when hovered over
var occupations_resource = preload("res://scenes/engine/personality/Occupations.tscn").instance()
var p_and_d_resource = preload("res://scenes/engine/personality/Perks+Drawbacks.tscn").instance()
"""

#-unused?
signal back_pressed
signal mouse_over

func _save_party_member():
	pass

func _delete_party_member():
	pass

func _save_character():
	pass

func _load_character():
	pass

func _ready():
	# Set up SQL character save dictionary
	save_dict["NAME"] = {"data_type":"int", "primary_key": true, "not_null": true}
	save_dict["FACE"] = {"data_type":"text", "not_null": true}
	save_dict["COUNTRY"] = {"data_type":"text", "not_null": true}
	save_dict["GENDER"] = {"data_type":"int", "not_null": true}
	save_dict["BIRTH_DAY"] = {"data_type":"int", "not_null": true}
	save_dict["BIRTH_MONTH"] = {"data_type":"int", "not_null": true}
	save_dict["BIRTH_YEAR"] = {"data_type":"int", "not_null": true}
	save_dict["STR"] = {"data_type":"int", "not_null": true}
	save_dict["CON"] = {"data_type":"int", "not_null": true}
	save_dict["AGI"] = {"data_type":"int", "not_null": true}
	save_dict["DEX"] = {"data_type":"int", "not_null": true}
	save_dict["CHA"] = {"data_type":"int", "not_null": true}
	save_dict["WIS"] = {"data_type":"int", "not_null": true}
	save_dict["LDR"] = {"data_type":"int", "not_null": true}
	save_dict["MRK"] = {"data_type":"int", "not_null": true}
	save_dict["MED"] = {"data_type":"int", "not_null": true}
	save_dict["MEC"] = {"data_type":"int", "not_null": true}
	save_dict["ELC"] = {"data_type":"int", "not_null": true}
	save_dict["ENG"] = {"data_type":"int", "not_null": true}
	save_dict["OCCUPATIONS"] = {"data_type":"array", "not_null": true}
	save_dict["PERKS_AND_DRAWBACKS"] = {"data_type":"array", "not_null": true}
	
	database = SQLite.new()
	database.path = SAVE_DIR
	database.verbose_mode = true
	
	# Used to iterate between current party members
	character = Unit.new()
	
	g.date_time = OS.get_datetime()
	g.year = g.date_time.year + 1
	g.month = g.date_time.month
	g.day = g.date_time.day
	
	_fill_appearance_boxes()
	_get_faces()
	
	match g.game_difficulty:
		e.difficulty.EASY:
			current_difficulty = easy
			easy_button.disabled = true
		e.difficulty.NORMAL:
			current_difficulty = normal
			normal_button.disabled = true
		e.difficulty.HARD:
			current_difficulty = hard
			hard_button.disabled = true
		e.difficulty.EXTREME:
			current_difficulty = extreme
			extreme_button.disabled = true
		#default to normal
		_:
			current_difficulty = normal
			get_node("normal_button").disabled = true
	
	for country in names_resource._get_countries():
		country_choices.add_item(country)
	
	occupations_resource._sort()
	p_and_d_resource._sort()
	
	var i = 0
	for occupation in occupations_resource.occupations:
		
		var check_box = CheckBox.new()
		occupation_choices.add_child(check_box)
		check_box.add_color_override("font_color", Color("cfe104"))
		check_box.add_font_override("font", load("res://assets/fonts/command_prompt_16px.tres"))
	
		check_box.text = str(occupations_resource._get_name_by_id(occupation["ID"], gender))
		check_box.name = str(occupation["ID"])
		
		i += 1
	
	i = 0
	for perk in p_and_d_resource.perks_and_drawbacks:
		if !boosted_perks_only:
			if perk["COST"] > 0:
				var check_box = CheckBox.new()
				perk_choices.add_child(check_box)
				check_box.add_color_override("font_color", Color(0,1,0,1))
				check_box.add_font_override("font", load("res://assets/fonts/command_prompt_16px.tres"))
				
				check_box.text = str(perk["NAME"])
				check_box.name = str(perk["ID"])
		else:
			if perk["COST"] > 0:
				if perk.has("EFFECT"):
					var check_box = CheckBox.new()
					perk_choices.add_child(check_box)
					check_box.add_color_override("font_color", Color(0,1,0,1))
					check_box.add_font_override("font", load("res://assets/fonts/command_prompt_16px.tres"))
					
					check_box.text = str(perk["NAME"])
					check_box.name = str(perk["ID"])
		
		i += 1
	
	i = 0
	for drawback in p_and_d_resource.perks_and_drawbacks:
		if !boosted_perks_only:
			if drawback["COST"] < 0:
				var check_box = CheckBox.new()
				drawback_choices.add_child(check_box)
				check_box.add_color_override("font_color", Color(1,0,0,1))
				check_box.add_font_override("font", load("res://assets/fonts/command_prompt_16px.tres"))
				
				check_box.text = str(drawback["NAME"])
				check_box.name = str(drawback["ID"])
		else:
			if drawback["COST"] < 0:
				if drawback.has("EFFECT"):
					var check_box = CheckBox.new()
					drawback_choices.add_child(check_box)
					check_box.add_color_override("font_color", Color(1,0,0,1))
					check_box.add_font_override("font", load("res://assets/fonts/command_prompt_16px.tres"))
					
					check_box.text = str(drawback["NAME"])
					check_box.name = str(drawback["ID"])
		
		i += 1
	
	#connect labels to mouseover tooltips
	for occupation_checkbox in occupation_choices.get_children():
		occupation_checkbox.connect("mouse_entered", self, "_mouse_enter_occupation", [occupation_checkbox.name])
		occupation_checkbox.connect("mouse_exited", self, "_clear_help")
		occupation_checkbox.connect("pressed", self, "_toggle_occupation", [occupation_checkbox.name])
	
	#connect labels to mouseover tooltips
	for perk_checkbox in perk_choices.get_children():
		perk_checkbox.connect("mouse_entered", self, "_mouse_enter_perk_or_drawback", [perk_checkbox.name])
		perk_checkbox.connect("mouse_exited", self, "_clear_help")
		perk_checkbox.connect("pressed", self, "_toggle_perk_or_drawback", [perk_checkbox.name])
	
	#connect labels to mouseover tooltips
	for drawback_checkbox in drawback_choices.get_children():
		drawback_checkbox.connect("mouse_entered", self, "_mouse_enter_perk_or_drawback", [drawback_checkbox.name])
		drawback_checkbox.connect("mouse_exited", self, "_clear_help")
		drawback_checkbox.connect("pressed", self, "_toggle_perk_or_drawback", [drawback_checkbox.name])
	
	#the rest of the tooltips
	for item in tooltips:
		item.connect("mouse_entered", self, "_mouse_enter", [item.name])
		item.connect("mouse_exited", self, "_clear_help")
	
	_update_total()
	
	##DEBUG
	#_auto_roll_char()



func _on_start_button_pressed():
	pass
	"""
	if current_perk_points >= 0 and current_occupation_points >= 0:
	#give char a name if one isn't set
	if name_edit.text == "":
		name_edit.text = names_resource._generate_first_name(
		e.genders.MALE if !male_button.flat else
		e.genders.FEMALE if !female_button.flat else
		e.genders.OTHER_GENDER
		) + " " + names_resource._generate_last_name()
	#give birthdate if one isn't set
	if (!year_box.text.is_valid_integer() or !month_box.text.is_valid_integer() or !day_box.text.is_valid_integer() or year_box.text.length() < 4):
		_generate_date_of_birth()
	#disallow player from starting game if max points are exceeded, solved atm by disabling start_button
	elif total_points < 0:
		#TODO:
		$bottom_left/error_label.text = "Too many points assigned! Free up some points or penaltiesider changing difficulties."
	else:
		#start booting game
		get_node("loading_label").visible = true
		
		var t = Timer.new()
		add_child(t)
		t.set_wait_time(0.02)
		t.start()
		yield(t,"timeout")
		
		#set difficulty
		if easy_button.disabled:
			g.game_difficulty = e.difficulties.EASY
		elif normal_button.disabled:
			g.game_difficulty = e.difficulties.NORMAL
		elif hard_button.disabled:
			g.game_difficulty = e.difficulties.HARD
		elif extreme_button.disabled:
			g.game_difficulty = e.difficulties.EXTREME
		
		#TODO: add first/last/nick names_resource to player
		_get_player_name()
		
		#set player stats according to sliders
		g.player.strength = str_slider.value
		g.player.constitution = con_slider.value
		g.player.dexterity = dex_slider.value
		g.player.agility = agi_slider.value
		g.player.charisma = cha_slider.value
		g.player.wisdom = wis_slider.value
		g.player.leadership = ldr_slider.value
		g.player.mechanical = mec_slider.value
		g.player.engineering = eng_slider.value
		g.player.medicine = med_slider.value
		g.player.electronics = elc_slider.value
		g.player.marksmanship = mrk_slider.value
		
		g.player.max_health = round(g._get_health(g.player.constitution, g.player.strength))
		g.player.health = g.player.max_health
		g.player.max_stamina = round(g._get_stamina(g.player.constitution, g.player.agility))
		g.player.stamina = g.player.max_stamina
		#TODO: come up with a different way to work out metabolism? using wisdom for hunger/thirst willpower
		g.player.metabolism = round(g._get_healing_rate(g.player.constitution, g.player.wisdom))
		g.player.hunger = g.player.metabolism
		g.player.thirst = g.player.metabolism
		
		#set nationality
		g.player.place_of_birth = country_choices.selected
		
		#TODO: set gender
		
		
		g.player.face = faces[current_face_index]
		
		g.player.skin.mesh = load("res://assets/unit/human/mesh/Non-rigged/advancedCharacter.obj").duplicate()
		
		for surface in g.player.skin.mesh.get_surface_count():
			g.player.skin.mesh.surface_set_material(surface, load("res://assets/unit/human/material/male_player.tres") if g.player.gender == e.MALE else load("res://assets/unit/human/material/female_player.tres"))
		
		g.player.translation = Vector3(-2,0,-2)
		g.player.scale = Vector3(0.1,0.1,0.1)
		
		g.party.append(g.player)
		
		g.currently_selected_players.append(g.player)
		"""

func _update_total():
	_add_bonuses()
	
	character.strength = str_slider.value 
	character.constitution = con_slider.value
	character.dexterity = dex_slider.value
	character.agility = agi_slider.value
	character.charisma = cha_slider.value
	character.wisdom = wis_slider.value
	character.leadership = ldr_slider.value
	character.marksmanship = mrk_slider.value
	character.electronics = elc_slider.value
	character.medicine = med_slider.value
	character.engineering = eng_slider.value
	character.mechanical = mec_slider.value
	
	str_value.text = _get_stat_value(character.strength + str_bonus)
	con_value.text = _get_stat_value(character.constitution + con_bonus)
	dex_value.text = _get_stat_value(character.dexterity + dex_bonus)
	agi_value.text = _get_stat_value(character.agility + agi_bonus)
	cha_value.text = _get_stat_value(character.charisma + cha_bonus)
	wis_value.text = _get_stat_value(character.wisdom + wis_bonus)
	ldr_value.text = _get_stat_value(character.leadership + ldr_bonus)
	mrk_value.text = _get_stat_value(character.marksmanship + mrk_bonus)
	elc_value.text = _get_stat_value(character.electronics + elc_bonus)
	med_value.text = _get_stat_value(character.medicine + med_bonus)
	eng_value.text = _get_stat_value(character.engineering + eng_bonus)
	mec_value.text = _get_stat_value(character.mechanical + mec_bonus)
	
	
	total_points = (
	current_difficulty -
	str_slider.value -
	ldr_slider.value -
	con_slider.value -
	mrk_slider.value -
	dex_slider.value -
	elc_slider.value -
	agi_slider.value -
	med_slider.value -
	wis_slider.value -
	eng_slider.value -
	cha_slider.value -
	mec_slider.value
	)
	total_value.text = str(total_points)
	
	_update_skills()
	
	_set_bonuses_and_penalties()
	
	_check_occupation_points()
	occupation_description.text = str(_get_occupation_description())
	_check_perk_points()
	skill_description.text = str(_get_bonuses_and_penalties())
	
	_set_colours()
	
	_check_for_too_many_points()

func _update_skills():
	if (round_numbers):
		#health
		health.text = str(round(health_bonus + character._get_health()))
		#stamina
		stamina.text = str(round(stamina_bonus + character._get_stamina()))
		#carry weight
		carry_weight.text = str(round(carry_bonus + character._get_carry())) + "kg"
		#movement speed
		movement.text = str(round(movement_bonus + character._get_movement())) + "sq"
		#sneak
		sneak.text = str(round(sneak_bonus + character._get_sneak())) + "%"
		#healing rate
		healing.text = str(round(healing_bonus + character._get_healing()))
		#talk
		talk.text = str(round(talk_bonus + character._get_talk())) + "%"
		#initiative
		initiative.text = str(round(initiative_bonus + character._get_initiative()))
		#melee aim
		melee_aim.text = str(round(melee_aim_bonus + character._get_melee_aim())) + "%"
		#melee dmg
		melee_dmg.text = "+" + str(round(melee_dmg_bonus + character._get_melee_dmg()))
		#melee AP
		melee_ap.text = "+" + str(round(melee_ap_bonus + character._get_melee_ap()))
		#ranged aim
		ranged_aim.text = str(round(ranged_aim_bonus + character._get_ranged_aim())) + "%"
		#ranged dmg
		ranged_dmg.text = "+" + str(round(ranged_dmg_bonus + character._get_ranged_dmg()))
		#ranged AP
		ranged_ap.text = "+" + str(round(ranged_ap_bonus + character._get_ranged_ap()))
		#explosives
		explosives.text = str(round(explosives_bonus + character._get_explosives())) + "%"
		#lockpick
		lockpick.text = str(round(lockpick_bonus + character._get_lockpick())) + "%"
		#bluff
		bluff.text = str(round(bluff_bonus + character._get_bluff())) + "%"
		#medicine
		medicine.text = str(round(medicine_bonus + character._get_medicine())) + "%"
		#mechanical
		mechanical.text = str(round(mechanical_bonus + character._get_mechanical())) + "%"
		#electronics
		electronics.text = str(round(electronics_bonus + character._get_electronics())) + "%"
		#engineering
		engineering.text = str(round(engineering_bonus + character._get_engineering())) + "%"
		#dodge
		dodge.text = str(round(dodge_bonus + character._get_dodge())) + "%"
		#block
		block.text = str(round(block_bonus + character._get_block())) + "%"
		#learn
		learn.text = str(round(learn_bonus + character._get_learn())) + "%"
		#teach
		teach.text = str(round(teach_bonus + character._get_teach())) + "%"
	else:
		#health
		health.text = str(health_bonus + character._get_health())
		#stamina
		stamina.text = str(stamina_bonus + character._get_stamina())
		#carry weight
		carry_weight.text = str(carry_bonus + character._get_carry_weight()) + "kg"
		#movement speed
		movement.text = str(movement_bonus + character._get_movement()) + "sq"
		#sneak
		sneak.text = str(sneak_bonus + character._get_sneak()) + "%"
		#healing rate
		healing.text = str(healing_bonus + character._get_healing_rate())
		#talk
		talk.text = str(talk_bonus + character._get_talk()) + "%"
		#initiative
		initiative.text = str(initiative_bonus + character._get_initiative())
		#melee aim
		melee_aim.text = str(melee_aim_bonus + character._get_melee_aim()) + "%"
		#melee dmg
		melee_dmg.text = "+" + str(melee_dmg_bonus + character._get_melee_dmg())
		#melee AP
		melee_ap.text = "+" + str(melee_ap_bonus + character._get_melee_ap())
		#ranged aim
		ranged_aim.text = str(ranged_aim_bonus + character._get_ranged_aim()) + "%"
		#ranged dmg
		ranged_dmg.text = "+" + str(ranged_dmg_bonus + character._get_ranged_dmg())
		#ranged AP
		ranged_ap.text = "+" + str(ranged_ap_bonus + character._get_ranged_ap())
		#explosives
		explosives.text = str(explosives_bonus + character._get_explosives()) + "%"
		#lockpick
		lockpick.text = str(lockpick_bonus + character._get_lockpick()) + "%"
		#bluff
		bluff.text = str(bluff_bonus + character._get_bluff()) + "%"
		#medicine
		medicine.text = str(medicine_bonus + character._get_medicine()) + "%"
		#mechanical
		mechanical.text = str(mechanical_bonus + character._get_mechanical()) + "%"
		#electronics
		electronics.text = str(electronics_bonus + character._get_electronics()) + "%"
		#engineering
		engineering.text = str(engineering_bonus + character._get_engineering()) + "%"
		#dodge
		dodge.text = str(dodge_bonus + character._get_dodge()) + "%"
		#block
		block.text = str(block_bonus + character._get_block()) + "%"
		#learn
		learn.text = str(learn_bonus + character._get_learn()) + "%"
		#teach
		teach.text = str(teach_bonus + character._get_teach()) + "%"

func _toggle_occupation(id):
	var occupation = occupations_resource._get_occupation(int(id))
	if occupations.has(occupation):
		occupations.erase(occupation)
	else:
		occupations.append(occupation)
	
	_update_total()

func _toggle_perk_or_drawback(id):
	var p_or_d = p_and_d_resource._get_perk_or_drawback(int(id))
	if perks_and_drawbacks.has(p_or_d):
		perks_and_drawbacks.erase(p_or_d)
	else:
		perks_and_drawbacks.append(p_or_d)
	
	_update_total()

func _check_occupation_points():
	current_occupation_points = 0
	for occupation in occupations:
		current_occupation_points += occupations_resource._get_occupation_score(occupation)
	
	_lock_occupations()
	
	#occupation (# occupation point/s remaining)
	occupation_points_label.text = "occupation (" + str(occupation_points - current_occupation_points) + " occupation point" + ("s" if (current_occupation_points - occupation_points) != 1 and (current_occupation_points - occupation_points) != -1 else "") + " left)"

func _lock_occupations():
	if current_occupation_points >= occupation_points:
		for occupation in occupation_choices.get_children():
			if occupations.has(occupations_resource._get_occupation(int(occupation.name))):
				occupation.disabled = false
				occupation.pressed = true
			else:
				occupation.disabled = true
	else:
		for occupation in occupation_choices.get_children():
			if occupations_resource._get_occupation_score(occupations_resource._get_occupation(int(occupation.name))) <= (occupation_points - current_occupation_points):
				occupation.disabled = false
			else:
				if occupations.has(occupations_resource._get_occupation(int(occupation.name))):
					occupation.disabled = false
					occupation.pressed = true
				else:
					occupation.disabled = true

func _check_perk_points():
	current_total_perks = 0
	for p_or_d in perks_and_drawbacks:
		current_total_perks += 1
	
	current_perk_points = 0
	var plus = 0
	var minus = 0
	for p_or_d in perks_and_drawbacks:
		if p_or_d["COST"] > 0:
			plus += p_or_d["COST"]
		else:
			minus -= p_or_d["COST"]
	current_perk_points = (plus - minus)
	
	_lock_perks_and_drawbacks()
	
	#Perks/Drawbacks (# perk point/s remaining)
	perk_points_label.text = "Perks/Drawbacks (" + str(perk_points - current_perk_points) + " perk point" + ("s" if (current_perk_points - perk_points) != 1 and (current_perk_points - perk_points) != -1 else "") + " left)"
	#0/15 perks selected
	total_perks_label.text = str(current_total_perks) + "/" + str(total_perks)

func _lock_perks_and_drawbacks():
	print(perk_points - current_perk_points)
	if current_total_perks >= total_perks:
		for perk_checkbox in perk_choices.get_children():
			if perks_and_drawbacks.has(p_and_d_resource._get_perk_or_drawback(int(perk_checkbox.name))):
				perk_checkbox.disabled = false
				perk_checkbox.pressed = true
			else:
				perk_checkbox.disabled = true
		for drawback_checkbox in drawback_choices.get_children():
			if perks_and_drawbacks.has(p_and_d_resource._get_perk_or_drawback(int(drawback_checkbox.name))):
				drawback_checkbox.disabled = false
				drawback_checkbox.pressed = true
			else:
				drawback_checkbox.disabled = true
	elif current_perk_points >= perk_points:
		for perk_checkbox in perk_choices.get_children():
			if perks_and_drawbacks.has(p_and_d_resource._get_perk_or_drawback(int(perk_checkbox.name))):
				perk_checkbox.disabled = false
				perk_checkbox.pressed = true
			else:
				perk_checkbox.disabled = true
		for drawback_checkbox in drawback_choices.get_children():
			if perks_and_drawbacks.has(p_and_d_resource._get_perk_or_drawback(int(drawback_checkbox.name))):
				drawback_checkbox.disabled = false
				drawback_checkbox.pressed = true
			else:
				drawback_checkbox.disabled = false
	else:
		for perk_checkbox in perk_choices.get_children():
			var perk = p_and_d_resource._get_perk_or_drawback(int(perk_checkbox.name))
			if perks_and_drawbacks.has(p_and_d_resource._get_perk_or_drawback(int(perk_checkbox.name))):
				perk_checkbox.disabled = false
				perk_checkbox.pressed = true
			else:
				if perk["COST"] <= (perk_points - current_perk_points):
					perk_checkbox.disabled = false
				else:
					perk_checkbox.disabled = true
		for drawback_checkbox in drawback_choices.get_children():
			if perks_and_drawbacks.has(p_and_d_resource._get_perk_or_drawback(int(drawback_checkbox.name))):
				drawback_checkbox.disabled = false
				drawback_checkbox.pressed = true
			else:
				drawback_checkbox.disabled = false
	"""
	for p_or_d in perks_and_drawbacks:
		if p_or_d.has("OPPOSITE"):
			if p_or_d["COST"] > 0:
				for opposite in p_or_d["OPPOSITE"]:
					for drawback_checkbox in drawback_choices.get_children():
						if opposite == int(drawback_checkbox.name):
							drawback_checkbox.disabled = true
			else:
				for opposite in p_or_d["OPPOSITE"]:
					for perk_checkbox in perk_choices.get_children():
						if opposite == int(perk_checkbox.name):
							perk_checkbox.disabled = true
							"""

func _add_bonuses():
	_clear_bonuses()
	for occupation in occupations:
		if occupation.has("BONUS"):
			var i = 0
			while i < occupation["BONUS"].size():
				_add_bonus(occupation["BONUS"][i], occupation["BONUS"][i + 1])
				i += 2
	
	for p_or_d in perks_and_drawbacks:
		if p_or_d.has("EFFECT"):
			var i = 0
			while i < p_or_d["EFFECT"].size():
				_add_bonus(p_or_d["EFFECT"][i], p_or_d["EFFECT"][i + 1])
				i += 2
	#if str_bonus > 0:
		#make green and flash
	#elif str_bonus < 0:
		#make red and flash
	#else:
		#make correct color and stop flashing

func _add_bonus(stat, number):
	match stat:
		e.STR:
			str_bonus += number
		e.CON:
			con_bonus += number
		e.DEX:
			dex_bonus += number
		e.AGI:
			agi_bonus += number
		e.CHA:
			cha_bonus += number
		e.WIS:
			wis_bonus += number
		e.LDR:
			ldr_bonus += number
		e.MRK:
			mrk_bonus += number
		e.MED:
			med_bonus += number
		e.ELC:
			elc_bonus += number
		e.MEC:
			mec_bonus += number
		e.ENG:
			eng_bonus += number
		e.HEALTH_STAT:
			health_bonus += number
		e.STAMINA_STAT:
			stamina_bonus += number
		e.CARRY_STAT:
			carry_bonus += number
		e.HEALING_STAT:
			healing_bonus += number
		e.MOVEMENT_STAT:
			movement_bonus += number
		e.SNEAK_STAT:
			sneak_bonus += number
		e.TALK_STAT:
			talk_bonus += number
		e.BLUFF_STAT:
			bluff_bonus += number
		e.LOCKPICK_STAT:
			lockpick_bonus += number
		e.EXPLOSIVES_STAT:
			explosives_bonus += number
		e.MEDICINE_STAT:
			medicine_bonus += number
		e.MECHANICAL_STAT:
			mechanical_bonus += number
		e.ELECTRONICS_STAT:
			electronics_bonus += number
		e.ENGINEERING_STAT:
			engineering_bonus += number
		e.LEARN_STAT:
			learn_bonus += number
		e.TEACH_STAT:
			teach_bonus += number
		e.INITIATIVE_STAT:
			initiative_bonus += number
		e.BLOCK_STAT:
			block_bonus += number
		e.DODGE_STAT:
			dodge_bonus += number
		e.MELEE_AIM_STAT:
			melee_aim_bonus += number
		e.MELEE_DMG_STAT:
			melee_dmg_bonus += number
		e.MELEE_AP_STAT:
			melee_ap_bonus += number
		e.RANGED_AIM_STAT:
			ranged_aim_bonus += number
		e.RANGED_DMG_STAT:
			ranged_dmg_bonus += number
		e.RANGED_AP_STAT:
			ranged_ap_bonus += number
		_:
			return null

func _clear_bonuses():
	str_bonus = 0 
	con_bonus = 0
	dex_bonus = 0
	agi_bonus = 0
	cha_bonus = 0
	wis_bonus = 0
	ldr_bonus = 0
	mrk_bonus = 0
	elc_bonus = 0
	med_bonus = 0
	eng_bonus = 0
	mec_bonus = 0
	
	health_bonus = 0
	stamina_bonus = 0
	carry_bonus = 0
	healing_bonus = 0
	movement_bonus = 0
	sneak_bonus = 0
	talk_bonus = 0
	bluff_bonus = 0
	lockpick_bonus = 0
	explosives_bonus = 0
	medicine_bonus = 0
	mechanical_bonus = 0
	electronics_bonus = 0
	engineering_bonus = 0
	learn_bonus = 0
	teach_bonus = 0
	initiative_bonus = 0
	block_bonus = 0
	dodge_bonus = 0
	melee_aim_bonus = 0
	melee_dmg_bonus = 0
	melee_ap_bonus = 0
	ranged_aim_bonus = 0
	ranged_dmg_bonus = 0
	ranged_ap_bonus = 0

func _quit():
	queue_free()

func _on_randomise_button_pressed():
	loading_label.visible = true
	# buffer to make sure load label has time to appear
	var t = Timer.new()
	add_child(t)
	t.set_wait_time(0.035)
	t.start()
	
	yield(t,"timeout")
	if !change_made:
		_roll_random_character()
	else:
		#TODO:
		#print("Are you sure you want to make changes?")
		#yes
		_roll_random_character()
		change_made = false
		#no
		pass
	loading_label.visible = false

func _roll_random_character():
	#newstuff!
	_clear_selection_boxes()
	occupations = occupations_resource._get_random_occupations(occupation_points)
	perks_and_drawbacks = p_and_d_resource._get_random_perks_and_drawbacks(perk_points)
	
	g._sort_array_alphabetically(perks_and_drawbacks)
	_mouse_enter_start_button()
	
	#gender
	var roll = g._get_random_1_100()
	if roll < 50:
		male_button.flat = false
		female_button.flat = true
		male_button.add_color_override("font_color", Color("216dff"))
		female_button.add_color_override("font_color", Color("ffffff"))
		facial_hair_choices.disabled = false
	elif roll <= 100:
		male_button.flat = true
		female_button.flat = false
		female_button.add_color_override("font_color", Color("b65fff"))
		male_button.add_color_override("font_color", Color("ffffff"))
		facial_hair_choices.disabled = true
		facial_hair_choices.select(0)
	else:
		male_button.flat = true
		female_button.flat = true
	
	#name
	name_edit.text = names_resource._generate_first_name(
	e.genders.MALE if !male_button.flat else
	e.genders.FEMALE if !female_button.flat else
	e.genders.OTHER_GENDER
	) + " " + names_resource._generate_last_name()
	
	#date of birth
	_generate_date_of_birth()
	
	#face
	_get_faces()
	if !faces.empty():
		randomize()
		current_face_index = randi()%faces.size() 
	_display_face(faces[current_face_index] if !faces.empty() else "0")
	
	#stats
	_clear_sliders()
	total_points = current_difficulty
	while total_points > 0:
		_place_stat_points()
	
	#nationality
	randomize()
	country_choices.selected = randi()%country_choices.get_item_count()

func _get_faces():
	faces.clear()
	_get_appearance()
	faces = face_generator._get_faces(e.genders.MALE if !male_button.flat else e.genders.FEMALE if !female_button.flat else e.genders.OTHER_GENDER, appearance)
	_randomise_faces()

func _randomise_faces():
	var shuffledList = [] 
	var indexList = range(faces.size())
	for i in range(faces.size()):
		randomize()
		var x = randi()%indexList.size()
		shuffledList.append(faces[indexList[x]])
		indexList.remove(x)
	faces = shuffledList

func _display_face(index):
	face.texture = load(g.FACE_SPRITE_DIR + (faces[current_face_index] if faces.size() > 1 else "0") + ".png")

func _on_left_button_pressed():
	current_face_index -= 1
	if current_face_index < 0:
		current_face_index = faces.size() - 1
	_display_face(faces[current_face_index] if !faces.empty() else "0")

func _on_right_button_pressed():
	current_face_index += 1
	if current_face_index > faces.size() - 1:
		current_face_index = 0
	_display_face(faces[current_face_index] if !faces.empty() else "0")

func _get_appearance():
	appearance.clear()
	
	if skin_choices.text != "Skin":
		appearance["SKIN"] = skin_choices.text
	if hair_choices.text != "Hair":
		appearance["HAIR_COLOUR"] = hair_choices.text
	if facial_hair_choices.text != "Facial hair":
		appearance["FACIAL_HAIR"] = facial_hair_choices.text
	if int(age_value.text) > g.LIFE_EXPECTANCY:
		appearance["OLD"] = true
	"""
	if accessory_button.text != "Accessory":
		appearance["sdgfg"] = accessory_button.text
	"""

func _display_current_face():
	if current_face_index == -1 or faces.empty():
		_display_face("0")
	else:
		_display_face(current_face_index)

func _fill_appearance_boxes():
	skin_choices.add_item("Skin")
	for value in face_generator._get_appearance_values("SKIN"):
		skin_choices.add_item(value)
	
	hair_choices.add_item("Hair")
	for value in face_generator._get_appearance_values("HAIR_COLOUR"):
		hair_choices.add_item(value)
	
	facial_hair_choices.add_item("Facial hair")
	for value in face_generator._get_appearance_values("FACIAL_HAIR"):
		facial_hair_choices.add_item(value)
	
	accessory_choices.add_item("Accessory")
	for value in face_generator._get_appearance_values("FRECKLES"):
		accessory_choices.add_item("Freckles")
	for value in face_generator._get_appearance_values("GLASSES"):
		accessory_choices.add_item("Glasses")
		"""
	for value in face_generator._get_appearance_values("OLD"):
		accessory_choices.add_item(??? i don't think i meant to do this)
		"""

func _check_for_too_many_points():
	if total_points < 0 or current_occupation_points < 0 or current_perk_points < 0:
		start_button.disabled = true
		total_value.add_color_override("font_color", Color(1,0,0,1))
	else:
		start_button.disabled = false
		total_value.add_color_override("font_color", Color(1,1,1,1))

func _slider_changer(slider, label, holder, bonus, value):
	change_made = true
	
	if total_points <= 0:
		if value < holder:
			slider.value = value
		else:
			slider.value = holder
	elif value - holder >= total_points:
		slider.value = holder + total_points
		total_points = 0
	else:
		slider.value = value
	
	label.text = (
	str(int(bonus + slider.value)) if (bonus + slider.value) >= 10
	else "0" + str(int(bonus + slider.value))
	)

func _get_stat_value(number):
	if number > 100:
		return "100"
	elif number < 0:
		return "0"
	else:
		return str(number)

func _change_value(slider):
	if slider.value < 99:
		if (slider.value + 1 + additional) > 99:
			additional -= (slider.value + 1 + additional) - 100
			slider.value = 99
		else:
			slider.value += 1 + additional
			additional = 0

func _stat_colour_changer(slider, label):
	if slider >= g.UPPER_THRESHOLD:
		label.add_color_override("font_color", Color(0,1,0,1))
	elif slider <= g.LOWER_THRESHOLD:
		label.add_color_override("font_color", Color(1,0,0,1))
	else:
		label.add_color_override("font_color", Color(1,1,1,1))

func _set_colours():
	_stat_colour_changer(str_slider.value + str_bonus, str_value)
	_stat_colour_changer(con_slider.value + con_bonus, con_value)
	_stat_colour_changer(dex_slider.value + dex_bonus, dex_value)
	_stat_colour_changer(agi_slider.value + agi_bonus, agi_value)
	_stat_colour_changer(cha_slider.value + cha_bonus, cha_value)
	_stat_colour_changer(wis_slider.value + wis_bonus, wis_value)
	_stat_colour_changer(ldr_slider.value + ldr_bonus, ldr_value)
	_stat_colour_changer(mrk_slider.value + mrk_bonus, mrk_value)
	_stat_colour_changer(mec_slider.value + mec_bonus, mec_value)
	_stat_colour_changer(med_slider.value + med_bonus, med_value)
	_stat_colour_changer(elc_slider.value + elc_bonus, elc_value)
	_stat_colour_changer(eng_slider.value + eng_bonus, eng_value)

func _clear_sliders():
	str_slider.value = 1
	con_slider.value = 1
	dex_slider.value = 1
	agi_slider.value = 1
	cha_slider.value = 1
	wis_slider.value = 1
	
	ldr_slider.value = 0
	mrk_slider.value = 0
	mec_slider.value = 0
	eng_slider.value = 0
	med_slider.value = 0
	elc_slider.value = 0

func _place_stat_points():
	var roll = g._get_random_1_100()
	match roll:
		1, 2:
			_change_value(str_slider)
		3, 4:
			_change_value(con_slider)
		5, 6:
			_change_value(dex_slider)
		7, 8:
			_change_value(agi_slider)
		9, 10:
			_change_value(cha_slider)
		11, 12:
			_change_value(wis_slider)
		13:
			_change_value(ldr_slider)
		14:
			_change_value(mrk_slider)
		15:
			_change_value(med_slider)
		16:
			_change_value(mec_slider)
		17:
			_change_value(elc_slider)
		18:
			_change_value(eng_slider)
		#else add 1 to additional if total points allow
		_:
			if total_points > 1 + additional:
				additional += 1
			
	#more randomness (1% chance to 1/2 points in stat)
	roll = g._get_random_1_100()
	match roll:
		1:
			if str_slider.value > 2:
				str_slider.value / 2
		2:
			if dex_slider.value > 2:
				dex_slider.value / 2
		3:
			if con_slider.value > 2:
				con_slider.value / 2
		4:
			if agi_slider.value > 2:
				agi_slider.value / 2
		5:
			if cha_slider.value > 2:
				cha_slider.value / 2
		6:
			if wis_slider.value > 2:
				wis_slider.value / 2
		7:
			if ldr_slider.value > 2:
				ldr_slider.value / 2
		8:
			if mrk_slider.value > 2:
				mrk_slider.value / 2
		9:
			if med_slider.value > 2:
				med_slider.value / 2
		10:
			if elc_slider.value > 2:
				elc_slider.value / 2
		11:
			if mec_slider.value > 2:
				mec_slider.value / 2
		12:
			if eng_slider.value > 2:
				eng_slider.value / 2
		_:
			pass

func _change_age():
	var age
	if year_box.text.is_valid_integer() and month_box.text.is_valid_integer() and day_box.text.is_valid_integer():
		if year_box.text.length() == 4:
			age = (
			g.year -int(year_box.text) - 1 if g.month < int(month_box.text) else
			g.year - int(year_box.text) if g.month > int(month_box.text) else
			#by day
			g.year - int(year_box.text) - 1 if g.day < int(day_box.text) else
			g.year - int(year_box.text)
			)
			age_value.text = "DOB\nAge\n" + str(int(age))
		else:
			age_value.text = "?"
			
	else:
		age_value.text = "?"


func _generate_date_of_birth():
	var old_roll = g._get_random_1_100()
	var difference
	if old_roll > 10:
		difference = float(g.LIFE_EXPECTANCY - g.YOUNGEST_AGE) / 100
	else:
		difference = float(g.MAX_POSSIBLE_AGE - (g.LIFE_EXPECTANCY * .9)) / 100
	var age = int(difference * g._get_random_1_100()) + g.YOUNGEST_AGE
	var year = g.year - age
	
	year_box.text = str(year)
	
	var month = 0
	while month == 0:
		month = int(g._get_random_1_100() * 0.12)
	month_box.text = str(month)
	
	var day = 0
	while day == 0:
		match month:
			9, 4, 6, 11:
				day = int(g._get_random_1_100() * .30)
			1, 3, 5, 7, 8, 10, 12:
				day = int(g._get_random_1_100() * .31)
			2:
				if g.LEAP_YEAR:
					day = int(g._get_random_1_100() * .29)
				else:
					day = int(g._get_random_1_100() * .28)
	day_box.text = str(day)
	
	_change_age()

func _get_first_and_last_name_only():
	if !name_edit.text.find(" ") == -1:
		var space_index = name_edit.text.find(" ")
		var first_name = name_edit.text.left(space_index)
		var last_name = name_edit.text.right(space_index)
		
		g.player.first_name = first_name
		g.player.last_name = last_name
	else:
		g.player.first_name = name_edit.text

func _get_player_name():
	if name_edit.text.find("'") >= 0:
		if name_edit.text.find_last("'") != name_edit.text.find("'"):
			var nick_name_length = name_edit.text.find_last("'") - name_edit.text.find("'")
			g.player.nick_name = name_edit.text.substr(name_edit.text.find("'") + 1, nick_name_length - 1)
			g.player.first_name = name_edit.text.left(name_edit.text.find("'"))
			g.player.last_name = name_edit.text.right(name_edit.text.find_last("'"))
			print(g.player.first_name)
			print(g.player.nick_name)
			print(g.player.last_name)
		else:
			_get_first_and_last_name_only()
	elif name_edit.text.find('"') >= 0:
		if name_edit.text.find_last('"') != name_edit.text.find('"'):
			var nick_name_end = name_edit.text.find_last('"')
			#TODO: cut nick name out
			
		else:
			_get_first_and_last_name_only()
	else:
		_get_first_and_last_name_only()

func _free_up_difficulty_buttons():
	easy_button.disabled = false
	normal_button.disabled = false
	hard_button.disabled = false
	extreme_button.disabled = false

#clears occupation/perk/drawback boxes for resets/random
func _clear_selection_boxes():
	for occupation in occupation_choices.get_children():
		occupation.pressed = false
	
	for perk in perk_choices.get_children():
		perk.pressed = false
	
	for drawback in drawback_choices.get_children():
		drawback.pressed = false

func _on_easy_button_pressed():
	_free_up_difficulty_buttons()
	easy_button.disabled = true
	current_difficulty = easy
	_update_total()

func _on_normal_button_pressed():
	_free_up_difficulty_buttons()
	normal_button.disabled = true
	current_difficulty = normal
	_update_total()

func _on_hard_button_pressed():
	_free_up_difficulty_buttons()
	hard_button.disabled = true
	current_difficulty = hard
	_update_total()

func _on_extreme_button_pressed():
	_free_up_difficulty_buttons()
	extreme_button.disabled = true
	current_difficulty = extreme
	_update_total()

func _select_male_button():
	#DESELECT FEMALE/SELECT MALE/ENABLE FACIAL HAIR
	if !female_button.flat:
		female_button.flat = true
		male_button.flat = false
		facial_hair_choices.disabled = false
		male_button.add_color_override("font_color", Color("216dff"))
		female_button.add_color_override("font_color", Color("ffffff"))
	#DESELECT IF SELECTED
	elif !male_button.flat:
		male_button.flat = true
	#SELECT MALE
	else:
		male_button.flat = false
		facial_hair_choices.disabled = false
		male_button.add_color_override("font_color", Color("216dff"))
		female_button.add_color_override("font_color", Color("ffffff"))
	
	_get_faces()
	if !faces.empty():
		randomize()
		current_face_index = randi()%faces.size()
	else:
		current_face_index = 0
	
	_display_current_face()

func _select_female_button():
	#DESELECT MALE/SELECT FEMALE/DISABLE FACIAL HAIR
	if !male_button.flat:
		male_button.flat = true
		female_button.flat = false
		facial_hair_choices.disabled = true
		female_button.add_color_override("font_color", Color("b65fff"))
		male_button.add_color_override("font_color", Color("ffffff"))
		facial_hair_choices.select(0)
	#DESELECT FEMALE IF SELECTED
	elif !female_button.flat:
		female_button.flat = true
		facial_hair_choices.disabled = false
	#SELECT FEMALE
	else:
		female_button.flat = false
		facial_hair_choices.disabled = true
		female_button.add_color_override("font_color", Color("b65fff"))
		male_button.add_color_override("font_color", Color("ffffff"))
	
	_get_faces()
	if !faces.empty():
		randomize()
		current_face_index = randi()%faces.size()
	else:
		current_face_index = 0
	_display_current_face()

func _on_male_button_pressed():
	gender = e.genders.MALE
	_select_male_button()

func _on_female_button_pressed():
	gender = e.genders.FEMALE
	_select_female_button()

func _on_round_or_decimal_button_pressed():
	if round_numbers:
		round_numbers = false
		get_node("right/top_right/round_or_decimal_button").text = "R"
		_update_skills()
	else:
		round_numbers = true
		get_node("right/top_right/round_or_decimal_button").text = "D"
		_update_skills()

func _on_skin_choices_item_selected(ID):
	_get_faces()
	if faces.size() > 0:
		randomize()
		current_face_index = randi()%(faces.size())
	else:
		current_face_index = 0
	
	_display_face(faces[current_face_index] if !faces.empty() else "0")


func _on_hair_choices_item_selected(ID):
	_get_faces()
	if faces.size() > 0:
		randomize()
		current_face_index = randi()%(faces.size())
	else:
		current_face_index = 0
	
	_display_face(faces[current_face_index] if !faces.empty() else "0")


func _on_facial_hair_choices_item_selected(ID):
	_get_faces()
	if faces.size() > 0:
		randomize()
		current_face_index = randi()%(faces.size())
	else:
		current_face_index = 0
	
	_display_face(faces[current_face_index])


func _on_accessory_choices_item_selected(ID):
	_get_faces()
	if faces.size() > 0:
		randomize()
		current_face_index = randi()%(faces.size())
	else:
		current_face_index = 0
	
	_display_face(faces[current_face_index])

func _on_back_button_pressed():
	emit_signal("back_pressed")
	queue_free()

#display tooltip from help.gd on mouseover
func _mouse_enter(name):
	help_label.text = help._get_help("char_gen_" + str(name))

func _mouse_enter_occupation(id):
	var occupation = occupations_resource._get_occupation(int(id))
	help_label.text = occupations_resource._get_bonus_description(occupation) + "\n"
	help_label.text += occupations_resource._get_name_by_id(occupation["ID"], gender).to_upper() + "\n"
	help_label.text += help._get_help("char_gen_" + occupations_resource._get_name_by_id(occupation["ID"], gender)) + "\n"
	help_label.text += _get_contract_string(occupation)

func _mouse_enter_perk_or_drawback(id):
	var perk_or_drawback = p_and_d_resource._get_perk_or_drawback(int(id))
	help_label.text = p_and_d_resource._get_effect_description(perk_or_drawback) + "\n"
	help_label.text += perk_or_drawback["NAME"].to_upper() + "\n"
	help_label.text += help._get_help("char_gen_" + perk_or_drawback["NAME"]) + "\n"
	help_label.text += p_and_d_resource._get_cost_description(perk_or_drawback) + "\n"
	help_label.text += p_and_d_resource._get_opposite_description(perk_or_drawback) 

func _mouse_enter_start_button():
	help_label.text = ""
	
	if occupations != []:
		for occupation in occupations:
			help_label.text += occupation["NAME"][0] + " "
			help_label.text += occupations_resource._get_bonus_description(occupation)
			help_label.text += "\n"
	
	if perks_and_drawbacks != []:
		for perk in perks_and_drawbacks:
			if perk.has("EFFECT"):
				if perk["COST"] > 0:
					help_label.text += perk["NAME"] + " "
					help_label.text += p_and_d_resource._get_effect_description(perk)
					help_label.text += "\n"
		
		for drawback in perks_and_drawbacks:
			if drawback.has("EFFECT"):
				if drawback["COST"] < 0:
					help_label.text += drawback["NAME"] + " "
					help_label.text += p_and_d_resource._get_effect_description(drawback)
					help_label.text += "\n"
		
		for perk in perks_and_drawbacks:
			if !perk.has("EFFECT"):
				if perk["COST"] > 0:
					help_label.text += perk["NAME"] + " "
					help_label.text += "\n"
		
		for drawback in perks_and_drawbacks:
			if !drawback.has("EFFECT"):
				if drawback["COST"] < 0:
					help_label.text += drawback["NAME"] + " "
					help_label.text += "\n"

func _clear_help():
	help_label.text = "Hover over text for more information."
 
func _get_positive_stat_description(stat):
	match stat:
		e.STR:
			return "remarkably strong"
		e.CON:
			return "physically resilient"
		e.DEX:
			return "nimble-fingered"
		e.AGI:
			return "quick on your feet"
		e.CHA:
			return "good with people"
		e.WIS:
			return "super intelligent"
		e.LDR:
			return "a strong leader"
		e.MRK:
			return "a great shot"
		e.MED:
			return "medically proficient"
		e.MEC:
			return "mechanically minded"
		e.ELC:
			return "tech savvy"
		e.ENG:
			return "an engineering genius"
		_:
			return ""

func _get_negative_stat_description(stat):
	match stat:
		e.STR:
			return "weak-muscled"
		e.CON:
			return "delicate"
		e.DEX:
			return "antidexterous"
		e.AGI:
			return "sluggish"
		e.CHA:
			return "socially maladjusted"
		e.WIS:
			return "dumb"
		_:
			return ""

func _set_bonuses_and_penalties():
	bonuses = []
	penalties = []
	
	var stats = [
		[e.STR, character.strength],
		[e.CON, character.constitution],
		[e.DEX, character.dexterity],
		[e.AGI, character.agility],
		[e.CHA, character.charisma],
		[e.WIS, character.wisdom]
	]
	
	var secondary_stats = [
		[e.LDR, character.leadership],
		[e.MRK, character.marksmanship],
		[e.MED, character.medicine],
		[e.MEC, character.mechanical],
		[e.ELC, character.electronics],
		[e.ENG, character.engineering]
	]
	
	stats.sort_custom(self, "_high_to_low")
	
	for stat in stats:
		if stat[1] > g.UPPER_THRESHOLD - 1:
			bonuses.append(stat[0])
	for stat in secondary_stats:
		if stat[1] > g.UPPER_THRESHOLD - 1:
			bonuses.append(stat[0])
	
	for stat in stats:
		if stat[1] < g.LOWER_THRESHOLD + 1:
			penalties.append(stat[0])

func _get_occupation_description():
	var start = "Before the end of the world you were "
	var occupations_string = ""
	
	if occupations != []:
		var i = 1
		for occupation in occupations:
			if occupations.size() > 1 and i == occupations.size():
				occupations_string += " and " + occupation["PREFIX"] + " "  + occupation["NAME"][0]
			elif i < occupations.size() - 1:
				occupations_string += occupation["PREFIX"] + " "  + occupations_resource._get_name_by_id(occupation["ID"], gender) + ", "
			else:
				occupations_string += occupation["PREFIX"] + " " + occupations_resource._get_name_by_id(occupation["ID"], gender) 
			
			i += 1
		
		return start + occupations_string + "."
	else:
		return start + "unemployed."

func _get_bonuses_and_penalties():
	var bonuses_string = ""
	var penalties_string = ""
	
	if bonuses != []:
		var i = 0
		while i < bonuses.size():
			if bonuses.size() > 1 and i == bonuses.size() - 1:
				bonuses_string += " and " + _get_positive_stat_description(bonuses[i])
			elif i < bonuses.size() - 2:
				bonuses_string += _get_positive_stat_description(bonuses[i]) + ", "
			else:
				bonuses_string += _get_positive_stat_description(bonuses[i])
			
			i += 1
	
	if penalties != []:
		var i = 0
		while i < penalties.size():
			if penalties.size() > 1 and i == penalties.size() - 1:
				penalties_string += " and " + _get_negative_stat_description(penalties[i])
			elif i < penalties.size() - 2:
				penalties_string += _get_negative_stat_description(penalties[i]) + ", "
			else:
				penalties_string += _get_negative_stat_description(penalties[i])
			
			i += 1
	
	#if bonuses + penalties
	if penalties != [] and bonuses != []:
		return ("You are " + bonuses_string + ".\nHowever, you are also " + penalties_string + ".")
	#if bonuses / no penalties
	elif penalties == [] and bonuses != []:
		return ("You are " + bonuses_string + ". \nYou don't have any inherent weaknesses.")
	#if penalties / no bonuses
	elif bonuses == [] and penalties != []:
		return ("You don't have any inherent strengths.\nUnfortunately, you're also " + penalties_string + ".")
	else:
		return("You are not remarkable in any way.\n\nIt's a wonder you survived.")

func _high_to_low(a, b):
	if (b[1]) < (a[1]):
		return true
	else:
		return false

func _get_contract_string(occupation):
	match occupations_resource._get_occupation_score(occupation):
		1:
			return "casual (1 occupation point)"
		2:
			return "part time (2 occupation points)"
		3:
			return "full time (3 occupation points)"
		_:
			return null


func _on_year_text_changed(new_text):
	if !year_box.text.empty():
		if year_box.text.is_valid_integer():
			if year_box.text.length() >= 4:
				var latest_year = g.year - g.YOUNGEST_AGE
				var earliest_year = g.year - (g.MAX_POSSIBLE_AGE / 3)
				
				if int(year_box.text) > latest_year:
					year_box.text = str(latest_year)
				elif int(year_box.text) < earliest_year:
					year_box.text = str(earliest_year)
				
				if day_box.text.is_valid_integer() and month_box.text.is_valid_integer():
					match int(month_box.text):
						11, 4, 6, 8:
							if int(day_box.text) > 30:
								day_box.text = "30"
						1, 3, 5, 7, 9, 10, 12:
							if int(day_box.text) > 31:
								day_box.text = "31"
						2:
							if year_box.text.is_valid_integer() and day_box.text.is_valid_integer():
								if (int(year_box.text) - 776) % 4 == 0:
									if int(day_box.text) > 29:
										day_box.text = "29"
								else:
									if int(day_box.text) > 28:
										day_box.text = "28"
							else:
								if int(day_box.text) > 28:
									day_box.text = "28"
								
		else:
			year_box.clear()
	
	_change_age()

func _check_year(year, earliest, latest, index):
	if int(year_box.text[index]) > int(year[index]):
		return false
	elif int(year_box.text[index]) < int(earliest[index]):
		return false
	elif int(year_box.text[index]) > int(latest[index]):
		return false
	else:
		return true
	

func _on_month_text_changed(new_text):
	if !month_box.text.empty():
		if month_box.text.is_valid_integer():
			if int(month_box.text) > 12:
				month_box.text = "12"
			if day_box.text.is_valid_integer():
				match int(month_box.text):
					11, 4, 6, 8:
						if int(day_box.text) > 30:
							day_box.text = "30"
					1, 3, 5, 7, 9, 10, 12:
						if int(day_box.text) > 31:
							day_box.text = "31"
					2:
						if year_box.text.is_valid_integer() and day_box.text.is_valid_integer():
							if (int(year_box.text) - 776) % 4 == 0:
								if int(day_box.text) > 29:
									day_box.text = "29"
							else:
								if int(day_box.text) > 28:
									day_box.text = "28"
						else:
							if int(day_box.text) > 28:
								day_box.text = "28"
		else:
			month_box.clear()
	
	_change_age()

func _on_day_text_changed(new_text):
	if !day_box.text.empty():
		if day_box.text.is_valid_integer():
			if month_box.text.is_valid_integer():
				match int(month_box.text):
					11, 4, 6, 8:
						if int(day_box.text) > 30:
							day_box.text = "30"
					1, 3, 5, 7, 9, 10, 12:
						if int(day_box.text) > 31:
							day_box.text = "31"
					2:
						if year_box.text.is_valid_integer():
							if (int(year_box.text) - 776) % 4 == 0:
								if int(day_box.text) > 29:
									day_box.text = "29"
							else:
								if int(day_box.text) > 28:
									day_box.text = "28"
						else:
							if int(day_box.text) > 28:
								day_box.text = "28"
			else: 
				if int(day_box.text) > 31:
					day_box.text = "31"
		else:
			day_box.clear()
	
	_change_age()

#needs to be this way to stop sliders from going over total_points
func _on_str_slider_value_changed(value):
	_slider_changer(str_slider, str_value, character.strength, str_bonus, value)
	_update_total()

func _on_ldr_slider_value_changed(value):
	_slider_changer(ldr_slider, ldr_value, character.leadership, ldr_bonus, value)
	_update_total()

func _on_con_slider_value_changed(value):
	_slider_changer(con_slider, con_value, character.constitution, con_bonus, value)
	_update_total()

func _on_mrk_slider_value_changed(value):
	_slider_changer(mrk_slider, mrk_value, character.marksmanship, mrk_bonus, value)
	_update_total()

func _on_dex_slider_value_changed(value):
	_slider_changer(dex_slider, dex_value, character.dexterity, dex_bonus, value)
	_update_total()

func _on_elc_slider_value_changed(value):
	_slider_changer(elc_slider, elc_value, character.electronics, elc_bonus, value)
	_update_total()

func _on_agi_slider_value_changed(value):
	_slider_changer(agi_slider, agi_value, character.agility, agi_bonus, value)
	_update_total()

func _on_med_slider_value_changed(value):
	_slider_changer(med_slider, med_value, character.medicine, med_bonus, value)
	_update_total()

func _on_wis_slider_value_changed(value):
	_slider_changer(wis_slider, wis_value, character.wisdom, wis_bonus, value)
	_update_total()

func _on_eng_slider_value_changed(value):
	_slider_changer(eng_slider, eng_value, character.engineering, eng_bonus, value)
	_update_total()

func _on_cha_slider_value_changed(value):
	_slider_changer(cha_slider, cha_value, character.charisma, cha_bonus, value)
	_update_total()

func _on_mec_slider_value_changed(value):
	_slider_changer(mec_slider, mec_value, character.mechanical, mec_bonus, value)
	_update_total()


#node references
onready var start_button = get_node("VBoxContainer/bottom/right/HBoxContainer2/bottom/user_buttons/start_button")
onready var total_value = get_node("VBoxContainer/top/VBoxContainer/country/total_value")
onready var loading_label = get_node("loading_label")

onready var occupation_points_label = get_node("VBoxContainer/bottom/left/bottom_header/occupation_points")
onready var perk_points_label = get_node("VBoxContainer/bottom/left/bottom_header/perk_points")
onready var total_perks_label = get_node("VBoxContainer/bottom/left/bottom_header/total_perks")

onready var day_box = get_node("VBoxContainer/top/VBoxContainer/HBoxContainer/name_entry/day")
onready var month_box = get_node("VBoxContainer/top/VBoxContainer/HBoxContainer/name_entry/month")
onready var year_box = get_node("VBoxContainer/top/VBoxContainer/HBoxContainer/name_entry/year")
onready var age_value = get_node("VBoxContainer/top/VBoxContainer/HBoxContainer/name_entry/age_value")

onready var occupation_choices = get_node("VBoxContainer/bottom/left/bottom_left/occupations/occupation_choices")
onready var perk_choices = get_node("VBoxContainer/bottom/left/bottom_left/perks/perk_choices")
onready var drawback_choices = get_node("VBoxContainer/bottom/left/bottom_left/drawbacks/drawback_choices")

onready var name_edit = get_node("VBoxContainer/top/VBoxContainer/HBoxContainer/name_edit")
onready var country_choices = get_node("VBoxContainer/top/VBoxContainer/country/country_choices")
onready var help_label = get_node("VBoxContainer/bottom/left/HBoxContainer/help_label")

onready var male_button = get_node("VBoxContainer/top/VBoxContainer/HBoxContainer/VBoxContainer/VBoxContainer/male_button")
onready var female_button = get_node("VBoxContainer/top/VBoxContainer/HBoxContainer/VBoxContainer/VBoxContainer/female_button")

onready var easy_button = get_node("VBoxContainer/bottom/right/HBoxContainer2/bottom/difficulty_buttons/HBoxContainer/easy_button")
onready var normal_button = get_node("VBoxContainer/bottom/right/HBoxContainer2/bottom/difficulty_buttons/HBoxContainer/normal_button")
onready var hard_button = get_node("VBoxContainer/bottom/right/HBoxContainer2/bottom/difficulty_buttons/HBoxContainer/hard_button")
onready var extreme_button = get_node("VBoxContainer/bottom/right/HBoxContainer2/bottom/difficulty_buttons/extreme_button")

onready var face = get_node("VBoxContainer/bottom/right/HBoxContainer/VBoxContainer/bottom_right/face")
onready var skin_choices = get_node("VBoxContainer/bottom/right/HBoxContainer/VBoxContainer/bottom_right/face_options/face1/skin_choices")
onready var hair_choices = get_node("VBoxContainer/bottom/right/HBoxContainer/VBoxContainer/bottom_right/face_options/face1/hair_choices")
onready var accessory_choices = get_node("VBoxContainer/bottom/right/HBoxContainer/VBoxContainer/bottom_right/face_options/face2/accessory_choices")
onready var facial_hair_choices = get_node("VBoxContainer/bottom/right/HBoxContainer/VBoxContainer/bottom_right/face_options/face2/facial_hair_choices")

onready var skill_description = get_node("VBoxContainer/bottom/right/HBoxContainer/VBoxContainer/description/skill_description_label")
onready var occupation_description = get_node("VBoxContainer/bottom/right/HBoxContainer/VBoxContainer/description/occupation_description_label")

onready var str_value = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/str/str_value")
onready var con_value = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/con/con_value")
onready var dex_value = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/dex/dex_value")
onready var agi_value = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/agi/agi_value")
onready var cha_value = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/cha/cha_value")
onready var wis_value = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/wis/wis_value")
onready var ldr_value = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/ldr/ldr_value")
onready var mrk_value = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/mrk/mrk_value")
onready var med_value = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/med/med_value")
onready var mec_value = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/mec/mec_value")
onready var elc_value = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/elc/elc_value")
onready var eng_value = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/eng/eng_value")

onready var str_slider = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/str/str_slider")
onready var con_slider = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/con/con_slider")
onready var dex_slider = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/dex/dex_slider")
onready var agi_slider = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/agi/agi_slider")
onready var cha_slider = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/cha/cha_slider")
onready var wis_slider = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/wis/wis_slider")
onready var ldr_slider = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/ldr/ldr_slider")
onready var mrk_slider = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/mrk/mrk_slider")
onready var med_slider = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/med/med_slider")
onready var mec_slider = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/mec/mec_slider")
onready var elc_slider = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/elc/elc_slider")
onready var eng_slider = get_node("VBoxContainer/bottom/left/HBoxContainer/skills/eng/eng_slider")

onready var health = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/health/health_value")
onready var stamina = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/stamina/stamina_value")
onready var carry_weight = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/carry/carry_value")
onready var healing = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/healing/healing_value")
onready var movement = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/movement/movement_value")
onready var sneak = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/sneak/sneak_value")
onready var talk = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/talk/talk_value")
onready var bluff = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/bluff/bluff_value")
onready var lockpick = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/lockpick/lockpick_value")
onready var explosives = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/explosives/explosives_value")
onready var medicine = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/medicine/medicine_value")
onready var mechanical = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/mechanical/mechanical_value")
onready var electronics = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/electronics/electronics_value")
onready var engineering = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/engineering/engineering_value")
onready var learn = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/learn/learn_value")
onready var teach = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/teach/teach_value")
onready var initiative = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/initiative/initiative_value")
onready var block = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/block/block_value")
onready var dodge = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/dodge/dodge_value")
onready var melee_aim = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/melee_aim/melee_aim_value")
onready var melee_dmg = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/melee_dmg/melee_dmg_value")
onready var melee_ap = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/melee_ap/melee_ap_value")
onready var ranged_aim = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/ranged_aim/ranged_aim_value")
onready var ranged_dmg = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/ranged_dmg/ranged_dmg_value")
onready var ranged_ap = get_node("VBoxContainer/bottom/right/HBoxContainer/stats/ranged_ap/ranged_ap_value")


#put all nodes with on-hover tooltips in here
onready var tooltips = [
male_button, female_button,
easy_button, normal_button, hard_button, extreme_button,

help_label,

$VBoxContainer/top/VBoxContainer/country/new_char_button,
$VBoxContainer/top/VBoxContainer/country/load_char_button,
$VBoxContainer/top/VBoxContainer/country/save_char_button,

$VBoxContainer/bottom/left/HBoxContainer/skills/str/str_label, 
$VBoxContainer/bottom/left/HBoxContainer/skills/con/con_label,
$VBoxContainer/bottom/left/HBoxContainer/skills/dex/dex_label,
$VBoxContainer/bottom/left/HBoxContainer/skills/agi/agi_label,
$VBoxContainer/bottom/left/HBoxContainer/skills/cha/cha_label,
$VBoxContainer/bottom/left/HBoxContainer/skills/wis/wis_label, 
$VBoxContainer/bottom/left/HBoxContainer/skills/ldr/ldr_label,
$VBoxContainer/bottom/left/HBoxContainer/skills/mrk/mrk_label,
$VBoxContainer/bottom/left/HBoxContainer/skills/med/med_label,
$VBoxContainer/bottom/left/HBoxContainer/skills/mec/mec_label,
$VBoxContainer/bottom/left/HBoxContainer/skills/elc/elc_label,
$VBoxContainer/bottom/left/HBoxContainer/skills/eng/eng_label,

$VBoxContainer/bottom/right/HBoxContainer/stats/health/health_label, 
$VBoxContainer/bottom/right/HBoxContainer/stats/stamina/stamina_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/carry/carry_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/healing/healing_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/movement/movement_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/sneak/sneak_label, 
$VBoxContainer/bottom/right/HBoxContainer/stats/talk/talk_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/bluff/bluff_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/lockpick/lockpick_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/explosives/explosives_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/medicine/medicine_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/mechanical/mechanical_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/electronics/electronics_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/engineering/engineering_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/learn/learn_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/teach/teach_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/initiative/initiative_label, 
$VBoxContainer/bottom/right/HBoxContainer/stats/block/block_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/dodge/dodge_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/melee_aim/melee_aim_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/melee_dmg/melee_dmg_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/melee_ap/melee_ap_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/ranged_aim/ranged_aim_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/ranged_dmg/ranged_dmg_label,
$VBoxContainer/bottom/right/HBoxContainer/stats/ranged_ap/ranged_ap_label
]

"""
func _save_character_OLD():
	var save_game = File.new()
	save_game.open("res://assets/saved_characters/" + name_edit.text + ".char", File.WRITE)
	
	character_details["NAME"] = name_edit.text
	character_details["FACE"] = current_face_index
	character_details["COUNTRY"] = country_choices.text
	character_details["GENDER"] = null # TODO:
	character_details["BIRTH_DAY"] = day_box.text as int
	character_details["BIRTH_MONTH"] = month_box.text as int
	character_details["BIRTH_YEAR"] = year_box.text as int
	character_details["STR"] = str_slider.value
	character_details["CON"] = con_slider.value
	character_details["AGI"] = agi_slider.value
	character_details["DEX"] = dex_slider.value
	character_details["CHA"] = cha_slider.value
	character_details["WIS"] = wis_slider.value
	character_details["LDR"] = ldr_slider.value
	character_details["MRK"] = mrk_slider.value
	character_details["MED"] = med_slider.value
	character_details["MEC"] = mec_slider.value
	character_details["ELC"] = elc_slider.value
	character_details["ENG"] = eng_slider.value
	
	
	var occupation_ids = []
	for occupation in occupations:
		occupation_ids.append(occupation["ID"])
	character_details["OCCUPATIONS"] = occupation_ids
		
	var perk_and_drawback_ids = []
	for perk_or_drawback in perks_and_drawbacks:
		perk_and_drawback_ids.append(perk_or_drawback["ID"])
	character_details["PERKS_DRAWBACKS"] = perk_and_drawback_ids
	
	for key in character_details.keys():
		save_game.store_line(JSON.print(key) + " " + JSON.print(character_details[key]))
	
	save_game.close()

func _load_character_OLD():
	var save_game = File.new()
	if !save_game.file_exists("res://assets/saved_characters/" + name_edit.text + ".char"):
		return
	save_game.open("res://assets/saved_characters/" + name_edit.text + ".char", File.READ)
	
	var save_key
	var save_value
	while !save_game.eof_reached():
		var line = save_game.get_line()
		save_key = line.left(line.find(" ", 0) - 1).right(1)
		save_value = line.right(line.find(" ", 0) + 1)
		character_details[save_key] = save_value
	
	name_edit.text = _fix_save_string(character_details["NAME"])
	current_face_index = character_details["FACE"]
	country_choices.text = _fix_save_string(character_details["COUNTRY"])
	#TODO: GENDER
	day_box.text = character_details["BIRTH_DAY"]
	month_box.text = character_details["BIRTH_MONTH"]
	year_box.text = character_details["BIRTH_YEAR"]
	str_slider.value = character_details["STR"] as int
	con_slider.value = character_details["CON"] as int
	dex_slider.value = character_details["DEX"] as int
	agi_slider.value = character_details["AGI"] as int
	cha_slider.value = character_details["CHA"] as int
	wis_slider.value = character_details["WIS"] as int
	ldr_slider.value = character_details["LDR"] as int
	mrk_slider.value = character_details["MRK"] as int
	med_slider.value = character_details["MED"] as int
	mec_slider.value = character_details["MEC"] as int
	elc_slider.value = character_details["ELC"] as int
	eng_slider.value = character_details["ENG"] as int
	
	occupations.clear()
	for occupation_id in _read_array_string(character_details["OCCUPATIONS"]):
		occupations.append(occupations_resource._get_occupation(occupation_id))
	perks_and_drawbacks.clear()
	for p_or_d_id in _read_array_string(character_details["PERKS_DRAWBACKS"]):
		perks_and_drawbacks.append(p_and_d_resource._get_perk_or_drawback(p_or_d_id))
	print(occupations)
	print(perks_and_drawbacks)
	
	
	face.texture = load(g.FACE_SPRITE_DIR + current_face_index + ".png")
	
	save_game.close()
	
	change_made = false

func _fix_save_string(string):
	return string.left(string.length() - 1).right(1)

func _read_array_string(array_string) -> Array:
	var output = []
	var number = ""
	for character in array_string:
		if character == "[" or character == "]" or character == ",":
			number += character
		else:
			if number != "":
				output.append(number as int)
				number = ""
	return output
"""
