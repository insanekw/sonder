extends Node2D

const FRAMES_PER_GAME_SECOND = 60
const TIME_MULTIPLIER = 1
var frame_counter = 0

var game_soft_paused = false

# Counter for days player have survived in current game
var player_day = 1

#TODO:key value method for determining day of the week for any given date

func _ready():
	if g.date_time == null:
		g.date_time = OS.get_datetime()
		g.seconds = g.date_time.second
		g.minutes = g.date_time.minute
		g.hour = g.date_time.hour
		g.day = g.date_time.day
		g.month = g.date_time.month
		g.year = g.date_time.year + 1
		g.am_pm = "am" if g.hour <12 else "pm"
		g.day_of_week = g.date_time.weekday
	
	# Freeze time
	_start_stop_clock()
	_print_date_and_time()
	
	set_process(true)

func _start_stop_clock():
	if game_soft_paused:
		$stopped_label.visible = false
		game_soft_paused = false
	else:
		$stopped_label.visible = true
		game_soft_paused = true

func _process(delta):
	# Count time/date
	if !game_soft_paused:
		frame_counter += 1
		if frame_counter >= FRAMES_PER_GAME_SECOND / TIME_MULTIPLIER:
			frame_counter = 0
			g.seconds += 1
			_calculate_time()
		
		# Display time
		_print_date_and_time()


func _calculate_time():
	if g.seconds >= 60:
		g.seconds -= 60
		g.minutes += 1
		if g.minutes >= 60:
			g.minutes -= 60
			g.hour += 1
			if g.hour == 13:
				g.hour = 1
			if g.hour == 12:
				if g.am_pm == "am":
					g.am_pm = "pm"
				else:
					g.am_pm = "am"
					player_day += 1
					g.day_of_week += 1
					if g.day_of_week > 6:
						g.day_of_week = 0
					g.day += 1
					if g.day > 28:
						# 30 days hath september..
						if (g.month == 11 or 4 or 6 or 8) and g.day > 30:
							_go_to_next_month()
						# ..all the rest have 31
						elif (g.month == 1 or 3 or 5 or 7 or 9 or 10 or 12) and g.day > 31:
							_go_to_next_month()
						# ..except for february
						elif g.leap_year and g.month == 2 and g.day > 29:
							_go_to_next_month()
						elif !g.leap_year and g.month == 2 and g.day > 28:
							_go_to_next_month()

func _go_to_next_month():
	g.day = 1
	g.month += 1
	if g.month > 12:
		g.month -= 12
		g.year += 1

func _print_date_and_time():
	get_node("display/date").text = _print_date()
	get_node("display/day").text = _print_day_of_week()
	get_node("display/time").text = _print_time()

func _print_date():
	return (
	(str(g.day) if g.day >= 10 
	else "0" + str(g.day)) 
	+ "/" +
	(str(g.month) if g.month >= 10 
	else "0" + str(g.month)) 
	+ "/" +
	(str(g.year) if g.year >= 10 
	else "0" + str(g.year))
	)

func _print_time():
	return (
	("00" if g._24_hour_time and g.hour == 12 and g.am_pm == "am"
	else str(g.hour + 12) if (g._24_hour_time and g.hour >= 1 and g.hour < 12 and g.am_pm == "pm")
	else str(g.hour) if g.hour >= 10
	else "0" + str(g.hour))
	+ ":" +
	(str(g.minutes) if g.minutes >= 10 
	else "0" + str(g.minutes))
	+ ":" +
	(str(g.seconds) if g.seconds >= 10 
	else "0" + str(g.seconds))
	# AM/PM
	+ (g.am_pm if !g._24_hour_time else "")
	)

func _print_day_of_week():
	if g.day_of_week == 0:
		 return "Sunday"
	elif g.day_of_week == 1:
		return "Monday"
	elif g.day_of_week == 2:
		return "Tuesday"
	elif g.day_of_week == 3:
		return "Wednesday"
	elif g.day_of_week == 4:
		return "Thursday" 
	elif g.day_of_week == 5:
		return "Friday"
	else:
		return "Saturday"
