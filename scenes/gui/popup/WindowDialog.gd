extends WindowDialog

onready var party_members_tab = get_node("main/party_members")

# Called when the node enters the scene tree for the first time.
func _ready():
	party_members_tab.add_tab("Player 1")
	party_members_tab.add_tab("Player 2")
	party_members_tab.add_tab("Player 3")
	party_members_tab.add_tab("Player 4")
	popup()
	self.size_flags_stretch_ratio = $main.size_flags_stretch_ratio

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
