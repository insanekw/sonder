extends Node

enum {
	CLIMB, SMASH, CUT
	}

# Rules for certain tile types -ie A* weight, interaction flags, cover % etc
var rules = [
	# Floor
	{
		"NAME": "floor",
		"ID": [0, 1, 2, 3, 4],
		"WEIGHT": 100,#absence of weight denotes an unwalkable area
		"FLAGS": [],
		"COVER": 0
	},
	# Wall
	{
		"NAME": "wall",
		"ID": [5],
		"FLAGS": [CLIMB, SMASH],
		"COVER": 100
	},
	# Fence
	{
		"NAME": "fence",
		"ID": [6],
		"WEIGHT": 300,
		"FLAGS": [CLIMB, SMASH, CUT],
		"COVER": 10
	}
]
